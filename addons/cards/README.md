# TEM CARDS #

This is simple implementation to build territory chards including a map and prepared for printing in pdf format.

Could not use html2canvas, because rendering of two cards is not working probably. Things are getting worse when more than one map is on the website e.g. another div or table. One map was working fine, also with decimal zoom stages.

Use Leaflet-image plugin, with the drawback that only integer zoom stages are working => zoomSnap must be set to 1. But then maps are rendered very nicely and independent from the website layout.
