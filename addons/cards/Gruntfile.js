module.exports = function(grunt) {

 grunt.initConfig({
    //pkg: grunt.file.readJSON('package.json'),


    //---------------------------------------------------------------------------------------------
    // modify bower components, parse file while copying (also used just for copying)
    //---------------------------------------------------------------------------------------------
    'string-replace': {
      //'jspdf': {
        // folder structure does not follow the bower requirement
      //},
      'opentem_card' : {
        files: {
            'dist/opentem_card_root.js'     : 'src/opentem_card_root.js',
            'dist/opentem_card_root.php'    : 'src/opentem_card_root.php',
            'dist/leaflet-address-marker.js': 'src/leaflet-address-marker.js',
            'dist/opentem_card_addr.js'     : 'src/opentem_card_addr.js',
            'dist/opentem_card_addr.php'    : 'src/opentem_card_addr.php',
            'dist/opentem_card_regular.js'  : 'src/opentem_card_regular.js',
            'dist/opentem_card_regular.php' : 'src/opentem_card_regular.php',
            'dist/opentem_card_search.js'   : 'src/opentem_card_search.js' ,
            'dist/opentem_card_search.php'  : 'src/opentem_card_search.php'
        },
        options: {
            replacements: [{
                pattern: /src\/(.*\.png)/g,
                replacement: "addons/opentem_card/images/$1"
            }]
        }
      }
    },

    //---------------------------------------------------------------------------------------------
    // merge bower components to single file
    //---------------------------------------------------------------------------------------------
    bower_concat: {
      all: {
        dest: {
          'js': 'dist/opentem_card_plugins.js',
          'css': 'dist/opentem_card.css'
        },
        mainFiles: {
          // upper lower case breaks bower_concat
          'jspdf': ['dist/jspdf.debug.js']
          //'html2canvas': ['build/html2canvas.js']
        },
        callback: function(mainFiles, component) {
          // use modified files form string-replace if available
          return mainFiles.map(function(filepath) {
            var mod = filepath.replace(/\.(js|css)$/, '.mod.$1');
            return grunt.file.exists(mod) ? mod : filepath;
          });
        },
        bowerOptions: {
          relative: false
        }
      }
    },

    //---------------------------------------------------------------------------------------------
    // copy images for distribution
    //---------------------------------------------------------------------------------------------
    copy: {
      images: {
        files: [
          { expand: true, cwd: 'src', src: '*.png', dest: 'dist/images' }
        ]
      }
    },
	
    bowercopy: {
      default: {
        files: {
          'dist/images': '*/dist/images/*'
        }
      }
    },

    //---------------------------------------------------------------------------------------------
    // minimize javascript codes for faster browser load
    //---------------------------------------------------------------------------------------------
    uglify: {
      opentem_card_plugins: {
        src: 'dist/opentem_card_plugins.js',
        dest: 'dist/opentem_card_plugins.min.js'
      },
      leafletBoundarySearch: {
        src:  'dist/leaflet-address-marker.js',
        dest: 'dist/leaflet-address-marker.min.js'
      },
      opentem_card_root: {
        src: 'dist/opentem_card_root.js',
        dest: 'dist/opentem_card_root.min.js'
      },
      opentem_card_addr: {
        src: 'dist/opentem_card_addr.js',
        dest: 'dist/opentem_card_addr.min.js'
      },
      opentem_card_regular: {
        src: 'dist/opentem_card_regular.js',
        dest: 'dist/opentem_card_regular.min.js'
      },
      opentem_card_search: {
        src: 'dist/opentem_card_search.js',
        dest: 'dist/opentem_card_search.min.js'
      }
    }

  });

  grunt.loadNpmTasks('grunt-string-replace');
  grunt.loadNpmTasks('grunt-bowercopy');
  grunt.loadNpmTasks('grunt-bower-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.registerTask('dist', ['string-replace', 'bower_concat', 'copy', 'bowercopy', 'uglify']);

};
