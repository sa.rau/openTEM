<!-- server side data handling of the map -->
<?php

    // simulated db

    // check if data was send
    if ( !empty($_POST) ) {

        // get send data from browser (client - server interface)
        $tid = $_POST['ID'];

        // decode map picture
        $mapImgString = $_POST['mapImgString'];
        $signature = str_replace('data:image/png;base64,', '', $mapImgString);
        $signature = str_replace(' ', '+', $signature);
        $data = base64_decode($signature);
        file_put_contents("db/map.png",$data);

        // decode map mini picture
        $mapMiniImgString = $_POST['mapMiniImgString'];
        $signature = str_replace('data:image/png;base64,', '', $mapMiniImgString);
        $signature = str_replace(' ', '+', $signature);
        $data = base64_decode($signature);
        file_put_contents("db/map_mini.png",$data);

        // after saving png files call php script
        file_put_contents('db/map.txt',"${tid}");
    } else {
        // TODO Error
        file_put_contents('db/error.txt',"Da war nix");
    }

?>
