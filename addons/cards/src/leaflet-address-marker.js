// Author Stephan Rau

L.Icon.Text = L.Icon.Default.extend({
    options: {
        text: '',
        iconUrl:       'marker-icon-solid.png',
    },

    // unfortunately the leaflet image cannot handle the div or canvas for marker
    // therefore instead of a solid maker picture with dynamic text, just static pictures used
	_createIconDynamic: function (oldIcon) {
        var options = this.options;
		var size = L.point(options['iconSize']);
		var anchor = L.point(options.iconAnchor);

		if (!anchor && size) {
			anchor = size.divideBy(2, true);
		}

        var name = "icon";
        var src = this._getIconUrl(name);

        var div = document.createElement('div');

        div.className = "leaflet-marker-icon " + name;
        div.style.backgroundImage = "url('" + src + "')";
        div.style.marginLeft = -anchor.x + 'px';
        div.style.marginTop  = -anchor.y + 'px';
        div.style.width  = size.x + 'px';
        div.style.height = size.y + 'px';

        var span = L.DomUtil.create('span');
        span.style.width  = size.x + 'px';
        span.style.height = size.y + 'px';
        span.style.textAlign = 'center';
        span.style.fontSize = '14px';
        span.style.fontWeight  = 'bold';
        span.style.color = 'white';
        span.style.marginTop = '4px';
        span.style.marginLeft = '-1px';
        span.style.pointerEvents = 'none';
        span.style.display = 'inline-block';
        span.innerHTML = options.text;
        div.appendChild(span);

        return div;

    },

	createIcon: function (oldIcon) {
        var name = "icon";
        var iconUrl = this.options.iconUrl;

		if (!L.Icon.Default.imagePath) {	// Deprecated, backwards-compatibility only
			L.Icon.Default.imagePath = this._detectIconPath();
		}

        // build icon url test if text is between 1 and 20 else return standard image
        if ( this.options.text > 0 && this.options.text < 21 ) {
            iconUrl = 'marker-icon-solid-' + this.options.text + '.png';
        }
        var src = (this.options.imagePath || L.Icon.Default.imagePath) + iconUrl;
		//var src = this._getIconUrl('icon');

		var img = this._createImg(src);
        this._setIconStyles(img, name);

        return img;


    },

});

L.AddressMarker = function (address,value) {
    var latlng = {"lat" : 48.125767833701666, "lng" : 11.48998260498047};

    var nominatimApiUrl = 'https://nominatim.openstreetmap.org/search?'
        + 'format=json'
        + '&limit=1'
        + '&q="' + address + '"'
    ;

    $.get(nominatimApiUrl, function(data) {
        latlng = { "lat" : data[0].lat, "lng" : data[0].lon };
        L.marker( latlng, {
            icon: new L.Icon.Text({text : value})
        }).addTo(opentem_maps_near);
    });

};
