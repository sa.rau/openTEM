<!DOCTYPE html>
<html>
<head>
    <title>opentem-cards</title>
    <!-- Bootstrap core CSS and some custom styles -->
    <link href="./design/css/bootstrap.css" rel="stylesheet">
    <link href="./design/css/theme.bootstrap.css"  rel="stylesheet">
    <!-- <script src="./design/js/jquery.js"></script> -->
    <!-- include merged and minified leaflet plugins needed form opentem_maps -->
    <link rel="stylesheet" href="../maps/dist/opentem_maps.css" />
    <script src="../maps/dist/opentem_maps_plugins.js"></script>
    <!-- include debug able plugins for the tem card extension                                   -->
    <!-- <script src="bower_components/jspdf/dist/jspdf.debug.js"></script> -->
    <!-- <script src="bower_components/leaflet-image/leaflet-image.js"></script> -->
    <link rel="stylesheet" href="./dist/opentem_card.css" />
    <script src="./dist/opentem_card_plugins.js"></script>
    <!-- add some css to hide page while generating the pdf                                      -->
    <style>
        #cover {
            display: none;      text-align: center;     padding-top: 200px;     background: #CCC;
            opacity: 0.5;       position: absolute;     top: 0;                 left: 0;
            width: 100%;        height: 100%;           z-index: 10000;
            font-family: Verdana, sans-serif;           font-size: 30px;
        }
        #cover.active{
            display: block;     cursor: wait;
        }
    </style>
</head>
<body>

<h1>Select Card Type</h1>

    <?php
        if( isset($_REQUEST['maptype']) ) {
            $maptype = $_REQUEST['maptype'];
        } else {
            $maptype = "card_regular";
        }
    ?>


    <form>
        <div class="form-group"> <div class="input-group">
            <span class="input-group-addon">Card Type</span>
            <select class="form-control" name=maptype onchange="this.form.submit()">
                <option value="card_regular"<?php if($maptype == "card_regular") {print "selected";} ?> >regular</option>
                <option value="card_search" <?php if($maptype == "card_search") {print "selected";} ?> >search</option>
                <option value="card_root"   <?php if($maptype == "card_root") {print "selected";} ?> >root</option>
                <option value="card_addr"   <?php if($maptype == "card_addr") {print "selected";} ?> >addr</option>
            </select>
        </div></div>
    </form>


    <!-- ======================================================================================== -->
    <?php
        // for debugging on webserver -------
        ini_set('display_startup_errors',1);
        ini_set('display_errors',1);
        error_reporting(-1);
        //-----------------------------------

        //------------------------------------------------------------------------------------------
        // configuration and sample data usually provided by a database e.g. mySQL or SQLite
        //------------------------------------------------------------------------------------------
        //xdebug_break();

        // headers (language depended)
        $json_string = file_get_contents("./config/header.json");
        $header = json_decode($json_string, true);

        // common congregation information
        $json_string = file_get_contents("./config/congregation.json");
        $congr = json_decode($json_string, true);


        // define printed card dimension in mm
        $json_string = file_get_contents("./config/" . $maptype . ".json");
        $card = json_decode($json_string, true);

        // load special field info
        $json_string = file_get_contents("./config/territoriesaddinfo.json");
        $taicn = json_decode($json_string, true); // territory additional info column name

        // calculate map dimension in px
        $webpage_width_px = ($maptype == "card_regular") ? 300 : 900;
        //xdebug_break();
        $far_width_px = $card["map"]["far"]["width"] * $card["map"]["far"]["dpmm"];
        $far_height_px = $card["map"]["far"]["height"] * $card["map"]["far"]["dpmm"];
        $far_scale = $webpage_width_px / $far_width_px;
        $far_translate_x_px = ($webpage_width_px - $far_width_px) / 2 / $far_scale ;
        $far_translate_y_px = ($webpage_width_px * $far_height_px / $far_width_px - $far_height_px) / 2 / $far_scale ;

        $webpage_width_px = 900;
        $near_width_px = $card["map"]["near"]["width"] * $card["map"]["near"]["dpmm"];
        $near_height_px = $card["map"]["near"]["height"] * $card["map"]["near"]["dpmm"];
        $near_scale = $webpage_width_px / $near_width_px;
        $near_translate_x_px = ($webpage_width_px - $near_width_px) / 2 / $near_scale ;
        $near_translate_y_px = ($webpage_width_px * $near_height_px / $near_width_px - $near_height_px) / 2 / $near_scale ;

        $map_style = array(
            "near"  => 'style="'.
                'width: '  . $near_width_px . 'px; '.
                'height: ' . $near_height_px . 'px; ' .
                'transform: scale(' . $near_scale . ')' .
                            'translate(' . $near_translate_x_px . 'px, ' . $near_translate_y_px . 'px);"'
            ,
            "far"  => 'style="'.
                'width: '  . $far_width_px . 'px; '.
                'height: ' . $far_height_px . 'px; ' .
                'transform: scale(' . $far_scale . ')' .
                            'translate(' . $far_translate_x_px . 'px, ' . $far_translate_y_px . 'px);"'
        );
    ?>

    <!-- ======================================================================================== -->
    <!-- server side data handling of the map                                                     -->
    <!-- ======================================================================================== -->
    <?php
        // simulated db (just a file in json format, which stores a single data set)
        $db = '../maps/db/db.txt';

        // just opened the page
        if ( file_exists($db) ) {
            // read file - TODO check if readable and proper json format
            $opentem_maps_data = json_decode(file_get_contents($db),true);
        } else {
            // load some default values
            $opentem_maps_data = [
                "mark"      => null,
                "center"    => '{"lat":48.0995,"lng":11.5751}',
                "zoom"      => 15
            ];
        }

        // territory test data for card test (again single territory data)
        $terri = array (
            "id"        => "1",
            "name"      => "705",
            "flats"     => "260",
            "blockpart" => "80%",
            "ask"       => "1",
            "streets"   => array (
                ['name' => 'Armanspergstr. 4, 81545 München'          , 'info' => "" ],
                ['name' => 'Grünwalderstrasse 6'            , 'info' => "inkl. Läden" ],
                ['name' => 'Harlachingerstrasse 1 - 17'     , 'info' => "" ],
                ['name' => 'Reichardtweg (???)'             , 'info' => "" ],
                ['name' => 'Reichenhallerstrasse 3 - 15'    , 'info' => "" ],
                ['name' => 'Reichenhallerstrasse 8'         , 'info' => "" ],
                ['name' => 'Schorerstrasse 4 - 6'           , 'info' => "" ],
                ['name' => 'Volckmerstrasse 2 - 4'          , 'info' => "" ],
                ['name' => 'Weningstrasse 2 - 18'           , 'info' => "" ],
                ['name' => 'Weningstrasse 3'                , 'info' => "" ],
                ['name' => 'Wettersteinstraße 1 - 9'        , 'info' => "" ],
                ['name' => 'Wettersteinstraße 2 - 10'       , 'info' => "" ]
            ),
            "info" => array (
                 ["Strassemitlangennamen 1 ", "einNameder auch länger ist", "03/2017", "NWB"]
               , ["Strassemitlangennamen 2 ", "einNameder auch länger ist", "02/2016", "NWB"]
               , ["Strassemitlangennamen 3 ", "einNameder auch länger ist", "01/2015", "NWB"]
               , ["Strassemitlangennamen 4 ", "einNameder auch länger ist", "12/2014", "NWB"]
               , ["Strassemitlangennamen 5 ", "einNameder auch länger ist", "11/2013", "NWB"]
               , ["Strassemitlangennamen 6 ", "einNameder auch länger ist", "10/2012", "NWB"]
               , ["Strassemitlangennamen 7 ", "einNameder auch länger ist", "9/2010",  "NWB"]
               , ["Strassemitlangennamen 8 ", "einNameder auch länger ist", "8/2009",  "NWB"]
            /* , ["Strassemitlangennamen 9 ", "einNameder auch länger ist", "07/2008", "NWB"]
               , ["Strassemitlangennamen 10", "einNameder auch länger ist", "06/2007", "NWB"]
               , ["Strassemitlangennamen 11", "einNameder auch länger ist", "05/2006", "NWB"]
               , ["Strassemitlangennamen 12", "einNameder auch länger ist", "04/2000", "NWB"]
               , ["Strassemitlangennamen 13", "einNameder auch länger ist", "03/2017", "NWB"]
               , ["Strassemitlangennamen 14", "einNameder auch länger ist", "02/2017", "NWB"]
               , ["Strassemitlangennamen 15", "einNameder auch länger ist", "01/2017", "NWB"]
               , ["Strassemitlangennamen 16", "einNameder auch länger ist", "12/2017", "NWB"]
               , ["Strassemitlangennamen 17", "einNameder auch länger ist", "1/2017",  "NWB"]
               , ["Strassemitlangennamen 18", "einNameder auch länger ist", "1/2017",  "NWB"]
               , ["Strassemitlangennamen 19", "einNameder auch länger ist", "1/2017",  "NWB"]
               , ["Strassemitlangennamen 20", "einNameder auch länger ist", "1/2017",  "NWB"] */
            )
        );

    ?>
    <!-- ======================================================================================== -->






    <!-- ======================================================================================== -->
    <!-- load opentem_maps frontend addon the map                                                 -->
    <!-- ======================================================================================== -->
    <?php
    // load example data and simple card representation for selected maptype
        require("./src/opentem_" . $maptype . ".php");
    ?>
    <!-- save server side data in java script variable                                            -->
    <script>
        var header  = <?php echo json_encode($header)   ;?>;
        var congr   = <?php echo json_encode($congr)    ;?>;
        var card    = <?php echo json_encode($card)     ;?>;
        var terri   = <?php echo json_encode($terri)    ;?>;
        var taicn   = <?php echo json_encode($taicn)    ;?>;
    </script>
    <!-- load tem_cards extension in debug able version                                          -->
    <script <?php print('src="src/opentem_' . $maptype . '.js"');?>></script>
    <script> <?php
        // use php to write some javascript code
        // null is an empty string, but for the javascript function a "null" string is needed
        if( !isset($opentem_maps_data["mark"]) ) {
            $opentem_maps_data["mark"] = 'null';
        }

        $zoom_far  = $opentem_maps_data["zoom"] + $card["map"]["far"]["zoom"]["delta"];
        $zoom_near = $opentem_maps_data["zoom"] + $card["map"]["near"]["zoom"]["delta"];

        print("opentem_maps_init($opentem_maps_data[mark],$opentem_maps_data[center],$zoom_far,$zoom_near);");

    ?> </script>

</body>
</html>

