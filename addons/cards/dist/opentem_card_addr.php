    <!-- ======================================================================================== -->
    <!--     web site which is representing the data of the printable pdf and looks alike         -->
    <!-- ======================================================================================== -->
    <div id="cover">Einen Moment Geduld bitte, PDF wird generiert.</div>
    <div style="width: 920px; border: 5px solid #ccc; margin-top: 10px">

        <button style="width: 100%; height: 3em" id="gen_tem_card_pdf" class="btn btn-success">
            Generiere Adress Karten PDF
        </button>
        <span class="input-group-addon"><input type="checkbox" id="gen_tem_card_print_address">
            Adressen drucken.
        </span>

        <div style="width: 100%; height: 2em; border: 5px solid #ccc;
                font-family: Verdana, sans-serif; font-size: 30px; text-align: center;">
            <?php echo $congr["name"]; ?>
        </div>

        <div style="width: 100%; height: <?php echo $near_height_px * $near_scale + 10;?>px; border: 5px solid #ccc">
            <div id="map_near" <?php echo $map_style["near"];?>></div>
        </div>

        <div style="width: 100%; height: 60px; border: 5px solid #ccc;
                font-family: Verdana, sans-serif; font-size: 30px; text-align: center;">
            <?php echo $congr["name"]; ?>
        </div>

            <!--
        <div style="width: 100%; height: 330px; border: 5px solid #ccc">
            <div style="float: left; width: 320px; height: 320px; border: 5px solid #ccc">
                <div style="width: 100%; height: <?php echo $far_height_px * $far_scale + 10;?>px; border: 5px solid #ccc">
                    <div id="map_far"  <?php echo $map_style["far"];?>></div>
                </div>
            </div>



        </div>
            -->

        <div style="width: 100%; height: 50px; border: 5px solid #ccc;
            font-family: Verdana, sans-serif; font-size: 25px; text-align: center;">
            Bekannte Adressen
        </div>

        <div style="width: 100%; border: 5px solid #ccc;
            font-family: Verdana, sans-serif; font-size: 18px;">
            <?php
            //xdebug_break();
            $line = 1;
            foreach ($terri["streets"] as $street) {
                echo $line . ". " . $street["name"] . ", "  . $street["info"] . "</br>";
                $line = $line + 1;
            }
            ?>
        </div>

        <!-- <div style="width: 100%; height: 100%; border: 5px solid #ccc"> -->
            <div style="width: 100%; height: 50px; border: 5px solid #ccc;
                font-family: Verdana, sans-serif; font-size: 25px; text-align: center;">
                Zusatzinformationen
            </div>
            <div style="width: 100%; border: 5px solid #ccc;
                font-family: Verdana, sans-serif; font-size: 15px;">
                <?php
                if ( $terri["ask"] )
                    echo "<strong><font color=\"red\">" . $header["ask"] . "</font></strong></br>";

                foreach ($terri["info"] as $info) {
                    $info_string = "";
                    foreach ( $info as $column) {
                        $info_string .= $column . " ";
                    }
                    echo $info_string . "</br>";
                }
                ?>
            </div>

        <!-- </div> -->

    </div>
