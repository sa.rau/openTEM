/***************************************************************************************************
    opentem_maps_near := TEritory Management MAPS
****************************************************************************************************
    Copyright (c) 2017 Stephan Rau
    Licensed under the MIT license.
****************************************************************************************************
    creation date:  25.01.2017
    last change:    25.06.2017
***************************************************************************************************/

//--------------------------------------------------------------------------------------------------
// osm tile servers
//--------------------------------------------------------------------------------------------------
// connect to a tile server and connect to the leaflet plugin
   var osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';           // default one
// var osmUrl = 'http://{s}.tile.stamen.com/toner/{z}/{x}/{y}.png';             // high contrast
// var osmUrl = 'http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png';   // low contrast
// var osmUrl = 'http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png';
// var osmUrl = 'https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png';
// var osmUrl = 'http://{s}.tile.stamen.com/terrain/{z}/{x}/{y}.png';
// var osmUrl = 'http://{s}.tile.stamen.com/watercolor/{z}/{x}/{y}.png';
// var osmUrl = 'http://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png';
// var osmUrl = 'http://tile.openstreetmap.de/{z}/{x}/{y}.png';                 // not working
var osmAttrib = '&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a>';


//--------------------------------------------------------------------------------------------------
// create main map (near)
//--------------------------------------------------------------------------------------------------
var osm_near = new L.tileLayer(osmUrl, {maxZoom: card.map.near.zoom.max, attribution: osmAttrib});
var opentem_maps_near = new L.Map('map_near', {
        layers: [osm_near],
        preferCanvas: true,
        zoomControl: false,
        fullscreenControl: false    // do not load leaflet-fullscreen plugin
    });
    opentem_maps_near.dragging.disable();
    opentem_maps_near.touchZoom.disable();
    opentem_maps_near.doubleClickZoom.disable();
    opentem_maps_near.scrollWheelZoom.disable();
    opentem_maps_near.boxZoom.disable();
    opentem_maps_near.keyboard.disable();
//--------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------
// create second map with different zoom to get an overview, disable all mouse interaction
//--------------------------------------------------------------------------------------------------
/*
var osm_far = new L.TileLayer(osmUrl, {minZoom: 0, maxZoom: card.map.far.zoom.max, attribution: osmAttrib});
var opentem_maps_far = new L.Map('map_far', {
        layers: [osm_far],
        preferCanvas: true,
        zoomControl: false,
        fullscreenControl: false    // load leaflet-fullscreen plugin
    });
    opentem_maps_far.dragging.disable();
    opentem_maps_far.touchZoom.disable();
    opentem_maps_far.doubleClickZoom.disable();
    opentem_maps_far.scrollWheelZoom.disable();
    opentem_maps_far.boxZoom.disable();
    opentem_maps_far.keyboard.disable();
*/
//--------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------
// set map to initial state (default value or values from database)
//--------------------------------------------------------------------------------------------------
function opentem_maps_init(pArea,pCenter,pZoom_far,pZoom_near) {

    // set initial values of the far map
    var bounds = [[pCenter.lat-0.1, pCenter.lng-0.1,], [pCenter.lat+0.1, pCenter.lng+0.1]]
    //opentem_maps_far.setView(pCenter,pZoom_far);

    // sent initial map position near map
    //   worldBounds needed to invert map mark of near map
    var worldBounds = [[90, -180], [90, 180], [-90, 180],[-90, -180]];
    opentem_maps_near.setView(pCenter,pZoom_near);

    // add marks for known streets
    var currentNumber = 1;
    terri.streets.forEach(function(street) {
        //console.log(street.name)
        // pdf.text(street.name + ", " + street.info, x, y - card.p1.marginTB );
        L.AddressMarker(street.name, currentNumber);
        currentNumber++;
    });

    //L.AddressMarker("Armanspergstr. 4, 81545 München", 1);
    //L.AddressMarker("Erlbachstr. 9, 81249 München", 20);

    // add area mark if available   TODO must be available ?
    if(pArea){
        L.polygon(
            [worldBounds,pArea],
            {'fillColor': '#ffffff', 'fillOpacity': 0.70}
        ).addTo(opentem_maps_near);
        //L.polygon(pArea).addTo(opentem_maps_far);
    }

}
//--------------------------------------------------------------------------------------------------


//==================================================================================================
// generate printable pdf powered by jspdf
//==================================================================================================

// get button and cover to hide web site while generating pdf
var exportButton = document.getElementById('gen_tem_card_pdf');
//var hideAddress = document.getElementById('gen_tem_card_hide_address');
var cover = document.getElementById('cover');

// page just loaded enable button and do not cover page
exportButton.disabled = false;
cover.className = '';

// do when button was clicked
exportButton.addEventListener('click', function() {

    // disable button and cover page
    exportButton.disabled = true;
    cover.className = 'active';

    // promise is used because getting the tiles and generating a single picture takes some time
    Promise.all([
        /*
        new Promise(function (resolve){
            // generate picture of far map for pdf generation
            leafletImage(opentem_maps_far, function(err, canvas) {
                //document.body.appendChild(canvas);      // show picture without pdf generation
                resolve(canvas.toDataURL("image/png", 1.0));
            });
        })
        ,
        */
        new Promise(function (resolve){
            // generate picture of near map for pdf generation
            leafletImage(opentem_maps_near, function(err, canvas) {
                //document.body.appendChild(canvas);      // show picture without pdf generation
                resolve(canvas.toDataURL("image/png", 1.0));
            });
        })
    ]).then(function (mapPics) {

        //------------------------------------------------------------------------------------------
        // option to send pic to server instead of generating pdf in browser
        //------------------------------------------------------------------------------------------
        /*
        r = new XMLHttpRequest();
        r.open('POST', 'tem_cards.php', true);
        r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        r.send('mapMiniImgString='+mapPics[0]+'&mapImgString='+mapPics[1]+'&ID='+tid);
        ----------------------------------------------------------------------------------------- */


        //------------------------------------------------------------------------------------------
        // generate printable pdf
        //------------------------------------------------------------------------------------------
        //debugger;

        // create pdf and set default Font
        var pdf = new jsPDF(card.paper.orientation, card.paper.units, card.paper.format);
        pdf.addFont (card.font.path, card.font.name, card.font.style, card.font.encoding);
        pdf.setFont (card.font.name);
        pdf.setFillColor(card.color.background);

        // add cutting markers
        var y; var x;
        // upper cutting edge
        y = card.border + card.print.margin;
        pdf.line(
            card.border - card.print.border, y,
            card.border + card.width + card.print.border, y
        );
        // lower cutting edge or folding edge
        y = card.border + 2 * card.height - card.print.margin;
        pdf.line(
            card.border - card.print.border, y,
            card.border + card.print.margin, y
        );
        pdf.line(
            card.border + card.width - card.print.margin, y,
            card.border + card.width + card.print.border, y
        );
        // left cutting edge
        x = card.border + card.print.margin;
        pdf.line(
            x, card.border - card.print.border,
            x, card.border + card.print.margin
        );
        pdf.line(
            x, card.border + card.height * 2 - card.print.margin,
            x, card.border + card.height * 2 + card.print.border
        );
        x = card.border + card.width - card.print.margin;
        pdf.line(
            x, card.border - card.print.border,
            x, card.border + card.print.margin
        );
        pdf.line(
            x, card.border + card.height * 2 - card.print.margin,
            x, card.border + card.height * 2 + card.print.border
        );

        // add main card
        pdf.addImage(mapPics[0], 'PNG',
            card.border, card.border,
            card.map.near.width, card.map.near.height
        );

        // add near card
        /*
        pdf.addImage(mapPics[0], 'PNG',
            card.border, card.border + card.height + card.h1.height, // + card.map.far.height,
            card.map.far.width, card.map.far.height
        );
        */

        // add folding marker
        pdf.setDrawColor(150);
        y = card.border + card.height;
        pdf.line(
            card.border - card.print.border, y,
            card.border + card.width + card.print.border, y
        );

        // create copyright and main header background
        pdf.rect(
            card.border, card.border + card.height - card.copyright.height,
            card.width,  card.h1.height + card.h1.marginTB + card.copyright.height, 'F'
        );

        // add copyright
        pdf.setFontStyle(card.copyright.style); pdf.setFontSize(card.copyright.size);
        var today = new Date(); var year = today.getFullYear(); var month = today.getMonth()+1;
        pdf.text( year + "/" + month
            , card.border + card.h1.marginLR
            , card.border + card.height - card.copyright.marginTB
        );
          pdf.text( card.copyright.text
            , card.border + card.width - card.h1.marginLR
            , card.border + card.height - card.copyright.marginTB
            , 'right'
        );

        // add big header
        pdf.setFontStyle(card.h1.style); pdf.setFontSize(card.h1.size);
        pdf.text( congr.name
            , card.border + card.h1.marginLR
            , card.border + card.height + card.h1.height
            //, 'center'
        );
        pdf.text( header.territory + " " + terri.name
            , card.border + card.width - card.h1.marginLR
            , card.border + card.height + card.h1.height
            , 'right'
        );
        pdf.setDrawColor(0);
        y = card.border + card.height + card.h1.height + card.h1.marginTB;
        pdf.line( card.border ,             y
                , card.border + card.width, y
        );

        // add known addresses
        pdf.setFontStyle(card.streets.style);
        pdf.setFontSize(card.streets.size);
        pdf.setDrawColor(0);
        var line = 1;
        var xl = card.border + card.h1.marginLR;
        //var yu = card.border + card.height
        //       + card.h1.height + card.h1.marginTB + card.streets.marginTB ;
               //+ card.h1.height + card.p1.marginTB
               //+ card.p1.marginTB + card.p1.height;
        x = xl;
        y = y + card.streets.height;

        terri.streets.forEach(function(street) {
            pdf.text(line + "."  , xl                         , y );
            pdf.text(street.name , xl + card.streets.marginLR , y );
            if( document.getElementById('gen_tem_card_print_address').checked ) {
                pdf.text(street.info , xl + card.streets.width    , y );
            }
            // print text line
            pdf.line( card.border             , y + card.streets.marginTB
                    , card.border + card.width, y + card.streets.marginTB
            );
            y = y + card.streets.height + card.streets.marginTB;
            line++;
        });
        // add additional lines when space is not filled
        while ( line <= card.streets.lines ) {
            pdf.text(line + ". " , xl, y );
            pdf.line( card.border             , y + card.streets.marginTB
                    , card.border + card.width, y + card.streets.marginTB
            );
            y = y + card.streets.height + card.streets.marginTB;
            line++;
        }

        // print known addresses
        pdf.setFillColor(card.color.background);
        x = card.border;
        y = y - card.streets.height;
        //y = card.border + card.height + card.h1.height; // + card.streets.lines * card.streets.height;
        pdf.rect(   // main header background
            card.border , y,
            card.width, card.add_info.h1.height + card.add_info.h1.marginTB,'F'
        );
        pdf.line( card.border             , y
                , card.border + card.width, y
        ); // redraw last street lines
        x = x + card.h1.marginLR;
        y = y + card.add_info.h1.height;
        pdf.setFontStyle(card.add_info.h1.style); pdf.setFontSize(card.add_info.h1.size);
        var h_string = header.add_info;
        //      if ask line there info must come sooner, therefore abuse the bit value as number
        var h_look_inside = terri.info.length + Number(terri.ask);
        if ( h_look_inside  > card.add_info.breakline ) {
            h_string = h_string + " " + header.look_inside;
        }
        pdf.text(h_string, x, y );

        pdf.setFontStyle(card.p1.style); pdf.setFontSize(card.p1.size);
        pdf.setDrawColor(0);
        var line = 1;
        var i_string;

        //yu = card.border + card.height
        //   + card.h1.height + card.map.far.height
        //   + card.add_info.h1.height + card.add_info.h1.marginTB;
        y = y + card.add_info.h1.marginTB;
        pdf.line( card.border             , y
                , card.border + card.width, y
        ); // draw black border for add info header
        xl = card.border + card.h1.marginLR;
        y = y + card.p1.height;
        // add blod red mark to ask somebody
        if ( terri.ask === "1" || terri.ask == 1 ) {
            pdf.setFontType("bold"); pdf.setTextColor(255,0,0);
            pdf.text(header.ask, x, y );
            pdf.setFontType("normal"); pdf.setTextColor(0);
            pdf.line( card.border , y + card.p1.marginTB,
                      card.border + card.width, y + card.p1.marginTB
            );
            y = y + card.p1.height;
            line++;
        }
        var i_string;
        // add info from add_info table of database
        terri.info.forEach(function(info) {
            x = xl;
            // run through defined columns in territoriesaddinfo.json
            cn = 0;
            taicn.forEach(function(column) {
                if( column.name in card.add_info.column ) {
                    c_col = card.add_info.column[column.name];
                } else {
                    c_col = card.add_info.column["default"];
                }
                // merge string with separator and appendix and calculate the x position
                i_string = c_col.prefix + info[cn] + c_col.separator + c_col.suffix;
                // calculate position of text depending on alignment
                //   align left is not allowed by jspdf => workaround to avoid error
                var align = c_col.align;
                if(c_col.align === "right") {
                    x = x + c_col.width - card.p1.marginLR;
                } else if(c_col.align === "center") {
                    x = x + c_col.width / 2
                } else {
                    align = "";
                }
                //--------------------------------------------------------------------
                pdf.text(i_string, x, y, align );   // add text on calculated position
                //--------------------------------------------------------------------
                // calculate start position for next column
                if(align === "") {
                    x = x + c_col.width;
                } else if(c_col.align === "right") {
                    x = x + card.p1.marginLR;
                } else if(align === "center") {
                    x = x + c_col.width / 2
                }
                // increment column number
                cn++;
            });
            // print text line
            pdf.line( card.border             , y + card.p1.marginTB
                    , card.border + card.width, y + card.p1.marginTB
            );
            if ( line == card.add_info.breakline ) { y = y + 3 + card.p1.height; } // card end => add extra space for folding
            y = y + card.p1.height + card.p1.marginTB;
            line++;
        });

        // add additional lines when space is not filled
        while ( line <= 20 ) {
            pdf.line( card.border, y + card.p1.marginTB, card.border + card.width, y + card.p1.marginTB );
            if ( line == card.add_info.breakline ) { y = y + 3 + card.p1.height; } // card end => add extra space for folding
            y = y + card.p1.height;
            line++;
        }


        // pdf generation finished, enable button and remove cover
        cover.className = '';
        exportButton.disabled = false;

        // output the pdf --------------------------------------------------------------------------
        //pdf.output('dataurlnewwindow'); // data url is very long => firefox is getting very slow
        pdf.save(header.territory + "_" + terri.name + ".pdf"); // ~ 2.5 MB

    });


});
//--------------------------------------------------------------------------------------------------
