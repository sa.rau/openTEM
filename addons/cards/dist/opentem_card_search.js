/***************************************************************************************************
    opentem_maps_near := TEritory Management MAPS
****************************************************************************************************
    Copyright (c) 2017 Stephan Rau
    Licensed under the MIT license.
****************************************************************************************************
    creation date:  25.01.2017
    last change:    25.06.2017
***************************************************************************************************/

//--------------------------------------------------------------------------------------------------
// osm tile servers
//--------------------------------------------------------------------------------------------------
// connect to a tile server and connect to the leaflet plugin
   var osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';           // default one
// var osmUrl = 'http://{s}.tile.stamen.com/toner/{z}/{x}/{y}.png';             // high contrast
// var osmUrl = 'http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png';   // low contrast
// var osmUrl = 'http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png';
// var osmUrl = 'https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png';
// var osmUrl = 'http://{s}.tile.stamen.com/terrain/{z}/{x}/{y}.png';
// var osmUrl = 'http://{s}.tile.stamen.com/watercolor/{z}/{x}/{y}.png';
// var osmUrl = 'http://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png';
// var osmUrl = 'http://tile.openstreetmap.de/{z}/{x}/{y}.png';                 // not working
var osmAttrib = '&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a>';


//--------------------------------------------------------------------------------------------------
// create main map (near)
//--------------------------------------------------------------------------------------------------
var osm_near = new L.tileLayer(osmUrl, {maxZoom: card.map.near.zoom.max, attribution: osmAttrib});
var opentem_maps_near = new L.Map('map_near', {
        layers: [osm_near],
        preferCanvas: true,
        zoomControl: false,
        fullscreenControl: false    // do not load leaflet-fullscreen plugin
    });
    opentem_maps_near.dragging.disable();
    opentem_maps_near.touchZoom.disable();
    opentem_maps_near.doubleClickZoom.disable();
    opentem_maps_near.scrollWheelZoom.disable();
    opentem_maps_near.boxZoom.disable();
    opentem_maps_near.keyboard.disable();
//--------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------
// create second map with different zoom to get an overview, disable all mouse interaction
//--------------------------------------------------------------------------------------------------
var osm_far = new L.TileLayer(osmUrl, {minZoom: 0, maxZoom: card.map.far.zoom.max, attribution: osmAttrib});
var opentem_maps_far = new L.Map('map_far', {
        layers: [osm_far],
        preferCanvas: true,
        zoomControl: false,
        fullscreenControl: false    // load leaflet-fullscreen plugin
    });
    opentem_maps_far.dragging.disable();
    opentem_maps_far.touchZoom.disable();
    opentem_maps_far.doubleClickZoom.disable();
    opentem_maps_far.scrollWheelZoom.disable();
    opentem_maps_far.boxZoom.disable();
    opentem_maps_far.keyboard.disable();
//--------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------
// set map to initial state (default value or values from database)
//--------------------------------------------------------------------------------------------------
function opentem_maps_init(pArea,pCenter,pZoom_far,pZoom_near) {

    // set initial values of the far map
    var bounds = [[pCenter.lat-0.1, pCenter.lng-0.1,], [pCenter.lat+0.1, pCenter.lng+0.1]]
    opentem_maps_far.setView(pCenter,pZoom_far);

    // sent initial map position near map
    //   worldBounds needed to invert map mark of near map
    var worldBounds = [[90, -180], [90, 180], [-90, 180],[-90, -180]];
    opentem_maps_near.setView(pCenter,pZoom_near);

    // add area mark if available   TODO must be available ?
    if(pArea){
        L.polygon(
            [worldBounds,pArea],
            {'fillColor': '#ffffff', 'fillOpacity': 0.70}
        ).addTo(opentem_maps_near);
        L.polygon(pArea).addTo(opentem_maps_far);
    }

}
//--------------------------------------------------------------------------------------------------


//==================================================================================================
// convert shown maps to pdf powered by jspdf
//==================================================================================================

// get button and cover to hide web site while generating pdf
var exportButton = document.getElementById('gen_tem_card_pdf');
var cover = document.getElementById('cover');

// page just loaded enable button and do not cover page
exportButton.disabled = false;
cover.className = '';

// do when button was clicked
exportButton.addEventListener('click', function() {

    // disable button and cover page
    exportButton.disabled = true;
    cover.className = 'active';

    // promise is used because getting the tiles and generating a single picture takes some time
    Promise.all([
        new Promise(function (resolve){
            // generate picture of far map for pdf generation
            leafletImage(opentem_maps_far, function(err, canvas) {
                resolve(canvas.toDataURL("image/png"));
            });
        })
        ,
        new Promise(function (resolve){
            // generate picture of near map for pdf generation
            leafletImage(opentem_maps_near, function(err, canvas) {
                resolve(canvas.toDataURL("image/png"));
            });
        })
    ]).then(function (mapPics) {

        //------------------------------------------------------------------------------------------
        // generate printable pdf
        //------------------------------------------------------------------------------------------
        //debugger;

        // create pdf and set default Font
        var pdf = new jsPDF(card.paper.orientation, card.paper.units, card.paper.format);
        pdf.addFont (card.font.path, card.font.name, card.font.style, card.font.encoding);
        pdf.setFont (card.font.name);
        pdf.setFillColor(card.color.background);

        // add far card (overview card)
        pdf.addImage(mapPics[0], 'PNG',
            card.border, card.border + card.h1.height,
            card.map.far.width, card.map.far.height
        );

        // header background (for big header list)
        pdf.rect(
            card.border, card.border,
            card.width,  card.h1.height, 'F'
        );

        // add big header
        pdf.setFontStyle(card.h1.style); pdf.setFontSize(card.h1.size);
        pdf.text( header.congregation + " " + congr.name
            , card.border + card.h1.marginLR
            , card.border + card.h1.height - card.h1.marginTB
            //, 'center'
        );
        pdf.text( header.territory + " " + terri.name
            , card.border + card.width - card.h1.marginLR
            , card.border + card.h1.height - card.h1.marginTB
            , 'right'
        );

        // create copyright background
        pdf.setFillColor(card.color.background);
        pdf.rect(
            card.border,   card.border + card.h1.height
                         + card.map.far.height,
            card.width,    card.copyright.height
                         + card.h2.height, 'F'
        );

        // header background (for add_info list)
        pdf.setFillColor(card.color.background);
        pdf.rect(
            card.border,   card.border
                         + card.h1.height
                         + card.map.far.height
                         + card.copyright.height
                         + card.h2.height
                         + card.p1.height * card.streets.lines ,
            card.width,  card.h2.height, 'F'
        );

        // add copyright
        y = card.border + card.h1.height + card.map.far.height + card.copyright.height;
        pdf.setFontStyle(card.copyright.style); pdf.setFontSize(card.copyright.size);
        var today = new Date(); var year = today.getFullYear(); var month = today.getMonth()+1;
        pdf.text( year + "/" + month
            , card.border + card.h1.marginLR
            , y - card.copyright.marginTB
        );
          pdf.text( card.copyright.text
            , card.border + card.width - card.h1.marginLR
            , y - card.copyright.marginTB
            , 'right'
        );
        pdf.line(
            card.border, y,
            card.border + card.width, y
        );

        // add h2 highlight line
        y = y + card.h2.height;
        pdf.line(
            card.border, y,
            card.border + card.width, y
        );

        // add streets header text
        x = card.border + card.h1.marginLR;
        pdf.setFontStyle(card.h2.style); pdf.setFontSize(card.h2.size);
        pdf.text(header.streets, x, y - card.h2.marginTB );

        // add streets to empty space
        pdf.setFontStyle(card.p1.style); pdf.setFontSize(card.p1.size);
        var line = 0;
        x = card.border + card.h1.marginLR;
        yu = y;
        // add vertical line
        pdf.line(
            card.border + card.streets.width, y,
            card.border + card.streets.width, y + card.streets.lines * card.p1.height
        );
        terri.streets.forEach(function(street) {
            y = y + card.p1.height;
            if ( line < card.streets.lines ) {
                pdf.line( card.border, y, card.border + card.width, y );
            } else if ( line == card.streets.lines ) {
                y = yu + card.p1.height;
                x = x + card.streets.width;
            }

            pdf.text(street.name + ", " + street.info, x, y - card.p1.marginTB );

            line++;
        });
        while ( line < card.streets.lines ) {
            y = y + card.p1.height;
            pdf.line( card.border, y, card.border + card.width, y);
            line++;
        }
        y = yu + card.p1.height * card.streets.lines;

        // add_info field
        // header seperation line
        y = y + card.h2.height;
        pdf.line(
            card.border, y,
            card.border + card.width, y
        );

        // header text
        x = card.border + card.h1.marginLR;
        pdf.setFontStyle(card.h2.style); pdf.setFontSize(card.h2.size);
        pdf.text(header.add_info, x, y - card.h2.marginTB );
        // add blod red mark to ask somebody
        if ( terri.ask === "1" || terri.ask == 1 ) {
            x = card.border + card.h1.marginLR + card.streets.width;
            pdf.setFontType("bold"); pdf.setTextColor(255,0,0);
            pdf.text( header.ask
                , card.border + card.width - card.h1.marginLR
                , y - card.h2.marginTB
                , 'right'
            );
            pdf.setFontType("normal"); pdf.setTextColor(0);
        }

        // add_info lines
        pdf.setFontStyle(card.p1.style); pdf.setFontSize(card.p1.size); pdf.setDrawColor(0);
        var line = 1;
        var i_string; var i_string_prev = "";
        // add info from add_info table of database
        terri.info.forEach(function(info) {
            x = card.border + card.p1.marginLR;
            y = y + card.p1.height;
            // run through defined columns in territoriesaddinfo.json
            cn = 0;
            taicn.forEach(function(column) {
                if( column.name in card.add_info.column ) {
                    c_col = card.add_info.column[column.name];
                } else {
                    c_col = card.add_info.column["default"];
                }
                // merge string with separator and appendix and calculate the x position
                i_string = c_col.prefix + info[cn] + c_col.separator + c_col.suffix;
                // calculate position of text depending on alignment
                //   align left is not allowed by jspdf => workaround to avoid error
                if ( c_col.width < 0 ) {
                    i_string_prev += i_string;
                } else {
                    var align = c_col.align;
                    if(c_col.align === "right") {
                        x = x + c_col.width - card.p1.marginLR;
                    } else if(c_col.align === "center") {
                        x = x + c_col.width / 2
                    } else {
                        align = "";
                    }
                    //--------------------------------------------------------------------
                    // add text on calculated position
                    pdf.text(i_string_prev + i_string, x, y - card.p1.marginTB, align );
                    //--------------------------------------------------------------------
                    // calculate start position for next column
                    if(align === "") {
                        x = x + c_col.width;
                    } else if(c_col.align === "right") {
                        x = x + card.p1.marginLR;
                    } else if(align === "center") {
                        x = x + c_col.width / 2
                    }
                    i_string_prev = "";
                }
                // increment column number
                cn++;
            });
            // print text line
            pdf.line( card.border, y, card.border + card.width, y );
            line++;
        });
        // add additional lines when space is not filled
        while ( y < card.border + card.height ) {
            y = y + card.p1.height;
            pdf.line( card.border, y, card.border + card.width, y );
            line++;
        }

        // add second page with high resolution map ------------------------------------------------
        pdf.addPage();
        pdf.setFillColor(card.color.background);

        // add near card (overview card)
        pdf.addImage(mapPics[1], 'PNG',
            card.border, card.border + card.h1.height,
            card.map.near.width, card.map.near.height
        );

        // create header background
        pdf.rect(
            card.border, card.border,
            card.width,  card.h1.height, 'F'
        );

        // add big header
        pdf.setFontStyle(card.h1.style); pdf.setFontSize(card.h1.size);
        pdf.text( header.congregation + " " + congr.name
            , card.border + card.h1.marginLR
            , card.border + card.h1.height - card.h1.marginTB
            //, 'center'
        );
        pdf.text( header.territory + " " + terri.name
            , card.border + card.width - card.h1.marginLR
            , card.border + card.h1.height - card.h1.marginTB
            , 'right'
        );

        // create copyright background
        pdf.setFillColor(card.color.background);
        pdf.rect(
            card.border, card.border + card.h1.height + card.map.near.height,
            card.width,  card.copyright.height, 'F'
        );

        // add copyright
        y = card.border + card.h1.height + card.map.near.height + card.copyright.height;
        pdf.setFontStyle(card.copyright.style); pdf.setFontSize(card.copyright.size);
        var today = new Date(); var year = today.getFullYear(); var month = today.getMonth()+1;
        pdf.text( year + "/" + month
            , card.border + card.h1.marginLR
            , y - card.copyright.marginTB
        );
          pdf.text( card.copyright.text
            , card.border + card.width - card.h1.marginLR
            , y - card.copyright.marginTB
            , 'right'
        );

        // pdf generation finished, enable button and remove cover
        cover.className = '';
        exportButton.disabled = false;

        // output the pdf --------------------------------------------------------------------------
        //pdf.output('dataurlnewwindow'); // data url is very long => firefox is getting very slow
        pdf.save(header.territory + "_" + terri.name + ".pdf"); // ~ 2.5 MB

    });


});
//--------------------------------------------------------------------------------------------------
