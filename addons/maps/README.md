# README #

Teritorry Managements Maps

### What is this repository for? ###

* mark a territory on a map and save the markt area via php on at the server
* based on leaflet, which is based on openstreetmap

### How to create and run the code ###

Basicly tem_maps is an use case of the leaflet client frontend with a php server backend.
To create the client code node.js is needed with the client tool bower and the build tool grunt.
If you do not know the tools and you are using a debian based linux just type the following.
(I'd this all on virtual machine which serves also as test web server)


```
#!bash

# do as root
apt-get install nodejs

# nodejs installs also npm which can be used as normal user

# check out the current git repository and enter dir
git clone .../tem_maps.git
cd tem_maps

# then install needed npm packged (gets info from package.json)
npm install

# use bower front end package manger to install leaflet components
# (gets info from bower.json)
bower install

# use grunt to build and minimize all the javascript code
#(gets info from Gruntfile.js)
# the result of this will be saved in the folder dist
grunt dist

# to run the test code also a special folder must be created
# to store the send data on the server
mkdir db
chmod 777

```

/*
http://gis.stackexchange.com/questions/211496/leaflet-draw-add-attributes-and-save-to-file
https://leanpub.com/leaflet-tips-and-tricks/read
https://bl.ocks.org/danswick/d30c44b081be31aea483

// use table format instead of div sections
http://bootsnipp.com/snippets/featured/bootstrap-snipp-for-datatable
*/

<!--
			/*
            http://gis.stackexchange.com/questions/211496/leaflet-draw-add-attributes-and-save-to-file
            https://leanpub.com/leaflet-tips-and-tricks/read
            https://bl.ocks.org/danswick/d30c44b081be31aea483

            // use table format instead of div sections
            http://bootsnipp.com/snippets/featured/bootstrap-snipp-for-datatable
            */

	-->
