<!-- server side data handling of the map -->
<?php
    // simulated db (just a file in json format, which stores a single data set)
    $db = 'db/db.txt';

    // check if data was send
    if ( !empty($_POST) ) {
        // get send data from browser (client - server interface)
        $opentem_maps_data = [
            "mark"      => $_POST['mapmark'],
            "center"    => $_POST['mapcenter'],
            "zoom"      => $_POST['mapzoom']
        ];

        // get area data from db when just pos update (real db, just do not change area data)
        if( $opentem_maps_data["mark"] == "posUpdate" ) {
            if ( file_exists($db) ) {
                $tmp = json_decode(file_get_contents($db),true);
                $opentem_maps_data["mark"] = $tmp["mark"];
            } else {
                $opentem_maps_data["mark"] = null;
            }
        }

        file_put_contents($db, json_encode($opentem_maps_data));
    } else {
        // TODO Error
    }

?>
