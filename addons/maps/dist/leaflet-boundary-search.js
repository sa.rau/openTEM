// Author
// sources used as inspiration
//  blog
//  leaflet-custom-search
//	https://www.suche-postleitzahl.org/plz-gebiet/80538
// description
//
(function(){

    // extend the leaflet controls by a boundary search function
    L.Control.BoundarySearch = L.Control.extend({

        copyFunctionCallBack: function(x) {
            alert('calling the default copy call back' + x);
        },

        // leaflet build in option handler
        options: {
              buttonSearch : 'search'
            , buttonGet    : 'get'
            , position     : 'topright'
            , boundaryNotFoundText : 'The searched boundary could not be found.'
        },

        // leaflet constructor
        initialize: function(options) {
            L.Util.setOptions(this, options);
        },

        // function to add to map
        onAdd: function (map) {

            var copyCallBack = this.copyFunctionCallBack;
            var currentPolyLine = null;

            // create search field and related buttons
            var container = L.DomUtil.create('div');
            container.id = "controlcontainer";
            $(container).html(
                //    '<div class="input-group" id="overpass-api-controls">'
                '<from class="form-inline">'
                +   '<div class="from-group">'
                //+   '<span class="input-group_addon" id="query-title">PLZ Eingeben:</span>'
                +   '<input class="form-control" style="width: 5.0em;" id="query-text-field" placeholder="PLZ" size="10">'
                +   '<button type="submit" class="btn btn-success" id="query-button">'
                +       '<span class="glyphicon glyphicon-search"></span></button>'
                +   '<button type="submit" class="btn btn-primary" id="copy-button">'
                +       '<span class="glyphicon glyphicon-ok"></span></button>'
                +   '</div>'
                +   '</from>'
            );

            // add function to buttons
            setTimeout(function () {



                // query function: get boundary from overpass api and hilight found boundary
                $("#query-button").click(function () {

                    // remove old search result
                    map.eachLayer(function (layer) {
                        if ( layer.options.type == 'search' ) {
                            map.removeLayer(layer);
                        }
                    });

                    // build overpass query url
                    var queryTextFieldValue = $("#query-text-field").val();
                    var overpassApiUrl = 'https://overpass-api.de/api/interpreter'
                        + '?data=[out:xml][timeout:25];'
                        + 'rel[boundary=postal_code][postal_code="' + queryTextFieldValue + '"];'
                        + 'out body;>;out skel qt;'
                    ;

                    // run overpass query and process return value
                    $.get(overpassApiUrl, function (osmDataAsXml) {

                        // converting Xml data to json returned by overpass api
                        //   using json provided by overpass api would need more complicated
                        //   postprocessing to create a single polyline
                        var osmDataAsJson = osmtogeojson(osmDataAsXml);

                        // check if overpass found something
                        if  ( osmDataAsJson.features.length > 0 ) {

                            var polyline = [];
                            var geometry = osmDataAsJson.features[0].geometry;
                            switch (geometry.type) {
                                case "Polygon":
                                geometry.coordinates.forEach( function (polygon) {
                                    polygon.forEach( function(point) {
                                    polyline.push({"lat":point[1],"lng":point[0]});
                                    });
                                });
                                break;
                                case "MultiPolygon":
                                geometry.coordinates.forEach( function (multipolygon) {
                                    multipolygon.forEach( function(polygon) {
                                    polygon.forEach( function(point) {
                                        polyline.push({"lat":point[1],"lng":point[0]});
                                    });
                                    });
                                });
                                break;
                                default:
                                    // TODO show somehow an error
                                    alert("Error: unknown geometry type found!");
                                break;
                            }

                            // add found boundary and add user options type: search
                            //   to identify the layer later when it should be copied to the db
                            L.polygon( polyline
                                , { color: 'green', fillOpacity: 0.25, type:"search" }
                            ).addTo(map);
                            map.fitBounds(polyline);
                            currentPolyLine = polyline;

                        } else {
                            //alert(this.options.boundaryNotFoundText);
                            alert("PLZ nicht gefunden. Versuche eine andere.");
                        }

                    });

                });

                // copy function
                $("#copy-button").click(function () {
                    copyCallBack(currentPolyLine);
                });

            }, 1);


            L.DomEvent.disableClickPropagation(container);
            return container;
        }

    });

    // function to create a new leaflet BoundarySearch object
    L.boundarySearch = function(/* args will pass automatically */){
        var args = Array.prototype.concat.apply([L.Control.BoundarySearch],arguments);
        return new (Function.prototype.bind.apply(L.Control.BoundarySearch, args));
    };

})();