module.exports = function(grunt) {

 grunt.initConfig({
    //pkg: grunt.file.readJSON('package.json'),

    copy: {
      javascript: {
        files: [
          { expand: true, cwd: 'src', src: '*.js', dest: 'dist/' }
        ]
      }
    },

    //---------------------------------------------------------------------------------------------
    // modify bower components
    //---------------------------------------------------------------------------------------------
    'string-replace': {
      'leaflet-fullscreen': {
        // folder structure does not follow the bower requirement
        files: {
            'bower_components/leaflet-fullscreen/dist/leaflet.fullscreen.mod.css': 'bower_components/leaflet-fullscreen/dist/leaflet.fullscreen.css',
        },
        options: {
          replacements: [{
              pattern: /url\((fullscreen.*\.png)\)/g,
              replacement: "url('images/$1')"
          }]
        }
      },
      'leaflet-search': {
        // folder structure does not follow the bower requirement
        files: {
            'bower_components/leaflet-search/dist/leaflet-search.mod.css': 'bower_components/leaflet-search/dist/leaflet-search.src.css',
        },
        options: {
          replacements: [{
              pattern: /\.\.\/images\//g,
              replacement: "images/"
          }]
        }
      }
    },

    //---------------------------------------------------------------------------------------------
    // merge bower components to single file
    //---------------------------------------------------------------------------------------------
    bower_concat: {
      all: {
        dest: {
          'js': 'dist/opentem_maps_plugins.js',
          'css': 'dist/opentem_maps.css'
        },
        mainFiles: {
          // upper lower case breaks bower_concat
            'leaflet-fullscreen': ['dist/Leaflet.fullscreen.js', 'dist/leaflet.fullscreen.css']
          , 'leaflet-search': ['dist/leaflet-search.src.js', 'dist/leaflet-search.mod.css']
          , 'leaflet-minimap': ['dist/Control.MiniMap.min.js', 'dist/Control.MiniMap.min.css']
          , 'osmtogeojson' : ['osmtogeojson.js']
        },
        callback: function(mainFiles, component) {
          // use modified files form string-replace if available
          return mainFiles.map(function(filepath) {
            var mod = filepath.replace(/\.(js|css)$/, '.mod.$1');
            return grunt.file.exists(mod) ? mod : filepath;
          });
        },
        bowerOptions: {
          relative: false
        }
      }
    },

    //---------------------------------------------------------------------------------------------
    // copy images for distribution
    //---------------------------------------------------------------------------------------------
    bowercopy: {
      default: {
        files: {
          'dist/images': '*/dist/images/*'
        }
      },
      'leaflet-fullscreen': {
        // folder structor does not follow the bower requirement
        files: {
          'dist/images': 'leaflet-fullscreen/dist/*.png'
        }
      },
      'leaflet-search': {
        // folder structor does not follow the bower requirement
        files: {
          'dist/images': ['leaflet-search/images/search-icon*.png', 'leaflet-search/images/*.gif']
        }
      }
    },

    //---------------------------------------------------------------------------------------------
    // minimize javascript codes for faster browser load
    //---------------------------------------------------------------------------------------------
    uglify: {
      opentem_maps_plugins: {
        src:  'dist/opentem_maps_plugins.js',
        dest: 'dist/opentem_maps_plugins.min.js'
      },
      leafletBoundarySearch: {
        src:  'dist/leaflet-boundary-search.js',
        dest: 'dist/leaflet-boundary-search.min.js'
      },
      opentem_maps: {
        src:  'dist/opentem_maps.js',
        dest: 'dist/opentem_maps.min.js'
      }
    }

  });

  grunt.loadNpmTasks('grunt-string-replace');
  grunt.loadNpmTasks('grunt-bowercopy');
  grunt.loadNpmTasks('grunt-bower-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.registerTask('dist', ['string-replace', 'bower_concat', 'bowercopy', 'copy', 'uglify']);

};
