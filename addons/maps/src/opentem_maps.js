/***************************************************************************************************
    opentem_maps := TEritory Management MAPS
****************************************************************************************************
    Copyright (c) 2017 Stephan Rau
    Licensed under the MIT license.
****************************************************************************************************
    creation date:  25.01.2017
    last change:    25.06.2017
***************************************************************************************************/

/* get postal code

    overpass-turbo.eu
    rel[boundary=postal_code][postal_code="80331"];
    out geom;

    wget 'http://overpass-api.de/api/interpreter?data=[out:json];rel[boundary=postal_code][postal_code="80331"];out geom;'

    http://blog-en.openalfa.com/how-to-query-openstreetmap-using-the-overpass-api

    https://github.com/perliedman/query-overpass/blob/master/index.js
    https://github.com/plepe/overpass-frontend

*/


//--------------------------------------------------------------------------------------------------
// basic map settings, just to show the map and add the fullscreen option
//--------------------------------------------------------------------------------------------------
// TODO tmap_basic ? when also used for user to  just show territory
var osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';

var osmAttrib = '&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors';

var osm = new L.tileLayer(osmUrl, {maxZoom: 19, attribution: osmAttrib});

var opentem_maps = new L.Map('map', {
        layers: [osm],
        zoomSnap: 1, //0.25,   smaller zoom steps no possible to get image with leaflet-image
        zoomDelta: 1, // 0.25,
        fullscreenControl: true    // load leaflet-fullscreen plugin
    });
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// create and add a search menue
//--------------------------------------------------------------------------------------------------
var searchControl = new L.Control.Search({
        url: 'https://nominatim.openstreetmap.org/search?format=json&q={s}',
        jsonpParam: 'json_callback',
        propertyName: 'display_name',
        propertyLoc: ['lat','lon'],
        position: 'topleft',
        marker: L.circleMarker([0,0],{color: '#f00'})
});
opentem_maps.addControl(searchControl);
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// create and add a boundary search menue
//--------------------------------------------------------------------------------------------------
var boundarySearchControl = new L.boundarySearch({
          buttomSearch : 'suchen'
        , buttomCopy   : 'kopieren'
        , position     : 'bottomright'
});
boundarySearchControl.copyFunctionCallBack = function (foundPolyLine) {
	//var message = "Geänderte Gebietsmakierung wurde gespeichert.";
	if (!foundPolyLine) {
		//message = "PLZ nicht gefunden."
		alert("PLZ nicht gefunden.");
	} else {
		// remove old search result
		opentem_maps.eachLayer(function (layer) {
			if ( layer.options.type == 'search' ) {
				opentem_maps.removeLayer(layer);
			}
		});

		// remove old saved polyline
		drawnItems.eachLayer(function (layer) {
			drawnItems.removeLayer(layer);
		});

		// add new polyline from found boundary
        L.polygon(foundPolyLine).addTo(drawnItems);

		// send poly line to server
		sendMapMark(JSON.stringify(foundPolyLine));

		// remove create polygon button
		opentem_maps_disableCreatePolygon();
	}
	//alert(message);

}
opentem_maps.addControl(boundarySearchControl);
//--------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------
// create and add the show edit menu
//--------------------------------------------------------------------------------------------------

// menu to add, modify and delete an area
var drawnItems = new L.FeatureGroup();
opentem_maps.addLayer(drawnItems);
L.drawLocal.draw.toolbar.buttons.polygon = 'Gebietsmakierung einf\u00fcgen'; // TODO language pack
var drawControl = new L.Control.Draw({
    position: 'topright',
    draw: {
        polyline: false,
        polygon: true,      // add if some useful and needed information can be added
        rectangle: false,
        circle: false,      // add if some useful and needed information can be added
        marker: false       // add if some useful and needed information can be added
    },
    edit: {
        featureGroup: drawnItems,
        remove: true
    }
});
opentem_maps.addControl(drawControl);

// menu to save current map position and zoom value
var savePositionControl = new L.easyButton(
    '<span class="glyphicon glyphicon-floppy-disk" style="transform: scale(1.5) translate(0.5px,1px)"></span>',
     function(btn, map){
        sendMapMark("posUpdate");
    }, {
        // leaflet default options
        position: 'topright'
    }
);
//--------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------
// send data to server
//--------------------------------------------------------------------------------------------------
function sendMapMark(area) {

    // get current position from map
    var mapPos = getPosition();

    // show a status for the user
    //window.document.getElementById('status').innerHTML = "Sending data to the server.";

    // sen data to server
    r = new XMLHttpRequest();
    r.open('POST', 'maps_db.php', true);
    r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    r.send('mapmark='+area+'&mapcenter='+mapPos.center+'&mapzoom='+mapPos.zoom+'&ID='+tid);

    // remove save button whenever something was send to the server
    opentem_maps.removeControl(savePositionControl);
}
//--------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------
// handle changes on markup layer
//--------------------------------------------------------------------------------------------------
//--- area markup created
opentem_maps.on(L.Draw.Event.CREATED, function(event) {
    var layer = event.layer;
    sendMapMark(JSON.stringify(layer.getLatLngs()));
    drawnItems.addLayer(layer);
    // remove polygon toolbar and show edit and delete toolbar instead
    opentem_maps_disableCreatePolygon();
});
//--- area markup changed
opentem_maps.on(L.Draw.Event.EDITED, function(event) {
    var layers = event.layers;  // is only one layer, but inside a array
    layers.eachLayer(function(layer) {
        sendMapMark(JSON.stringify(layer.getLatLngs()));
    });
});
//--- area markup deleted
opentem_maps.on(L.Draw.Event.DELETED, function(event) {
    sendMapMark(null);
    // if layer is removed show polygon and hide edit & delete
    opentem_maps_enableCreatePolygon();
});
//--- remove polygon option
function opentem_maps_disableCreatePolygon(){
    drawControl.setDrawingOptions({
        polygon:false
    });
    opentem_maps.removeControl(drawControl);
    opentem_maps.addControl(drawControl);
}
//--- add polygon option
function opentem_maps_enableCreatePolygon(){
    drawControl.setDrawingOptions({
        polygon:true
    });
    opentem_maps.removeControl(drawControl);
    opentem_maps.addControl(drawControl);
}
//--------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------
// handle map movement and zoom change
//--------------------------------------------------------------------------------------------------
//--- savePositionControl button pressed
function getPosition(){
    var mapCenter = JSON.stringify(opentem_maps.getCenter());
    var mapZoom = JSON.stringify(opentem_maps.getZoom());
    return {
        center: mapCenter,
        zoom: mapZoom
    };
}
// show save button when drag or zoom change starts
opentem_maps.addEventListener('dragstart', function(event) {
    opentem_maps.addControl(savePositionControl);
});
opentem_maps.addEventListener('zoomend', function(event) {
    opentem_maps.addControl(savePositionControl);
});
//--------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------
// set map to initial state (default value or values from database)
//--------------------------------------------------------------------------------------------------
function opentem_maps_init(pArea,pCenter,pZoom) {

    // sent initial map position and zoom value
    opentem_maps.setView(pCenter,pZoom);

    // add area mark if available
    if(pArea){
        L.polygon(pArea).addTo(drawnItems);
        opentem_maps_disableCreatePolygon();
    }

    // remove save button, only needed after changes
    opentem_maps.removeControl(savePositionControl);

    // add map mark for subterries, when subterries array exists
    if (typeof subterri != "undefined" ) {
        subterri.forEach( function(item) {
            // draw polygon
            var polygon = L.polygon( item.mark
                , { color: 'orange', fillOpacity: 0.25, type:"subterri" }
            ).addTo(opentem_maps);
            // add subterri name to map
            polygon.bindTooltip(item.name
                , {permanent: true, direction:"center"}
            ).openTooltip();
        });
    }

}
//--------------------------------------------------------------------------------------------------
