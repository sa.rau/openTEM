<!DOCTYPE html>
<html>
<head>
    <title>opentem-maps</title>
    <!-- Bootstrap core CSS and some custom styles -->
    <link href="./design/css/bootstrap.css" rel="stylesheet">
    <link href="./design/css/theme.bootstrap.css"  rel="stylesheet">
    <script src="./design/js/jquery.js"></script>
    <!-- include merged and minified leaflet plugins needed form opentem_maps -->
    <link rel="stylesheet" href="dist/opentem_maps.css" />
    <script src="dist/opentem_maps_plugins.js"></script>
	<script src="src/leaflet-boundary-search.js"></script>

</head>
<body>

    <h1>Select Map Type</h1>

    <?php
        if( isset($_REQUEST['maptype']) ) {
            $maptype = $_REQUEST['maptype'];
        } else {
            $maptype = "card_regular";
        }
    ?>


    <form>
        <div class="form-group"> <div class="input-group">
            <span class="input-group-addon">Map Type</span>
            <select class="form-control" name=maptype onchange="this.form.submit()">
                <option value="card_regular"<?php if($maptype == "card_regular") {print " selected";} ?> >regular</option>
                <option value="card_search" <?php if($maptype == "card_search") {print " selected";} ?> >search</option>
                <option value="card_root"   <?php if($maptype == "card_root") {print " selected";} ?> >root</option>
            </select>
        </div></div>
    </form>


    <?php
        // for debugging on webserver -------
        ini_set('display_startup_errors',1);
        ini_set('display_errors',1);
        error_reporting(-1);
        //-----------------------------------

        // define printed card dimension in mm
        $json_string = file_get_contents("./config/" . $maptype . ".json");
        $card = json_decode($json_string, true);

        // calculate map dimension in px
        $webpage_width_px = 900;
        $width_px = $webpage_width_px;
        $height_px = ($width_px / $card["map"]["near"]["width"]) * $card["map"]["near"]["height"];
        $style = 'style="width: ' . $width_px . 'px; height: ' . $height_px . 'px;" ' ;
    ?>

    <!-- this sets the map window size -->
    <div id="map" <?php echo $style ?>></div>

    <!-- server side data handling of the map -->
    <?php

        print "<script>\n";
        print "var subterri = [];\n";
        foreach ( array('81539', '81541', '81543', '81545', '81547', '81549') as $plz ) {
            $db = "db/$plz.txt";
            if( file_exists($db) ) {
                $subterri = json_decode(file_get_contents($db),true);
                print "subterri.push({"
                    . "name:\"$plz\","
                    . "center:$subterri[center],"
                    . "mark:$subterri[mark]"
                ."});\n";
            }
        }
        print "</script>\n";

        // simulated db (just a file in json format, which stores a single data set)
        $db = 'db/db.txt';

        // just opend the page
        if ( file_exists($db) ) {
            // read file
            // TODO check if readable and proper json format
            $opentem_maps_data = json_decode(file_get_contents($db),true);
        } else {
            // load some default values
            $opentem_maps_data = [
                "mark"      => null,
                "center"    => '{"lat":48.0995,"lng":11.5751}',
                "zoom"      => 15
            ];
        }

    ?>

    <!-- load opentem_maps fontend addon and build server - client interface -->
    <script>
        var tid     = 1;
        var card    = <?php echo json_encode($card)     ;?>;
    </script>
    <script src="src/opentem_maps.js"></script>
    <script> <?php // use php to write the javascript code

        //print( 'var linked_terri = ' . json_encode($opentem_subterri)  . "\n" );

        // null is an empty string, but for the javascript function a "null" string is needed
        if( !isset($opentem_maps_data["mark"]) ) {
            $opentem_maps_data["mark"] = 'null';
        }
        print(
            "\n        opentem_maps_init($opentem_maps_data[mark],$opentem_maps_data[center],$opentem_maps_data[zoom]);\n\n"
        );

        /*
        foreach($opentem_subterri as $name => $mapopt) {
            print( "        opentem_maps_add_mark(\"$name\", $mapopt[mark],$mapopt[center]);\n" );
        }
        */

    ?> </script>

</body>
</html>
