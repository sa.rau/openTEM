###########################################################################################
#
# openTEM webserver image: PHP with Apache and sqlite as database
#

FROM php:7.4-apache

# install vim
RUN apt update && apt -y upgrade
RUN apt install -y vim


RUN apt-get install -y libgd3 libgd-dev && rm -rf /var/lib/apt/lists/*
RUN docker-php-ext-configure gd --with-freetype --with-jpeg
RUN docker-php-ext-install -j$(nproc) gd


# install xdebug for development
RUN pecl install xdebug
RUN echo "zend_extension=`find /usr/local/lib -name '*xdebug.so'`\n"\
    "xdebug.mode=debug\n"\
    "xdebug.start_with_request=yes\n"\
    "xdebug.client_host=192.168.1.20"\
    > /usr/local/etc/php/conf.d/php-xdebug-ext.ini

# mount instead of copy or git clone
#COPY . /var/www/html/
