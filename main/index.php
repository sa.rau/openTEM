<?php session_start();?>

<?php
/***************************************************************************************************

 Author:    Stephan Rau

 Purpose:   defines the base layout and include other sites as php code

***************************************************************************************************/
ob_start();

/* debugging on webserver -----------
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
   ----------------------------------- */
//xdebug_break();
// include classes and start a session -------------------------------------------------------------
require_once( "./class/Database.php"        );
require_once( "./class/User.php"            );
require_once( "./class/OwnerNurse.php"      );
require_once( "./class/TerritoryNurse.php"  );
require_once( "./class/GroupNurse.php"      );
require_once( "./class/TypeNurse.php"       );

$pathway = "";

$user = User::sessionRelogin();
$action = filter_input(INPUT_GET, 'action');

if( isset($user) && $user->isLoggedIn() ) {

    $groups = GroupNurse::startDialogue();  // create before OwnerNurse, because she talks to
    $types  = TypeNurse::startDialogue();   // create before OwnerNurse, because she talks to
    $owners = OwnerNurse::startDialogue();
    $terris = new TerritoryNurse('terri');

    if( !isset($action) ) {
        $action = 'blog';
    } else {
        $step = filter_input(INPUT_GET, 'step');
    }
} else {

    if( isset($action) ) {
        $step = filter_input(INPUT_GET, 'step');
    } else {
        $action = 'login' ;
        $step   = 'enter' ;
    }
}

// load common congregation information
$json_string = file_get_contents("./config/congregation.json");
$congr = json_decode($json_string, true);

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>openTEM | <?php print $congr["nickname"] ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="design/images/favicon.ico">

    <script src="./design/js/jquery.js"></script>

    <!-- Bootstrap core CSS -->
    <link href="./design/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <!-- <link href="./design/css/simple-sidebar.css"   rel="stylesheet"> -->
    <link href="./design/css/theme.bootstrap.css"  rel="stylesheet">

    <!-- include merged and minified leaflet plugins needed form tem_maps TODO only when edit map-->
    <?php if( ($action == 'terri') && ($step == TerritoryNurse::EDIT) ) {
    print '<link rel="stylesheet" href="./addons/js/maps/opentem_maps.css" />'
        . '<script src="./addons/js/maps/opentem_maps_plugins.min.js"></script>'
        . '<script src="./addons/js/maps/leaflet-boundary-search.min.js"></script>'
    ;
    }?>

  </head>

  <body>

      <!-- Navbar -->
      <nav class="navbar-fixed-top navbar-inverse">
        <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <!-- creates lines in button when small width -->
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="navbar-brand">openTEM</div>
          <!-- Center Message TODO add conregation name from config -->
          <div class="navbar-center"><?php print $congr["nickname"]; ?></div>
        </div> <!-- navbar-header -->
        <div class="navbar-collapse collapse">
          <!-- Left side Links -->
          <ul class="nav navbar-nav">
            <?php if( isset($user) && $user->isLoggedIn() ) {
                print "<li class=\"dropdown\">
                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Formulare<b class=\"caret\"></b></a>
                    <ul class=\"dropdown-menu\">
                        <li class=\"dropdown-header\">Gebiet</li>";
                        $terris->printLink(TerritoryNurse::ADD     );
                        $terris->printLink(TerritoryNurse::EDIT    );
                print " <li role=\"separator\" class=\"divider\"></li>
                        <li class=\"dropdown-header\">Halter</li>";
                        $owners->printLink(OwnerNurse::ADD          );
                        $owners->printLink(OwnerNurse::EDIT         );
                print " <li role=\"separator\" class=\"divider\"></li>
                        <li class=\"dropdown-header\">Halter Typen</li>";
                        $types->printLink(TypeNurse::CREATE_FORM  );
                        $types->printLink(TypeNurse::SELECT_FORM  );
                print " <li role=\"separator\" class=\"divider\"></li>
                        <li class=\"dropdown-header\">Gruppen</li>";
                        $groups->printLink(GroupNurse::CREATE_FORM  );
                        $groups->printLink(GroupNurse::SELECT_FORM  );
                print " </ul></li>";

                print "<li class=\"dropdown\">
                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Berichte<b class=\"caret\"></b></a>
                    <ul class=\"dropdown-menu\">
                        <li class=\"dropdown-header\">Gebiet</li>";
                        $terris->printLink(TerritoryNurse::ALL            );
                        $terris->printLink(TerritoryNurse::ASSIGNED       );
                        $terris->printLink(TerritoryNurse::FREE           );
                        $terris->printLink(TerritoryNurse::REPORT         );
                        $terris->printLink(TerritoryNurse::ADD_INFO       );
                        $terris->printLink(TerritoryNurse::CARDS          );
                print " <li role=\"separator\" class=\"divider\"></li>
                        <li class=\"dropdown-header\">Halter</li>";
                        $owners->printLink(OwnerNurse::ALL              );
                print " </ul></li>";

            } ?>
          </ul>

          <!-- Right side Links -->
          <ul class="nav navbar-nav navbar-right">
          <?php if( isset($user) && $user->isLoggedIn() ) {
              print "<li class=\"dropdown\">
                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">$_SESSION[login] <b class=\"caret\"></b></a>
                <ul class=\"dropdown-menu\">
                    <li><a href=\"index.php?action=blog\">Blog</a></li>";
               if( $user->hasAdminRight() ) { // check if teritorry menu should be shown
                 print "<li><a href=\"addons/php/phpliteadmin/phpliteadmin.php\" target=\"_blank\">phpLiteAdmin</a></li>";
               }
               print "<li><a href=\"index.php?action=profil\">Profil ändern</a></li>
                    <li><a href=\"index.php?action=logout\">Abmelden</a></li>
                </ul></li>";
          } else {
            // echo '<li class="active"><a>Log In</a></li>' ;
          } ?>
            <li role="presentation">
                <a target="_blank" href="https://gitlab.com/sa.rau/openTEM/wikis/home">
                    <span class="glyphicon glyphicon-info-sign"></span>
                </a>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
        </div>
      </nav><!-- navbar -->


      <!-- Main component ====================================================================== -->
      <div class="container">
      <?php
        // TODO check if logged in
        switch( $action ) {
        case 'login':
            switch( $step ) {

                case "verify" :
                    $user = User::login();
                break;

                case "enter":
                default:
                    User::printLogin();
                break;
            }
        break;

        case 'blog':
            include($pathway . "blog.php");
        break;

        case 'owner':

            switch( $step ) {
                case OwnerNurse::EDIT:

                    $owners->updateDatabase();      // TODO somehow strange here

                    $owners->printOwnerDropdown();

                    if( $owners->isSelected() ) {   // if owner selected
                        $owners->printAssignField();
                        $owners->printFieldRecords();
                        $owners->printEditOwner();
                    }
                break;

                case OwnerNurse::ADD:
                    $owners->printAddOwner(); // TODO step EDIT ???
                break;

                default:
                    $owners->printReport($step);
                break;
            }

        break;

        case 'terri':

            switch( $step ) {
                case TerritoryNurse::EDIT:
                    $terris->evalPhpParameter();
                    $terris->updateDatabase();
                    $terris->printDropdown();
                    if( $terris->definedId() ) {
                        $terris->printTimeEntries();
                        $terris->printEdit();
                        $terris->printLinkedTerries();
                        $terris->printEditStreets();
                        $terris->printAddInfo();
                    }
                break;

                case TerritoryNurse::ADD:
                    $terris->printAdd();
                break;

                default:
                    $terris->printReport($step);
                break;
            }

        break;

        case GroupNurse::ACTION :
            switch( $step ) {
                case GroupNurse::CREATE:
                    $groups->createNew();
                case GroupNurse::CREATE_FORM:
                    $groups->printCreateFrom();
                break;

                case GroupNurse::SELECT_FORM:
                    $groups->printDropDown();
                break;

                case GroupNurse::UPDATE:
                    $groups->update();
                case GroupNurse::UPDATE_FORM:
                    $groups->printDropDown();
                    $groups->printUpdateFrom();
                break;


                default:
                break;
            }
        break;

        case TypeNurse::ACTION :
            switch( $step ) {
                case TypeNurse::CREATE:
                    $types->createNew();
                case TypeNurse::CREATE_FORM:
                    $types->printCreateFrom();
                break;

                case TypeNurse::SELECT_FORM:
                    $types->printDropDown();
                break;

                case TypeNurse::UPDATE:
                    $types->update();
                case TypeNurse::UPDATE_FORM:
                    $types->printDropDown();
                    $types->printUpdateFrom();
                break;

                default:
                break;
            }
        break;

        case 'profil':
            if( !isset($step) ) {
                $step = 1 ;
            }
            switch( $step ) {
                case 2 :
                    $user->changePw();
                break;
                default:
                    if (isset ($_GET["fehler"])) {
                        print '<div class="alert alert-warn">Die Passwörter stimmen nicht überein.'
                            . 'Bitte versuchen Sie es erneut.</div>';
                    }
                    $user->printChangePw();
                break;
            }
        break;

        case 'logout' :
            if( isset($user) ) {
                $user->logout();
            } else {
                header("Location: index.php?action=login");
            }
        break;

        default:
            include($pathway . "404.php");
        break;
        }
      ?>
      </div>
      <!-- ===================================================================================== -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="./design/js/jquery.js"></script> -->
    <script src="./design/js/bootstrap.min.js"></script>

    <!-- load table sorter for report sites -->
    <?php
     if( (($action == 'terri') && ($step != TerritoryNurse::EDIT))
     ||  (($action == 'owner') && ($step == OwnerNurse::ALL))       ) { print
          '<script src="./design/js/jquery.tablesorter.combined.js"></script>' . "\n"
        . '<script src="./addons/js/tablesorter.js"></script>'
    ;}?>

    <!-- load tem_maps frontend addon and build server - client interface -->
    <?php if( ($action == 'terri') && ($step == TerritoryNurse::EDIT) ) { print
          '<script src="./addons/js/maps/opentem_maps.min.js"></script>' . "\n"
        . '<script>opentem_maps_init(mark,center,zoom);</script>'
    ;}?>

  </body>
</html>
