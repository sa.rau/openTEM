

    <!-- include merged and minified leaflet plugins needed form tem_maps                        -->
    <link rel="stylesheet" href="addons/js/cards/opentem_card.css" />
    <script src="addons/js/cards/opentem_card_plugins.js"></script>
    <script src="addons/js/cards/leaflet-address-marker.js"></script>
    <!-- add some css to hide page while generating the pdf                                      -->
    <style>
        #cover {
            display: none;      text-align: center;     padding-top: 300px;     background: #CCC;
            opacity: 0.5;       position: absolute;     top: 0;                 left: 0;
            width: 100%;        height: 100%;           z-index: 10000;
            font-family: Verdana, sans-serif;           font-size: 30px;
        }
        #cover.active{
            display: block;     cursor: wait;
        }
    </style>

    <!-- ======================================================================================= -->
    <?php
        //------------------------------------------------------------------------------------------
        // configuration and sample data usually provided by a database e.g. mySQL or SQLite
        //------------------------------------------------------------------------------------------

        // load info from database
        $t_row = $this->db->getTerri($this->id);

        // headers (language depended)
        $json_string = file_get_contents("./config/header.json");
        $header = json_decode($json_string, true);

        // common congregation information
        $json_string = file_get_contents("./config/congregation.json");
        $congr = json_decode($json_string, true);

        // define printed card dimension in mm

        switch ( $t_row["col_type"] ) {
            case Territory::TYPE['ADDRESS']:
                $card_config_file = "./config/card_addr.json";
                break;
            case Territory::TYPE['SEARCH']:
                $card_config_file = "./config/card_search.json";
                break;
            case Territory::TYPE['ROOT']:
            case Territory::TYPE['REGULAR']:
            default:
                $card_config_file = "./config/card_regular.json";
                break;
        }
        $json_string = file_get_contents($card_config_file);
        $card = json_decode($json_string, true);

        // define printed card additional field infos
        $json_string = file_get_contents("./config/territoriesaddinfo.json");
        $taicn = json_decode($json_string, true); // Territory Additional Information Column Name

        // calculate map dimension in px
        $webpage_width_px = 900 ;
        $near_width_px = $card["map"]["near"]["width"] * $card["map"]["near"]["dpmm"];
        $near_height_px = $card["map"]["near"]["height"] * $card["map"]["near"]["dpmm"];
        $near_scale = $webpage_width_px / $near_width_px;
        $near_translate_x_px = ($webpage_width_px - $near_width_px) / 2 / $near_scale ;
        $near_translate_y_px = ($webpage_width_px * $near_height_px / $near_width_px - $near_height_px) / 2 / $near_scale ;

        $webpage_width_px = $webpage_width_px / $card["map"]["near"]["width"] * $card["map"]["far"]["width"] ;
        $far_width_px = $card["map"]["far"]["width"] * $card["map"]["far"]["dpmm"];
        $far_height_px = $card["map"]["far"]["height"] * $card["map"]["far"]["dpmm"];
        $far_scale = $webpage_width_px / $far_width_px;
        $far_translate_x_px = ($webpage_width_px - $far_width_px) / 2 / $far_scale ;
        $far_translate_y_px = ($webpage_width_px * $far_height_px / $far_width_px - $far_height_px) / 2 / $far_scale ;


        $map_style = array(
            "near"  => 'style="'.
                'width: '  . $near_width_px . 'px; '.
                'height: ' . $near_height_px . 'px; ' .
                'transform: scale(' . $near_scale . ')' .
                            'translate(' . $near_translate_x_px . 'px, ' . $near_translate_y_px . 'px);"'
            ,
            "far"  => 'style="'.
                'width: '  . $far_width_px . 'px; '.
                'height: ' . $far_height_px . 'px; ' .
                'transform: scale(' . $far_scale . ')' .
                            'translate(' . $far_translate_x_px . 'px, ' . $far_translate_y_px . 'px);"'
        );

        // get streets and add info
        $streets = array ();
        $info = array ();
        switch($t_row["col_type"]) {
            case Territory::TYPE['ROOT']:
            case Territory::TYPE['ADDRESS']:
                foreach ( $this->db->getLinkedTerries($this->id) as $terri ) {
                    $tempTerri = $this->db->getTerri($terri['ID']);
                    $this->db->getStreetsByTerri($terri['ID']);
                    while($row = $this->db->fetch()){
                        $streets[] = [
                            'name'   =>  $tempTerri["col_place"] . ", " . $row["col_street"]
                          , 'info'   => $row["col_note"]
                        ];
                    }
                    $this->db->getTerriAddInfo($terri['ID']);
                    while($row = $this->db->fetch()){
                        $info[] = json_decode($row["col_infojson"], true);
                    }
                }
                break;
            case Territory::TYPE['SEARCH']:
            case Territory::TYPE['REGULAR']:
            default:
                $this->db->getStreetsByTerri($this->id);
                while($row = $this->db->fetch()){
                    $streets[] = [
                        'name'   => $row["col_street"]
                      , 'info'   => $row["col_note"]
                    ];
                }
                $this->db->getTerriAddInfo($this->id);
                while($row = $this->db->fetch()){
                    $info[] = json_decode($row["col_infojson"], true);
                }
                break;
        }

        // get common territory info and do some name mapping
        $terri = array (
            "id"        => $t_row["ID"],
            "name"      => $t_row["col_name"],
            "flats"     => $t_row["col_flats"],
            "blockpart" => $t_row["col_blockpart"] . "%",
            "streets"   => $streets,
            "info"      => $info,
            "ask"       => $t_row["col_ask"]
        );

    ?>
    <!-- ======================================================================================= -->


    <!-- ======================================================================================= -->
    <!--     web site which is representing the data of the printable pdf and looks alike        -->
    <!-- ======================================================================================= -->
    <?php switch($t_row["col_type"]) {
        case Territory::TYPE['ROOT']:
            include ( "./addons/js/cards/opentem_card_root.php");
            $js_card_script = "addons/js/cards/opentem_card_root.js";
            break;
        case Territory::TYPE['ADDRESS']:
            include ( "./addons/js/cards/opentem_card_addr.php");
            $js_card_script = "addons/js/cards/opentem_card_addr.js";
            break;
        case Territory::TYPE['SEARCH']:
            include ( "./addons/js/cards/opentem_card_search.php");
            $js_card_script = "addons/js/cards/opentem_card_search.js";
            break;
        case Territory::TYPE['REGULAR']:
        default:
            include ( "./addons/js/cards/opentem_card_regular.php");
            $js_card_script = "addons/js/cards/opentem_card_regular.js";
            break;
    } ?>
    <!-- ======================================================================================= -->


    <!-- ======================================================================================= -->
    <!-- server side data handling of the map                                                    -->
    <!-- ======================================================================================= -->
    <?php
        // just opened the page
        if ( isset($t_row["col_mapmark"]) ) {
            // read file - TODO check if readable and proper json format
            $opentem_maps_data = json_decode($t_row["col_mapmark"], true);
        } else {
            // load some default values
            $opentem_maps_data = [
                "mark"      => null,
                "center"    => json_encode($congr["center"]),
                "zoom"      => json_encode($congr["zoom"])
            ];
        }
    ?>
    <!-- ======================================================================================= -->


    <!-- ======================================================================================= -->
    <!-- load tem_maps frontend addon the map                                                    -->
    <!-- ======================================================================================= -->
    <!-- save server side data in java script variable                                           -->
    <script>
        var header  = <?php echo json_encode($header)   ;?>;
        var congr   = <?php echo json_encode($congr)    ;?>;
        var card    = <?php echo json_encode($card)     ;?>;
        var terri   = <?php echo json_encode($terri)    ;?>;
        var taicn   = <?php echo json_encode($taicn)    ;?>;
    </script>
    <!-- load tem_cards extension in debug able version                                          -->
    <script src="<?php print $js_card_script ?>"></script>
    <script> <?php
        // use php to write some javascript code
        // null is an empty string, but for the javascript function a "null" string is needed

        if( !isset($opentem_maps_data["mark"]) ) {
            $opentem_maps_data["mark"] = 'null';
        }

        $zoom_far  = $opentem_maps_data["zoom"] + $card["map"]["far"]["zoom"]["delta"];
        $zoom_near = $opentem_maps_data["zoom"] + $card["map"]["near"]["zoom"]["delta"];

        print("opentem_maps_init($opentem_maps_data[mark],$opentem_maps_data[center],$zoom_far,$zoom_near);");

    ?> </script>


