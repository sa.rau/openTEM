<!-- server side data handling of the map -->
<?php
//xdebug_break();
require_once( "./class/Database.php"        );
$db = Database::connect();

    // simulated db (just a file in json format, which stores a single data set)
    //$db = 'db/data.txt';

    // check if data was send
    if ( !empty($_POST) ) {
        // get send data from browser (client - server interface)
        $tem_maps_data = [
            "mark"      => $_POST['mapmark'],
            "center"    => $_POST['mapcenter'],
            "zoom"      => $_POST['mapzoom']
        ];
        $gpar['ID'] = $_POST['ID'];

        // get area data from db when just pos update (real db, just do not change area data)
        if( $tem_maps_data["mark"] == "posUpdate" ) {

            //xdebug_break();
            $db_mapmark = $db->getTerriMapmark($_POST['ID']);

            if ( $db_mapmark != false ) {
                $tmp = json_decode($db_mapmark,true);
                $tem_maps_data["mark"] = $tmp["mark"];
            } else {
                $tem_maps_data["mark"] = null;
            }

        }


        $gpar['mapmark'] = json_encode($tem_maps_data);

        if( $db->update("karte", $gpar) ) {
            //print "<div class=\"alert alert-success\">Strasse (ID $strasse[ID]): '$strasse[Strasse]' aktualisiert</div>";
        } else {
            print "<div class=\"alert alert-danger\">Gebiet: 'Name: $gebiet[Name], ID: $gebiet[ID]' konnte nicht aktualisiert werden. Versuche es bitte noch einmal.</div>";
        }

        //file_put_contents($db, json_encode($tem_maps_data));
    } else {
        // TODO Error
    }

?>
