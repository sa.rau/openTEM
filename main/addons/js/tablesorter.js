/***************************************************************************************************
    script to add extend a table for column sort and filter function
****************************************************************************************************
    Copyright (c) 2017 Stephan Rau
    Licensed under the MIT license.
***************************************************************************************************/
$(document).ready(function(){
    $(function(){
        // add new widget called indexFirstColumn
         $.tablesorter.addWidget({
             // give the widget a id
             id: "indexFirstColumn",
             // format is called when the on init and when a sorting has finished
             format: function(table) {
                 // loop all tr elements and set the value for the first column
                 for(var i=0; i < table.tBodies[0].rows.length; i++) {
                     $("tbody tr:eq(" + i + ") td:first",table).html(i+1);
                 }
             }
         });

        $("table").tablesorter( {
            theme : 'bootstrap',
            headerTemplate : '{content}{icon}',
            widgets : [ "uitheme", "filter", "zebra", "indexFirstColumn" ],
            widgetOptions : {
                zebra : ["even", "odd"],
                filter_reset : ".reset"
            }
        });
        $("#trigger-link").click(function() {
            //var sorting = [[0,0],[2,0]];
            //$("table").trigger("sorton",[sorting]);
            var filter = [ "< 100" ];
            $("table").trigger('search', [filter]);
            return false;
        });
        $("button.match").click(function() {
            //var sorting = [[0,0],[2,0]];
            //$("table").trigger("sorton",[sorting]);
            var filter = $(this).data('filter-text').split(":");
            $("table").trigger('search', [filter]);
            return false;
        });
    });
});
