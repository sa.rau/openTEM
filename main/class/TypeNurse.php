<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TypeNurse
 *
 * @author EDRauS
 */

require_once( "./class/Type.php"   );

class TypeNurse {
    const CREATE        = 0;
    const CREATE_FORM   = 1;
    const UPDATE        = 2;
    const SELECT_FORM   = 3;
    const UPDATE_FORM   = 4;
    const REMOVE        = 5;
    const ACTION        = 'type';

    private static $HEADERS = array(
        self::CREATE_FORM   => "Hinzufügen",
        self::SELECT_FORM   => "Bearbeiten"
    );

    private $db         = null;          // database connection object
    private $types      = array ();
    private $selected   = null;
    private static $inst = null;        // used for singelton implementation

    public function __construct() {

        $this->db = Database::connect();

        $this->db->getTerriTypes();
        while( $row = $this->db->fetch() ){
            $this->types[$row['ID']] = new Type($row);
        }

    }


    public function __destruct() {
        $this->db = null;
    }


    public function printLink($step) {
        print '<li><a href="index.php?action=' . self::ACTION . '&step=' . $step . '">'
            . self::$HEADERS[$step]
            . '</a></li>';
    }


    public function printCreateFrom () {
        print '<h1>Haltertyp Hinzufügen</h1>'
            . '<form  class="form-inline" method="post"'
            . 'action="index.php?action=' . self::ACTION . '&step=' . self::CREATE . '">'
            . '<div class="form-group"> <div class="input-group">'
            . '<span class="input-group-addon">Name</span>'
            . '<input style="width: 20em;" type="text" class="form-control" '
            . 'placeholder="z.B. Verkündiger" name=type[name]>'
            . '</div> </div>'
            . '<button type="submit" class="btn btn-success" style="width: 10em;">'
            . 'Hinzufügen</button>'
            . '</form>';
    }


    public function createNew() {
        try {
            //$type = filter_input(INPUT_REQUEST, 'type');
            $type = $_REQUEST['type'];
            $id = $this->db->insert("type", $type);
            $p_row['col_name'] = $type['name'];
            $p_row['ID'] = $id;
            $this->type[$id] = new Type($p_row);
            print '<div class="alert alert-success">Neuer Haltertyp: '
                . $this->type[$id]->getName()
                . ' hinzugefügt</div>';
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }


    private function select() {
        // TODO filter_input does not work yet
        if( isset($_REQUEST['type']) ) {
            $type = $_REQUEST['type'];
        } else {
            $type = null;
        }
        if( isset($type['ID']) ) {
            $this->selected = $type['ID'];
        } else {
            $this->selected = null;
        }
    }

    public function getDropdownOptionLines($selected_type) {
        foreach ( $this->types as $type ) {
            $type->printOptionLine($selected_type);
        }
    }
    public function printDropdown() {
        $this->select();
        Type::printDropdownHeader(self::ACTION, self::UPDATE_FORM);
        $this->getDropdownOptionLines($this->selected);
        Type::printDropdownTailer();
    }


    public function printUpdateFrom () {
        $this->types[$this->selected]->printUpdate( self::ACTION, self::UPDATE );
        //$this->types[$this->selected]->printRemove( self::ACTION, self::REMOVE );
    }


    public function update() {
        $this->select();
        $this->types[$this->selected]->update();
    }

    public function remove() {
        // TODO only remove group when not referenced in Owner table
    }


    public static function startDialogue() {
        if ( self::$inst == null ) {
            self::$inst = new TypeNurse();
        }
        return self::$inst;
    }


    public function getName($p_id) {
        if( isset($this->types[$p_id]) ) {
            $ret = $this->types[$p_id]->getName();
        } else {
            $ret = '<span class="text-danger">Fehler: Unbekannter Typ</span>';
        }
        return $ret;
    }

}
