<?php
/***************************************************************************************************

 Author:    Stephan Rau

 Purpose:   take care of all the group objects

***************************************************************************************************/

require_once( "./class/Group.php"   );

class GroupNurse {

    const CREATE        = 0;
    const CREATE_FORM   = 1;
    const UPDATE        = 2;
    const SELECT_FORM   = 3;
    const UPDATE_FORM   = 4;
    const REMOVE        = 5;
    const ACTION        = 'groups';

    private static $HEADERS = array(
        self::CREATE_FORM   => "Hinzufügen",
        self::SELECT_FORM   => "Bearbeiten"
    );

    private $db         = null;          // database connection object
    private $groups     = array ();
    private $selected   = null;
    private static $inst = null;        // used for singelton implementation

    public function __construct() {

        $this->db = Database::connect();
        $this->db->selectGroups();
        while( $row = $this->db->fetch() ){
            $this->groups[$row['ID']] = new Group($row);
        }

    }

    public function __destruct() {
        $this->db = null;
    }

    public function printLink($step) {
        print "<li><a href=\"index.php?action=groups&step=$step\">"
            . self::$HEADERS[$step]
            . "</a></li>";
    }

    public function printCreateFrom () {
        print '<h1>Gruppe Hinzufügen</h1>'
            . '<form  class="form-inline" method="post"'
            . 'action="index.php?action=' . self::ACTION . '&step=' . self::CREATE . '">'
            . '<div class="form-group"> <div class="input-group">'
            . '<span class="input-group-addon">Name</span>'
            . '<input style="width: 20em;" type="text" class="form-control" '
            . 'placeholder="Gruppen Name" name=group[name]>'
            . '</div> </div>'
            . '<button type="submit" class="btn btn-success" style="width: 10em;">'
            . 'Hinzufügen</button>'
            . '</form>';
    }

    public function createNew() {
        try {
            //$group = filter_input(INPUT_REQUEST, 'group');
            $group = $_REQUEST['group'];
            $id = $this->db->insert("group", $group);
            // TODO get complete row from just inserted new group
            $row['col_name'] = $group['name'];
            $row['ID'] = $id;
            $this->groups[$id] = new Group($row);
            print '<div class="alert alert-success">Neue Gruppe: '
                . $this->groups[$id]->getName()
                . ' hinzugefügt</div>';
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    private function select() {
        // TODO filter_input does not work yet
        if( isset($_REQUEST['group']) ) {
            $group = $_REQUEST['group'];
        } else {
            $group = null;
        }
        if( isset($group['ID']) ) {
            $this->selected = $group['ID'];
        } else {
            $this->selected = null;
        }
    }

    public function getDropdownOptionLines($selected_group) {
        foreach ( $this->groups as $group ) {
            $group->printOptionLine($selected_group);
        }
    }

    public function printDropdown() {
        $this->select();
        Group::printDropdownHeader(self::ACTION, self::UPDATE_FORM);
        $this->getDropdownOptionLines($this->selected);
        Group::printDropdownTailer();
    }

    public function printUpdateFrom () {
        $this->groups[$this->selected]->printUpdate( self::ACTION, self::UPDATE );
        //$this->groups[$this->selected]->printRemove( self::ACTION, self::REMOVE );
    }

    public function update() {
        $this->select();
        $this->groups[$this->selected]->update();
    }

    public function removeGroup() {

    }

    public static function startDialogue() {
        if ( self::$inst == null ) {
            self::$inst = new GroupNurse();
        }
        return self::$inst;
    }

    public function getName($p_id) {
        if( isset($this->groups[$p_id]) ) {
            $ret = $this->groups[$p_id]->getName();
        } else {
            $ret = '<span class="text-danger">Fehler: Unbekannte Gruppe</span>';
        }
        return $ret;
    }

}
