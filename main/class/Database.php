<?php
/***************************************************************************************************

 Autor:     Stephan Rau

 Propose:   create database connection and verify SQL strings before submitted
            since only one database connection should be created this class is desinged as singleton
            that means the constructor is private and a public functions checks if allready a object
            exists

 TODO:

 History:
    18.03.2014 - Stephan Rau:
        o inital version
    19.03.2014 - Stephan Rau:
        o constructor, destructor, simpleQuery function and simple test finished
    16.04.2014 - Stephan Rau:
        o POD object cannot be serialized to store in the $_SESSION variable
          tried with __sleep and __wakeup to distroy and create a new POD object, but __wakeup
          was never called. There using the Serializable interface
    22.04.2014 - Stephan Rau:
        o Serializabel interface also not worked
        o now pure static variabels, and on each php a new database connection must be created
    23.04.2014 - Stephan Rau:
        o implemented all sql strings for gebiete.php
    11.08.2014 - Stephan Rau:
        o implemented insert, update and delte function
        o added new prepared sql strings
    07.02.2015 - Stephan Rau:
        o implementend new sql queries to handel new gebiet entries
    18.02.2015 - Stephan Rau:
        o added new option to get time entries in descading order

 **************************************************************************************************/

//class Database implements Serializable {
class Database {

    private static $inst = null;        // used for singelton implementation

    private $dbHandle = null;           // POD object instance
    private $cur_qry  = null;           // current executed query

    // section with prepared SQL queries
    private static $preparedSqlQueries = array(
        "SELECT"    => array (
            //"halter"
            //    => "SELECT h.ID, h.col_surname, h.col_firstname, h.col_ownertypeid, z.col_territoryid, z.col_back, COUNT(*) as nog
            //        FROM  tab_owners h, tab_times z
            //        WHERE h.ID = z.col_ownerid AND z.col_back IS NULL
            //        GROUP BY h.ID
            //        ORDER BY h.col_surname ASC, h.col_firstname ASC",
            //"halter"
            //    => "SELECT h.*, z.col_territoryid, SUM(CASE WHEN z.col_back IS NULL THEN 1 ELSE 0 END) as nog
            //        FROM  tab_owners h, tab_times z
            //        WHERE h.ID = z.col_ownerid OR h.ID NOT IN (SELECT col_ownerid FROM tab_times)
            //        GROUP BY h.ID
            //        ORDER BY h.col_surname ASC, h.col_firstname ASC",
            "halter"
                => "SELECT h.*, z.col_territoryid, SUM(CASE WHEN z.col_back IS NULL THEN 1 ELSE 0 END) as nog
                    FROM  tab_owners h, tab_times z
                    WHERE h.ID = z.col_ownerid
                    GROUP BY h.ID
                    ORDER BY h.col_surname ASC, h.col_firstname ASC",

            "halter_by_type"
                => "SELECT h.ID, h.col_surname, h.col_firstname, h.col_ownertypeid, z.col_territoryid, z.col_back, COUNT(*) as nog
                    FROM  tab_owners h, tab_times z
                    WHERE h.ID = z.col_ownerid AND z.col_back IS NULL
                      AND h.col_ownertypeid = :EXT
                    GROUP BY h.ID
                    ORDER BY h.col_surname ASC, h.col_firstname ASC",

            "halter_alle"
                => "SELECT *
                    FROM tab_owners
                    ORDER BY col_surname ASC, col_firstname ASC",

            "groups"
                => "SELECT *
                    FROM tab_groups
                    ORDER BY col_name ASC",

            "types"
                => "SELECT *
                    FROM tab_ownerstypes
                    ORDER BY col_name ASC",

            "halter_ID"
                => "SELECT ID, col_surname, col_firstname, col_ownertypeid, col_groupid
                    FROM   tab_owners
                     WHERE col_surname = :surname
                       AND col_firstname  = :firstname
                       AND col_ownertypeid   = :ownertypeid
                       AND col_groupid = :groupid",

            "halter_by_ID"
                => "SELECT ID, col_surname, col_firstname, col_ownertypeid, col_groupid
                    FROM   tab_owners
                     WHERE ID = :ID",

            "zeiten_by_halter"
                => "SELECT g.ID as g_ID, g.col_name, z.col_ownerid, z.col_territoryid, z.ID as z_ID, z.col_back, z.col_assigned, z.col_lost
                     FROM tab_territories g, tab_times z
                     WHERE z.col_ownerid = :ID
                      AND g.ID = z.col_territoryid
                      AND z.col_back IS NULL
                    ORDER BY z.col_assigned DESC, z.col_back DESC, g.col_name + 0 ASC, g.col_name ASC",

            "zeiten_by_halter_back"
                => "SELECT g.ID as g_ID, g.col_name, z.col_ownerid, z.col_territoryid, z.ID as z_ID, z.col_back, z.col_assigned, z.col_lost
                     FROM tab_territories g, tab_times z
                     WHERE z.col_ownerid = :ID
                      AND g.ID = z.col_territoryid
                      AND z.col_back IS NOT NULL
                     ORDER BY z.col_assigned DESC, z.col_back DESC, g.col_name + 0 ASC, g.col_name ASC",

            "zeiten_by_gebiet"
                => "SELECT *,
                      TIMESTAMPDIFF( MONTH, col_assigned, CURDATE() ) as owned_for_month
                     FROM tab_times
                     WHERE col_territoryid = :ID
                     ORDER by col_assigned DESC",

            "zeiten_last_entry_by_gebiet"
                => "SELECT *
                     FROM tab_times
                     WHERE col_territoryid = :ID
                     ORDER by col_assigned DESC LIMIT 1;",

            "zeiten_last_two_entries_by_gebiet"
                => "SELECT *,
                      DATE_FORMAT( col_back, '%d.%m.%Y' ) as frei_seit_format,
                      TIMESTAMPDIFF( MONTH, col_back, CURDATE() ) as frei_seit_monaten,
                      col_progress
                     FROM tab_times
                     WHERE col_territoryid = :ID
                     ORDER by col_assigned DESC LIMIT 2;",

            "gebiete_alle"
                => "SELECT ID, col_name
                    FROM tab_territories",

            "gebiete_sorted"
                => " SELECT *
                     FROM   tab_territories
                     ORDER  BY col_name + 0 ASC, col_name ASC",

            "gebiete_ausgegeben"
                => " SELECT g.ID, z.col_territoryid, z.col_assigned, z.col_back
                     FROM tab_territories g, tab_times z
                     WHERE g.ID = z.col_territoryid
                       AND z.col_back IS NULL",

            "gebiete_frei"
                => " SELECT g.ID FROM tab_territories g
                     WHERE g.ID NOT IN ( SELECT z.col_territoryid FROM tab_times z
                                         WHERE z.col_back IS NULL )",

            "gebiete_frei_sorted_dict"
               => " SELECT g.ID, g.col_name, z.col_territoryid
                    FROM tab_territories g, tab_times z
                    WHERE g.ID NOT IN ( SELECT col_territoryid FROM tab_times
                                        WHERE col_back IS NULL )
                    GROUP BY g.ID
                    ORDER BY g.col_name + 0 ASC, g.col_name ASC",

            "gebiete_frei_sorted_empty_zeiten"
               => " SELECT g.ID, g.col_name, g.col_flats, g.col_blockpart, g.col_type, g.col_place, g.col_mapmark,
                     DATE_FORMAT( '0000-00-00', '%d.%m.%Y' ) as frei_seit_format,
                     0 as frei_seit_monaten
                    FROM  tab_territories g
                    WHERE g.ID NOT IN ( SELECT z.col_territoryid FROM tab_times z)
                    GROUP BY g.ID
                    ORDER BY g.col_name + 0 ASC, g.col_name ASC;",

            "gebiete_frei_sorted"
                => " SELECT * FROM (
                       SELECT g.ID, g.col_name, g.col_flats, g.col_blockpart, g.col_type, g.col_place, g.col_mapmark,
                              z.col_territoryid, z.col_back as frei_seit,
                        DATE_FORMAT( z.col_back, '%d.%m.%Y' ) as frei_seit_format,
                        TIMESTAMPDIFF( MONTH, z.col_back, CURDATE() ) as frei_seit_monaten
                       FROM tab_territories g, tab_times z
                       WHERE g.ID = z.col_territoryid
                         AND g.ID NOT IN ( SELECT col_territoryid FROM tab_times
                                           WHERE col_back IS NULL )
                       ORDER BY frei_seit DESC
                     ) x GROUP BY ID ORDER BY frei_seit ASC",

            "gebiete_all_sorted"
                => " SELECT
                      g.ID, g.col_name, g.col_flats, g.col_blockpart, g.col_type, g.col_place,
                      s.col_territoryid, s.col_street, s.col_note
                     FROM tab_territories g, tab_territoriesstreets s
                     WHERE g.ID = s.col_territoryid
                     ORDER BY g.col_name + 0 ASC, g.col_name ASC, s.col_street ASC",

            "gebiet_by_ID"
                => "SELECT  g.ID as g_ID, g.col_name
                          , z.col_ownerid, z.col_territoryid, z.ID as z_ID, z.col_back, z.col_lost, z.col_assigned
                          , h.col_surname, h.col_firstname, h.ID as h_ID
                     FROM   tab_territories g, tab_times z, tab_owners h
                     WHERE z.col_territoryid = :ID
                      AND g.ID = :ID
                      AND h.ID = z.col_ownerid
                     ORDER BY z.col_assigned ASC",

            "gebiet_by_ID_dsc"
                => "SELECT  g.ID as g_ID, g.col_name
                          , z.col_ownerid, z.col_territoryid, z.ID as z_ID, z.col_back, z.col_lost, z.col_assigned, z.col_progress
                          , h.col_surname, h.col_firstname, h.ID as h_ID
                     FROM   tab_territories g, tab_times z, tab_owners h
                     WHERE z.col_territoryid = :ID
                      AND g.ID = :ID
                      AND h.ID = z.col_ownerid
                     ORDER BY z.col_assigned DESC;",

            "strassen_by_gebiet_ID"
                => " SELECT
                      col_territoryid, col_street, col_note, ID
                     FROM tab_territoriesstreets
                     WHERE col_territoryid = :ID
                     ORDER BY col_street ASC",

            "halter_nog"
                => "SELECT
                    h.ID as h_ID, z.col_back, z.col_lost, COUNT(*) as nog
                    FROM tab_owners h, tab_times z
                    WHERE h.ID = z.col_ownerid
                      AND z.col_back IS NULL
                    GROUP BY h.ID",

            "gebiete_ausgegeben_sorted"
                => " SELECT
                      h.ID as h_ID, h.col_surname, h.col_firstname,
                      g.ID as g_ID, g.col_name, g.col_flats,
                      z.ID as z_ID, z.col_territoryid, z.col_ownerid,
                      DATE_FORMAT(z.col_assigned, '%d.%m.%Y') as ausg, z.col_back, z.col_lost,
                      TIMESTAMPDIFF( MONTH, z.col_assigned, CURDATE() ) AS ausgeben_seit
                     FROM tab_owners h, tab_territories g, tab_times z
                     WHERE g.ID = z.col_territoryid
                       AND h.ID = z.col_ownerid
                       AND z.col_back IS NULL
                     ORDER BY z.col_assigned ASC",

            "gebiete_ausgegeben_sorted_short"
                => " SELECT g.*
                     FROM tab_territories g, tab_times z
                     WHERE g.ID = z.col_territoryid
                       AND z.col_back IS NULL
                     ORDER BY z.col_assigned ASC",

            "gebiete_s13_sorted"
                => " SELECT
                      h.ID as h_ID, h.col_surname, h.col_firstname,
                      g.ID as g_ID, g.col_name, g.col_flats,
                      z.ID as z_ID, z.col_territoryid, z.col_ownerid,
                     DATE_FORMAT( z.col_assigned, '%d.%m.%Y' ) as ausg,
                     DATE_FORMAT( z.col_back, '%d.%m.%Y' ) as zurueck
                     FROM tab_owners h, tab_territories g, tab_times z
                     WHERE g.ID = z.col_territoryid
                       AND h.ID = z.col_ownerid
                     ORDER BY
                      g.col_name + 0 ASC,
                      g.col_name ASC,
                      z.col_assigned DESC,
                      z.col_back DESC",

            "gebiete_count"
                => "SELECT COUNT(*) as nog, SUM(col_flats) as now
                    FROM   tab_territories",

            "gebiete_ausgegeben_count"
                => " SELECT COUNT(*) as aug
                     FROM tab_territories g, tab_times z
                     WHERE g.ID = z.col_territoryid
                       AND z.col_back IS NULL",

            "gebiete_frei_empty_zeiten_count"
               => " SELECT COUNT(*) as noet
                    FROM  tab_territories g
                    WHERE g.ID NOT IN ( SELECT z.col_territoryid FROM tab_times z)
                    GROUP BY g.ID",

            "gebiete_all_count"
                => " SELECT g.ID,
                        TIMESTAMPDIFF( MONTH, z.col_back, CURDATE() ) as back_since_month,
                        TIMESTAMPDIFF( MONTH, z.col_assigned, CURDATE() ) as ausg_since_month
                       FROM tab_territories g, tab_times z
                      WHERE g.ID = z.col_territoryid
                     ORDER BY g.ID ASC, back_since_month ASC;" // LIMIT 2;"
                     // GROUP BY g.ID;"


        ),

        "INSERT"    => array (
            "gebiet"
                => "INSERT INTO tab_territories (col_name, col_flats, col_blockpart, col_type, col_place, col_ask, col_comment)
                    VALUES (:name, :flats, :blockpart, :type, :place, :ask, :comment)",
            "strasse"
                => "INSERT INTO tab_territoriesstreets (col_territoryid, col_street, col_note)
                    VALUES (:territoryid, :street, :note)",
            "halter"
                => "INSERT INTO tab_owners (col_surname, col_firstname, col_ownertypeid, col_groupid)
                    VALUES (:surname, :firstname, :ownertypeid, :groupid)",
            "group"
                => "INSERT INTO tab_groups (col_name)
                    VALUES (:name)",
            "type"
                => "INSERT INTO tab_ownerstypes (col_name)
                    VALUES (:name)",
            "zeiten"
                => "INSERT INTO tab_times (col_territoryid, col_ownerid, col_assigned)
                    VALUES (:ID, :h_ID, :assigned);",
            "zeiten_all"
                => "INSERT INTO tab_times (col_territoryid, col_ownerid, col_assigned, col_back, col_lost, col_progress)
                    VALUES (:ID, :h_ID, :assigned, :back, :lost, :progress);"
        ),

        "UPDATE"    => array (
            "group"
                => "UPDATE tab_groups
                    SET    col_name = :name
                    WHERE  ID       = :ID",
            "type"
                => "UPDATE tab_ownerstypes
                    SET    col_name = :name
                    WHERE  ID       = :ID",
            "gebiet"
                => "UPDATE tab_territories
                    SET    col_name      = :name,
                           col_flats     = :flats,
                           col_blockpart = :blockpart,
                           col_type      = :type,
                           col_place     = :place,
                           col_ask       = :ask,
                           col_comment   = :comment
                    WHERE  ID            = :ID",
            "karte"
                => "UPDATE tab_territories
                    SET    col_mapmark    = :mapmark
                    WHERE  ID       = :ID",
            "strasse"
                => "UPDATE tab_territoriesstreets
                    SET    col_street = :street,
                           col_note   = :note
                    WHERE  ID         = :ID",
            "halter"
                => "UPDATE tab_owners
                    SET    col_surname = :surname,
                           col_firstname = :firstname,
                           col_ownertypeid   = :ownertypeid,
                           col_groupid = :groupid
                    WHERE  ID       = :ID",
            "zurueck"
                => "UPDATE tab_times
                    SET    col_assigned = :assigned,
                           col_back 	= :back,
						   col_lost			= :lost,
                           col_progress     = :progress
                    WHERE  ID = :z_ID",
            "ausgegeben"
                => "UPDATE tab_times
                    SET    col_assigned = :assigned
                    WHERE  ID = :z_ID"
        ),

        "DELETE"    => array (
            "gebiet"
                => "DELETE FROM tab_territories
                    WHERE  ID       = :ID",
            "strasse"
                => "DELETE FROM tab_territoriesstreets
                    WHERE  ID       = :ID",
            "zeiten"
                => "DELETE FROM tab_times
                    WHERE ID = :ID"
        )

    );



    //---- connectPDO ------------------------------------------------------------------------------
    // Propose:     connect to mySQL database with PDO
    // Parameter:   none
    // Return:      none
    private function connectPDO() {
        //print "now in connectPDO function<br>";
        try {
            // connect to db
            /*
            $db = new PDO(
                "mysql:host=".self::$dbConfRef['host'].";dbname=".self::$dbConfRef['name']
                ,self::$dbConfRef['user'],self::$dbConfRef['pw']
            );
            */
            $db = new PDO("sqlite:./database/territory.sqlite3");

            // error behaviour
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //$db->query("set character set utf8");
            //$db->query("set names utf8");

            // save in class variable
            $this->dbHandle = $db;

        } catch (PDOException $ex) {
            throw new PDOException("Connection Exception: " . $ex->getMessage());
        }
    }



    //---- __construct -----------------------------------------------------------------------------
    // Propose:     create connection to database
    // Parameter:   con_scheme = databse connection scheme (database key name) as refernce
    // Return:      new object is returned (indirect private dbHandle variable is set)
    private function __construct() {
        //print "now in __construct function<br>";
        $this->connectPDO();
        $this->dbHandle->exec("CREATE TABLE IF NOT EXISTS  tab_territoriesrelation (
            ID INTEGER PRIMARY KEY,
            col_territoryidmaster INTEGER,
            col_territoryidslave INTEGER
        )");
    }

    //---- connect ---------------------------------------------------------------------------------
    // Propose:     this is the singelton functions which calls the constuctor
    // Parameter:   none
    // Return:      return object reference
    public static function connect() {
        //print "<br><br><br>now in connect function<br>" . self::$dbConfRef;
        // only create new database connection, if no connection is there TODO check if this works
        if ( self::$inst == null ) {
            self::$inst = new Database();
        }
        return self::$inst;
    }


    //---- __destruct ------------------------------------------------------------------------------
    // Propose:     close connection to database (even for a singelton this must be pulbic)
    // Parameter:   none
    // Return:      none
    public function __destruct() {
        //print "now in __destruct function<br>";
        // remove reference to POD object => call POD destructor
        unset( $this->dbHandle );
    }


    //---- disconnect ------------------------------------------------------------------------------
    // Propose:     close database connection
    // Parameter:   none
    // Return:      none
    public function disconnect() {
        //print "now in disconnect function<br>";
        self::$inst = null;     // call Database singelton destructor
    }


    //---- simpleQuery -----------------------------------------------------------------------------
    // Propose:     exectue given SQL query string
    // Parameter:   qry     SQL query string
    // Return:      associated array with all query results (use for small query results)
    public function simpleQuery($qry) {
        //print "now in simpleQuery function<br>";
        try {

            $q = $this->dbHandle->prepare($qry);
            //print "prepare done<br>";
            if( $q->execute() ) {
            //print "return fetch all<br>";
                return $q->fetchAll(PDO::FETCH_ASSOC);
            } else {
            //print "execute failed<br>";
                return false;
            }

        } catch (PDOException $ex) {
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }


    //---- selectOneRow ----------------------------------------------------------------------------
    // Propose:     exectue predifined SQL query
    // Parameter:   qTab    SQL table
    //              qCol    table column
    //              qLike   like parameter
    // Return:      associated array with query result of the selected row
    public function selectOneRow($qTab, $qCol, $qLike) {
        //print "now in slelectOneRow function<br>";

        // create query
        $qry = "SELECT * FROM $qTab WHERE $qCol LIKE '$qLike'";
        //print "<h3>$qry</h3>";

        // use simpleQuery
        $ret = $this->simpleQuery($qry);

        // check if exact one element is returned
        if( count($ret) == 0 ) {
            return false;
        } elseif( count($ret) == 1 ) {
            return $ret[0];
        } else {
            throw new Exception("Not exact one Row is returned");
        }

    }


    //---- query -----------------------------------------------------------------------------------
    // Propose:     execute given SQL query string and save result in private variable
    // Parameter:   qry     SQL query string
    // Return:      none
    public function query($qry) {
        try {

            if( is_null($this->cur_qry) ) {
                //print "<h4>in query</h4>";
                $this->cur_qry = $this->dbHandle->prepare($qry);
                $this->cur_qry->execute();
            } else {
                throw new Exception("There is allready a query running.");
            }

        } catch (PDOException $ex) {
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }


    //---- select ----------------------------------------------------------------------------------
    // Propose:     execute preapared query string and save result in private variable
    // Parameter:   qry     preapared SQL query string naem
    // Return:      none
    public function select($qryNam) {
        if( array_key_exists($qryNam, self::$preparedSqlQueries["SELECT"]) ) {
            $this->query(self::$preparedSqlQueries["SELECT"][$qryNam]);
        } else {
            throw new Exception("Given qryNam '$qryNam' does not exits.");
        }
    }


    //---- selectLimit -----------------------------------------------------------------------------
    // Propose:     execute preapared query string with a limit number and save result in priv var
    // Parameter:   qry     preapared SQL query string naem
    // Return:      none
    public function selectLimit($qryNam, $limit) {
        if( array_key_exists($qryNam, self::$preparedSqlQueries["SELECT"]) ) {
            $this->query(self::$preparedSqlQueries["SELECT"][$qryNam] . " LIMIT $limit");
        } else {
            throw new Exception("Given qryNam '$qryNam' does not exits.");
        }
    }


    //---- selectCon -------------------------------------------------------------------------------
    // Propose:     select sql string with condition
    // Parameter:   qry     SQL query string
    //              par     SQL parameter array
    // Return:      none
    public function selectCon($qryNam, $par) {

        try {
            if( is_null($this->cur_qry) ) {
                if( array_key_exists($qryNam, self::$preparedSqlQueries["SELECT"]) ) {
                    $this->cur_qry = $this->dbHandle->prepare(self::$preparedSqlQueries["SELECT"][$qryNam]);
                    $this->cur_qry->execute($par);
                } else {
                    throw new Exception("Given qryNam '$qryNam' does not exits.");
                }
            } else {
                throw new Exception("There is allready a query running.");
            }

            return true;

        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }


    //---- fetch -----------------------------------------------------------------------------------
    // Propose:     fetch next row from cur_qry
    // Parameter:   none
    // Return:      associated array with column names as keys for current row
    public function fetch() {
        try {

            if( $ret = $this->cur_qry->fetch(PDO::FETCH_ASSOC) ) {
				return $ret;
            } else {
                $this->cur_qry = null;
                return false;
            }

        } catch (PDOException $ex) {
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }


    //---- rowCount --------------------------------------------------------------------------------
    // Propose:     get number of rows from cur_qry
    // Parameter:   none
    // Return:      associated array with column names as keys for current row
    public function rowCount() {
        try {
            // workaround sqlite3 POD rowCount "$this->cur_qry->rowCount()" is not working
            $rows = $this->cur_qry->fetchAll();
            $this->cur_qry = null;
            $ret = count($rows);
            return $ret;
        } catch (PDOException $ex) {
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }


    //---- insert ----------------------------------------------------------------------------------
    // Propose:
    // Parameter:   qry     SQL query string
    //              par     SQL parameter array
    // Return:      none
    public function insert($qryNam, $par) {

        try {
            if( is_null($this->cur_qry) ) {
                if( array_key_exists($qryNam, self::$preparedSqlQueries["INSERT"]) ) {
                    $this->cur_qry = $this->dbHandle->prepare(self::$preparedSqlQueries["INSERT"][$qryNam]);
                    $this->cur_qry->execute($par);
                    $ret = $this->dbHandle->lastInsertID();
                    $this->cur_qry = null;
                } else {
                    throw new Exception("Given qryNam '$qryNam' does not exits.");
                }
            } else {
                throw new Exception("There is allready a query running.");
            }

            return $ret;

        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }


    //---- update ----------------------------------------------------------------------------------
    // Propose:
    // Parameter:   qry     SQL query string
    //              par     SQL parameter array
    // Return:      none
    public function update($qryNam, $par) {

        try {
            if( is_null($this->cur_qry) ) {
                if( array_key_exists($qryNam, self::$preparedSqlQueries["UPDATE"]) ) {
                    $this->cur_qry = $this->dbHandle->prepare(self::$preparedSqlQueries["UPDATE"][$qryNam]);
                    $this->cur_qry->execute($par);
                    $this->cur_qry = null;
                } else {
                    throw new Exception("Given qryNam '$qryNam' does not exits.");
                }
            } else {
                throw new Exception("There is allready a query running.");
            }

            return true;

        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }


    //---- delete ----------------------------------------------------------------------------------
    // Propose:
    // Parameter:   qry     SQL query string
    //              par     SQL parameter array
    // Return:      none
    public function delete($qryNam, $par) {

        try {
            if( is_null($this->cur_qry) ) {
                if( array_key_exists($qryNam, self::$preparedSqlQueries["DELETE"]) ) {
                    $this->cur_qry = $this->dbHandle->prepare(self::$preparedSqlQueries["DELETE"][$qryNam]);
                    $this->cur_qry->execute($par);
                    $this->cur_qry = null;
                } else {
                    throw new Exception("Given qryNam '$qryNam' does not exits.");
                }
            } else {
                throw new Exception("There is allready a query running.");
            }

            return true;

        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }



    //##############################################################################################
    // new part for better failure handling of empty query reports
    //##############################################################################################


    //---- getNumOf --------------------------------------------------------------------------------
    // Propose:     return number of query results
    // Parameter:   qry     preapared SQL query string name
    // Return:      number or null on failure
    private function getNumOf($qry) {
        $ret = null;
        try {
            if( is_null($this->cur_qry) ) {
                //print "<h4>in query</h4>";
                $this->cur_qry = $this->dbHandle->prepare($qry);
                if( $this->cur_qry->execute() ) {
                    //$ret = $this->cur_qry->rowCount();
                    //$this->cur_qry = null;
                    $ret = $this->rowCount();
                } else {
                    $ret = 0;
                    $this->cur_qry = null;
                    throw new Exception("Execution Error");
                }
            } else {
                throw new Exception("There is allready a query running.");
            }
        } catch (PDOException $ex) {
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
        return $ret;
    }

    // wrappers for getNumOf
    public function getNumOfAllTerries() {
        $qry = "SELECT ID, col_name FROM tab_territories WHERE (col_type like 0 OR col_type like 2)";
        return $this->getNumOf($qry);
    }

    public function getNumOfUncountedTerries() {
        $qry = "SELECT ID, col_name FROM tab_territories WHERE col_flats LIKE 0";
        return $this->getNumOf($qry);
    }

    public function getNumOfAllFlats() {
        $qry = "SELECT COUNT(*) as nog, SUM(col_flats) as now FROM tab_territories";
        $ret = null;
        try {
            if( !(is_null($this->cur_qry)) ) {
                throw new Exception("There is allready a query running.");
            } else {
                $this->cur_qry = $this->dbHandle->prepare($qry);
                $this->cur_qry->execute(); // TODO execution errror ?
                while ( $row = $this->cur_qry->fetch() ) {
                    $ret = $row['now']; // number of col_flats (flats)
                }
                $this->cur_qry = null;
            }
            return $ret;
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }

    public function getNumOfFreeTerries() {
        /*
            "gebiete_frei"
                => " SELECT g.ID FROM tab_territories g
                     WHERE g.ID NOT IN ( SELECT z.col_territoryid FROM tab_times z
                                         WHERE z.col_back IS NULL )",

            "gebiete_frei_sorted_dict"
               => " SELECT g.ID, g.col_name, z.col_territoryid
                    FROM tab_territories g, tab_times z
                    WHERE g.ID NOT IN ( SELECT col_territoryid FROM tab_times
                                        WHERE col_back IS NULL )
                    GROUP BY g.ID
                    ORDER BY g.col_name + 0 ASC, g.col_name ASC",

            "gebiete_frei_sorted_empty_zeiten"
               => " SELECT g.ID, g.col_name, g.col_flats, g.col_blockpart, g.col_type, g.col_place, g.col_mapmark,
                     DATE_FORMAT( '0000-00-00', '%d.%m.%Y' ) as frei_seit_format,
                     0 as frei_seit_monaten
                    FROM  tab_territories g
                    WHERE g.ID NOT IN ( SELECT z.col_territoryid FROM tab_times z)
                    GROUP BY g.ID
                    ORDER BY g.col_name + 0 ASC, g.col_name ASC;",

            "gebiete_frei_sorted"
                => " SELECT * FROM (
                       SELECT g.ID, g.col_name, g.col_flats, g.col_blockpart, g.col_type, g.col_place, g.col_mapmark,
                              z.col_territoryid, z.col_back as frei_seit,
                        DATE_FORMAT( z.col_back, '%d.%m.%Y' ) as frei_seit_format,
                        TIMESTAMPDIFF( MONTH, z.col_back, CURDATE() ) as frei_seit_monaten
                       FROM tab_territories g, tab_times z
                       WHERE g.ID = z.col_territoryid
                         AND g.ID NOT IN ( SELECT col_territoryid FROM tab_times
                                           WHERE col_back IS NULL )
                       ORDER BY frei_seit DESC
                     ) x GROUP BY ID ORDER BY frei_seit ASC"
        */
        $qry = " SELECT g.ID FROM tab_territories g
                  WHERE g.ID NOT IN ( SELECT z.col_territoryid FROM tab_times z
                                       WHERE z.col_back IS NULL )";
        return $this->getNumOf($qry);
    }

    public function selFreeTerries() {
        $qry  = " SELECT g.ID, g.col_name
                    FROM tab_territories g
                   WHERE g.ID NOT IN ( SELECT col_territoryid FROM tab_times WHERE col_back IS NULL )
                GROUP BY g.ID
                ORDER BY g.col_name + 0 ASC, g.col_name ASC";
        try {
            if( !(is_null($this->cur_qry)) ) {
                throw new Exception("There is allready a query running.");
            } else {
                $this->cur_qry = $this->dbHandle->prepare($qry);
                $this->cur_qry->execute();
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }


    public function getNumOfAssignedTerries() {
        /*
            "gebiete_ausgegeben"
                => " SELECT g.ID, z.col_territoryid, z.col_assigned, z.col_back
                     FROM tab_territories g, tab_times z
                     WHERE g.ID = z.col_territoryid
                       AND z.col_back IS NULL",

            "gebiete_ausgegeben_sorted"
                => " SELECT
                      h.ID as h_ID, h.col_surname, h.col_firstname,
                      g.ID as g_ID, g.col_name, g.col_flats,
                      z.ID as z_ID, z.col_territoryid, z.col_ownerid,
                      DATE_FORMAT(z.col_assigned, '%d.%m.%Y') as ausg, z.col_back, z.col_lost,
                      TIMESTAMPDIFF( MONTH, z.col_assigned, CURDATE() ) AS ausgeben_seit
                     FROM tab_owners h, tab_territories g, tab_times z
                     WHERE g.ID = z.col_territoryid
                       AND h.ID = z.col_ownerid
                       AND z.col_back IS NULL
                     ORDER BY z.col_assigned ASC",

            "gebiete_ausgegeben_sorted_short"
                => " SELECT g.*
                     FROM tab_territories g, tab_times z
                     WHERE g.ID = z.col_territoryid
                       AND z.col_back IS NULL
                     ORDER BY z.col_assigned ASC",

            "gebiete_ausgegeben_count"
                => " SELECT COUNT(*) as aug
                     FROM tab_territories g, tab_times z
                     WHERE g.ID = z.col_territoryid
                       AND z.col_back IS NULL",

        */
        $qry = " SELECT g.ID, z.col_territoryid, z.col_assigned, z.col_back
                   FROM tab_territories g, tab_times z
                  WHERE g.ID = z.col_territoryid
                    AND z.col_back IS NULL";
        return $this->getNumOf($qry);
    }

    public function getTerritoryStatus($p_terriId) {
        $par['ID'] = $p_terriId;
        $ret = null;
        $qry = "SELECT col_back, col_progress, col_assigned "
             . " FROM  tab_times "
             . "WHERE  col_territoryid = :ID "
             . "ORDER  by col_assigned DESC LIMIT 2; ";
        try {
            if( is_null($this->cur_qry) ) {
                //print "<h4>in query</h4>";

                $date_today = new DateTime(date('Y-m-d'));

                // initial free since values, because they could be undefined
                $date_back = new DateTime("1900-01-01");
                $date_diff = $date_today->diff($date_back);
                $ret['freeSinceDate']   = $date_back->format('d.m.Y');
                $ret['freeSinceMonth']  = $date_diff->format('%y') * 12
                                        + $date_diff->format('%m') ;
                $ret['ownedSinceDate']  = null;
                $ret['ownedSinceMonth'] = null;
                $ret['lastDoneDate']    = $date_back->format('d.m.Y');
                $ret['lastDoneMonth']   = $date_diff->format('%y') * 12
                                        + $date_diff->format('%m') ;
                $ret['assigned']        = 0;
                $ret['currentProgress'] = null;

                // get values from db
                $this->cur_qry = $this->dbHandle->prepare($qry);
                if( $this->cur_qry->execute($par) ) {

                    while ( $row = $this->cur_qry->fetch() ) {

                        $date_assigned = new DateTime($row['col_assigned']);
                        $date_diff = $date_today->diff($date_assigned);
                        $ret['ownedSinceDate']   = $date_assigned->format('d.m.Y');
                        $ret['ownedSinceMonth']  = $date_diff->format('%y') * 12
                                                 + $date_diff->format('%m') ;
                        $ret['currentProgress'] = $row['col_progress'];

                        if( $row['col_back'] != null ) {
                            $date_back = new DateTime($row['col_back']);
                            $date_diff = $date_today->diff($date_back);
                            $ret['freeSinceDate']   = $date_back->format('d.m.Y');
                            $ret['freeSinceMonth']  = $date_diff->format('%y') * 12
                                                    + $date_diff->format('%m') ;
                            $ret['lastDoneDate']    = $date_back->format('d.m.Y');
                            $ret['lastDoneMonth']   = $date_diff->format('%y') * 12
                                                    + $date_diff->format('%m') ;
                            break; // stop looping since there is not older valid entry for back
                        } else {
                            $ret['assigned'] = 1;
                            $ret['freeSinceDate']   = null;
                            $ret['freeSinceMonth']  = null;
                        }


                    }
                    $this->cur_qry = null;
                } else {
                    $ret = 0;
                    $this->cur_qry = null;
                    throw new Exception("Execution Error");
                }
            } else {
                throw new Exception("There is allready a query running.");
            }
        } catch (PDOException $ex) {
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
        return $ret;
    }


    public function selTimeTableByTerrieID($p_terriId, $p_limit) {
        $qry = " SELECT h.ID as h_ID, h.col_surname, h.col_firstname,
                        z.ID as z_ID, z.col_territoryid, z.col_ownerid, z.col_assigned, z.col_back
                   FROM tab_owners h, tab_times z
                  WHERE z.col_territoryid = :g_ID  AND h.ID = z.col_ownerid
               ORDER BY z.col_assigned ASC, z.col_back ASC
                  LIMIT :z_limit; ";
        $par["g_ID"] = $p_terriId;
        $par["z_limit"] = $p_limit;

        try {
            if( !(is_null($this->cur_qry)) ) {
                throw new Exception("There is allready a query running.");
            } else {
                $this->cur_qry = $this->dbHandle->prepare($qry);
                $this->cur_qry->execute($par);
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }

    public function selAssignedTimeTableByOwnerID($p_ownerId) {
        $qry = " SELECT g.ID as g_ID, g.col_name, g.col_flats,
                        z.col_ownerid, z.col_territoryid, z.ID as z_ID, z.col_back, z.col_assigned, z.col_lost
                   FROM tab_territories g, tab_times z
                  WHERE z.col_ownerid = :ID
                        AND g.ID = z.col_territoryid
                        AND z.col_back IS NULL
                  ORDER BY z.col_assigned DESC, z.col_back DESC, g.col_name + 0 ASC, g.col_name ASC";

        $par["ID"] = $p_ownerId;

        try {
            if( !(is_null($this->cur_qry)) ) {
                throw new Exception("There is allready a query running.");
            } else {
                $this->cur_qry = $this->dbHandle->prepare($qry);
                $this->cur_qry->execute($par);
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }

    public function updateTimeEntryByID($p_tid,$p_assigned, $p_back, $p_lost, $p_progress) {
        //xdebug_break();
        $qry = "UPDATE tab_times
                   SET col_assigned = :assigned,
                       col_back = :back,
                       col_lost	= :lost,
                       col_progress = :progress
                WHERE  ID = :z_ID";
        $par['z_ID'] = $p_tid;
        $par['assigned'] = $p_assigned;
        $par['back'] = $p_back;
        $par['lost'] = $p_lost;
        $par['progress'] = $p_progress;

        try {
            if( !(is_null($this->cur_qry)) ) {
                throw new Exception("There is allready a query running.");
            } else {
                $this->cur_qry = $this->dbHandle->prepare($qry);
                $this->cur_qry->execute($par); // TODO check ? or is try block doning this?
                $this->cur_qry = null;
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }



    public function getCurrentOwnerInfo($p_terriId) {
        //xdebug_break();
        $par['ID'] = $p_terriId;
        $ret = null;
        $qry = "SELECT *"
             . " FROM  tab_times "
             . "WHERE  col_territoryid = :ID "
             . "ORDER  by col_assigned DESC; ";
        try {
            if( is_null($this->cur_qry) ) {
                //print "<h4>in query</h4>";
                $date_today = new DateTime(date('Y-m-d'));

                // run through assigned values as long as the owner is the sam
                $this->cur_qry = $this->dbHandle->prepare($qry);
                if( $this->cur_qry->execute($par) ) {
                    while ( $row = $this->cur_qry->fetch() ) {
                        if( !isset($ret['owner_id']) || ($ret['owner_id'] == $row['col_ownerid']) ) {
                            $date_assigned = new DateTime($row['col_assigned']);
                            $date_diff = $date_today->diff($date_assigned);
                            $ret['ownedForMonth']  = $date_diff->format('%y') * 12
                                                   + $date_diff->format('%m') ;
                            $ret['owner_id'] = $row['col_ownerid'];
                        } else {
                            break;
                        }
                    }
                    $this->cur_qry = null;
                } else {
                    $ret = 0;
                    $this->cur_qry = null;
                    throw new Exception("Execution Error");
                }
            } else {
                throw new Exception("There is allready a query running.");
            }
        } catch (PDOException $ex) {
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
        return $ret;
    }


    public function getAllTerries() {
        $qry = " SELECT * FROM   tab_territories ORDER  BY col_name + 0 ASC, col_name ASC";
        try {
            if( !(is_null($this->cur_qry)) ) {
                throw new Exception("There is allready a query running.");
            } else {
                $this->cur_qry = $this->dbHandle->prepare($qry);
                $this->cur_qry->execute();
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }

    public function getAllTerrieIDs() {
        $ret = [];
        $qry = " SELECT ID
		 FROM tab_territories
		 WHERE (col_type like 0 OR col_type like 2)
		 ORDER  BY col_name + 0 ASC, col_name ASC
	";
        try {
            if( is_null($this->cur_qry) ) {
                //print "<h4>in query</h4>";
                $this->cur_qry = $this->dbHandle->prepare($qry);
                if( $this->cur_qry->execute() ) {
                    while ( $row = $this->cur_qry->fetch() ) {
                        $ret[] = $row["ID"];
                    }
                    $this->cur_qry = null;
                } else {
                    $this->cur_qry = null;
                    throw new Exception("Execution Error");
                }
            } else {
                throw new Exception("There is allready a query running.");
            }
            return $ret;
        } catch (PDOException $ex) {
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
        return $ret;
    }




    public function getAssignedTerries() {
        $qry = " SELECT g.* "
             . "   FROM tab_territories g, tab_times z "
             . "  WHERE g.ID = z.col_territoryid AND z.col_back IS NULL "
             . "  ORDER BY z.col_assigned ASC";
        try {
            if( !(is_null($this->cur_qry)) ) {
                throw new Exception("There is allready a query running.");
            } else {
                $this->cur_qry = $this->dbHandle->prepare($qry);
                $this->cur_qry->execute();
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }

    public function getFreeTerries() {
        $qry = " SELECT g.*"
            . " FROM  tab_territories g, tab_times z"
            . " WHERE ( g.ID NOT IN ( SELECT z.col_territoryid FROM tab_times z) )"
            . "   OR  ( g.ID = z.col_territoryid AND g.ID NOT IN ( SELECT col_territoryid FROM tab_times WHERE col_back IS NULL ) )"
            . " GROUP BY g.ID"
            . " ORDER BY g.col_name + 0 ASC, g.col_name ASC;";
            /*
            "gebiete_frei_sorted"
            => " SELECT * FROM (
                   SELECT g.ID, g.col_name, g.col_flats, g.col_blockpart, g.col_type, g.col_place, g.col_mapmark,
                          z.col_territoryid, z.col_back as frei_seit,
                    DATE_FORMAT( z.col_back, '%d.%m.%Y' ) as frei_seit_format,
                    TIMESTAMPDIFF( MONTH, z.col_back, CURDATE() ) as frei_seit_monaten
                   FROM tab_territories g, tab_times z
                   WHERE g.ID = z.col_territoryid
                     AND g.ID NOT IN ( SELECT col_territoryid FROM tab_times
                                       WHERE col_back IS NULL )
                   ORDER BY frei_seit DESC
                 ) x GROUP BY ID ORDER BY frei_seit ASC",
                */
        try {
            if( !(is_null($this->cur_qry)) ) {
                throw new Exception("There is allready a query running.");
            } else {
                $this->cur_qry = $this->dbHandle->prepare($qry);
                $this->cur_qry->execute();
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }

    public function selectGroups() {
        $qry = "SELECT * FROM tab_groups ORDER BY col_name ASC";
        $this->query($qry);
    }

    public function getAllGroupIds() {
        $ret = [];
        $qry = "SELECT * FROM tab_groups ORDER BY col_name ASC";
        try {
            if( is_null($this->cur_qry) ) {
                //print "<h4>in query</h4>";
                $this->cur_qry = $this->dbHandle->prepare($qry);
                if( $this->cur_qry->execute() ) {
                    while ( $row = $this->cur_qry->fetch() ) {
                        $ret[] = $row["ID"];
                    }
                    $this->cur_qry = null;
                } else {
                    $this->cur_qry = null;
                    throw new Exception("Execution Error");
                }
            } else {
                throw new Exception("There is allready a query running.");
            }
            return $ret;
        } catch (PDOException $ex) {
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
        return $ret;
    }

    public function getGroupName($par) {
        $qry = "SELECT * FROM tab_groups WHERE ID LIKE $par";
        try {
            $ret = $this->simpleQuery($qry);
            // check if exact one element is returned
            if( count($ret) == 0 ) {
                return false;
            } elseif( count($ret) == 1 ) {
                return $ret[0]["col_name"];
            } else {
                throw new Exception("Not exact one Row is returned");
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }

    public function getStreetsByTerri($p_tid) {
        $qry = " SELECT col_territoryid, col_street, col_note, ID "
             . "   FROM tab_territoriesstreets "
             . "  WHERE col_territoryid = :ID "
             . "  ORDER BY col_street ASC";
        $par["ID"] = $p_tid;
        try {

            if( !(is_null($this->cur_qry)) ) {
                throw new Exception("There is allready a query running.");
            } else {
                $this->cur_qry = $this->dbHandle->prepare($qry);
                $this->cur_qry->execute($par);
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }

    public function getLastTerriOwnerEntry($p_tid) {
        $qry = "SELECT * "
             . "  FROM tab_times "
             . " WHERE col_territoryid = :ID "
             . " ORDER by col_assigned DESC LIMIT 1;";
        $par["ID"] = $p_tid;
        try {

            if( !(is_null($this->cur_qry)) ) {
                throw new Exception("There is allready a query running.");
            } else {
                $this->cur_qry = $this->dbHandle->prepare($qry);
                $this->cur_qry->execute($par);
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }


    //----------------------------------------------------------------------------------------------
    // additional territory information lines
    //----------------------------------------------------------------------------------------------
    public function getTerriAddInfoAll() {
        //xdebug_break();
        $qry = "SELECT * FROM tab_territoriesaddinfo ORDER BY col_infojson ASC";
        try {
            if( !(is_null($this->cur_qry)) ) {
                throw new Exception("There is allready a query running.");
            } else {
                $this->cur_qry = $this->dbHandle->prepare($qry);
                $this->cur_qry->execute();
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }

    public function getTerriAddInfo($p_tid) {
        //xdebug_break();
        $qry = "SELECT ID, col_infojson FROM tab_territoriesaddinfo WHERE col_territoryid = :ID ORDER BY col_infojson ASC";
        $par["ID"] = $p_tid;
        try {
            if( !(is_null($this->cur_qry)) ) {
                throw new Exception("There is allready a query running.");
            } else {
                $this->cur_qry = $this->dbHandle->prepare($qry);
                $this->cur_qry->execute($par);
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }

    public function insertTerriAddInfo($par) {
        //xdebug_break();
        $qry = "INSERT INTO tab_territoriesaddinfo (col_territoryid, col_infojson) VALUES (:territoryid, :infojson)";
        try {
            if( !(is_null($this->cur_qry)) ) {
                throw new Exception("There is allready a query running.");
            } else {
                $_par['territoryid'] = $par['territoryid'];
                $_par['infojson'] = json_encode($par['value']);
                $this->cur_qry = $this->dbHandle->prepare($qry);
                $ret = $this->cur_qry->execute($_par);
                //$ret = $this->dbHandle->lastInsertID(); // to open last territory page again
                $this->cur_qry = null;
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }


    public function updateTerriAddInfo($par) {
        //xdebug_break();
        $qry = "UPDATE tab_territoriesaddinfo SET col_infojson = :infojson WHERE  ID  = :ID";
        try {
            if( !(is_null($this->cur_qry)) ) {
                throw new Exception("There is allready a query running.");
            } else {
                $_par['ID'] = $par['ID'];
                $_par['infojson'] = json_encode($par['value']);
                $this->cur_qry = $this->dbHandle->prepare($qry);
                $this->cur_qry->execute($_par);
                $this->cur_qry = null;
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }

    public function deleteTerriAddInfo($par) {
        //xdebug_break();
        $qry = "DELETE FROM tab_territoriesaddinfo WHERE ID = :ID";
        try {
            if( !(is_null($this->cur_qry)) ) {
                throw new Exception("There is allready a query running.");
            } else {
                $this->cur_qry = $this->dbHandle->prepare($qry);
                $this->cur_qry->execute($par);
                $this->cur_qry = null;
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }

    public function getTerri($par) {
        //xdebug_break();
        $qry = "SELECT * FROM tab_territories WHERE ID LIKE $par";
        try {
            $ret = $this->simpleQuery($qry);
            // check if exact one element is returned
            if( count($ret) == 0 ) {
                return false;
            } elseif( count($ret) == 1 ) {
                return $ret[0];
            } else {
                throw new Exception("Not exact one Row is returned");
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }

    public function getTerriName($par) {
        $qry = "SELECT * FROM tab_territories WHERE ID LIKE $par";
        try {
            $ret = $this->simpleQuery($qry);
            // check if exact one element is returned
            if( count($ret) == 0 ) {
                return false;
            } elseif( count($ret) == 1 ) {
                return $ret[0]["col_name"];
            } else {
                throw new Exception("Not exact one Row is returned");
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }

    public function getTerriMapmark($par) {
        $qry = "SELECT * FROM tab_territories WHERE ID LIKE $par";
        try {
            $ret = $this->simpleQuery($qry);
            // check if exact one element is returned
            if( count($ret) == 0 ) {
                return false;
            } elseif( count($ret) == 1 ) {
                return $ret[0]["col_mapmark"];
            } else {
                throw new Exception("Not exact one Row is returned");
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }

    public function getTerriByName($par) {
        $qry = "SELECT * FROM tab_territories WHERE col_name LIKE '$par'";
        try {
            $ret = $this->simpleQuery($qry);
            // check if exact one element is returned
            if( count($ret) == 0 ) {
                return false;
            } elseif( count($ret) == 1 ) {
                return $ret[0];
            } else {
                throw new Exception("Not exact one Row is returned");
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }

    public function getTerriTypes() {
        $qry = "SELECT * FROM tab_ownerstypes ORDER BY col_name ASC";
        try {
            if( !(is_null($this->cur_qry)) ) {
                throw new Exception("There is allready a query running.");
            } else {
                $this->cur_qry = $this->dbHandle->prepare($qry);
                $this->cur_qry->execute();
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }

    public function getOwners() {
        $qry = "SELECT * "
             . "  FROM tab_owners "
             . " ORDER BY col_surname ASC, col_firstname ASC";
        try {
            if( !(is_null($this->cur_qry)) ) {
                throw new Exception("There is allready a query running.");
            } else {
                $this->cur_qry = $this->dbHandle->prepare($qry);
                $this->cur_qry->execute();
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }

    public function getOwnerIdsByGroupId($p_gid) {
        $ret = [];
        $qry = "SELECT *
                  FROM tab_owners
                 WHERE col_groupid = :groupid
                 ORDER BY col_surname ASC, col_firstname ASC";
        $par['groupid'] = $p_gid;
        try {
            if( is_null($this->cur_qry) ) {
                //print "<h4>in query</h4>";
                $this->cur_qry = $this->dbHandle->prepare($qry);
                if( $this->cur_qry->execute($par) ) {
                    while ( $row = $this->cur_qry->fetch() ) {
                        $ret[] = $row["ID"];
                    }
                    $this->cur_qry = null;
                } else {
                    $this->cur_qry = null;
                    throw new Exception("Execution Error");
                }
            } else {
                throw new Exception("There is allready a query running.");
            }
            return $ret;
        } catch (PDOException $ex) {
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
        return $ret;
    }

    public function getOwnerNameById($par) {
        $qry = "SELECT * FROM tab_owners WHERE ID LIKE $par";
        try {
            $ret = $this->simpleQuery($qry);
            // check if exact one element is returned
            if( count($ret) == 0 ) {
                return false;
            } elseif( count($ret) == 1 ) {
                return $ret[0]["col_surname"] . ", " . $ret[0]["col_firstname"] ;
            } else {
                throw new Exception("Not exact one Row is returned");
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }


    public function getNumOfOwnedTerritories($p_ownerId) {
        $qry = " SELECT *
             FROM tab_times
            WHERE col_ownerid = $p_ownerId AND col_back IS NULL";

        return $this->getNumOf($qry);
    }


    //----------------------------------------------------------------------------------------------
    // link territory information
    //----------------------------------------------------------------------------------------------
    public function insertLinkTerri($p_tidmaster, $p_tidslave) {
        //xdebug_break();
        $qry = "INSERT INTO tab_territoriesrelation (col_territoryidmaster, col_territoryidslave)
                VALUES (:territoryidmaster, :territoryidslave)";
        $par["territoryidmaster"] = $p_tidmaster;
        $par["territoryidslave"] = $p_tidslave;
        try {
            if( !(is_null($this->cur_qry)) ) {
                throw new Exception("There is allready a query running.");
            } else {
                $this->cur_qry = $this->dbHandle->prepare($qry);
                $this->cur_qry->execute($par);
                $this->cur_qry = null;
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }

    public function getNotLinkedTerries() {
        //xdebug_break();
        $qry = "SELECT g.ID, g.col_name
                  FROM tab_territories g
                 WHERE g.ID NOT IN ( SELECT col_territoryidslave FROM tab_territoriesrelation )
                 GROUP BY g.ID
                 ORDER BY g.col_name + 0 ASC, g.col_name ASC";
        $ret = [];
        try {
            if( is_null($this->cur_qry) ) {
                //print "<h4>in query</h4>";
                $this->cur_qry = $this->dbHandle->prepare($qry);
                if( $this->cur_qry->execute() ) {
                    while ( $row = $this->cur_qry->fetch() ) {
                        $ret[] = $row;
                    }
                    $this->cur_qry = null;
                } else {
                    $this->cur_qry = null;
                    throw new Exception("Execution Error");
                }
            } else {
                throw new Exception("There is allready a query running.");
            }
            return $ret;
        } catch (PDOException $ex) {
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
        return $ret;
    }

    public function getLinkedTerries($p_tidmaster) {
        //xdebug_break();
        $qry = "SELECT g.ID, g.col_name
                  FROM tab_territories g
                 WHERE g.ID IN ( SELECT col_territoryidslave
                                   FROM tab_territoriesrelation
                                   WHERE col_territoryidmaster LIKE $p_tidmaster
                                )
                 GROUP BY g.ID
                 ORDER BY g.col_name + 0 ASC, g.col_name ASC";
        $ret = [];
        try {
            if( is_null($this->cur_qry) ) {
                //print "<h4>in query</h4>";
                $this->cur_qry = $this->dbHandle->prepare($qry);
                if( $this->cur_qry->execute() ) {
                    while ( $row = $this->cur_qry->fetch() ) {
                        $ret[] = $row;
                    }
                    $this->cur_qry = null;
                } else {
                    $this->cur_qry = null;
                    throw new Exception("Execution Error");
                }
            } else {
                throw new Exception("There is allready a query running.");
            }
            return $ret;
        } catch (PDOException $ex) {
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
        return $ret;
    }

    public function deleteLinkTerri($p_tidmaster, $p_tidslave) {
        //xdebug_break();
        $qry = "DELETE FROM tab_territoriesrelation
                 WHERE col_territoryidmaster = :territoryidmaster
                   AND col_territoryidslave = :territoryidslave";
        $par["territoryidmaster"] = $p_tidmaster;
        $par["territoryidslave"] = $p_tidslave;
        try {
            if( !(is_null($this->cur_qry)) ) {
                throw new Exception("There is allready a query running.");
            } else {
                $this->cur_qry = $this->dbHandle->prepare($qry);
                $this->cur_qry->execute($par);
                $this->cur_qry = null;
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            throw new PDOException("PDO-Exception: " . $ex->getMessage());
        }
    }

}


// Test Section
/*
// simpleQuery return an array whith the complete SQL query result
// (only usefull for small amount of data)
$db = Database::connect("local");   // test constructor
print "<h1>simpleQuery Test</h1>";
$sql = "SELECT login, password, name, email, gebiete FROM user";
print "<h2>sql: '$sql'</h2>";
$rn = 1;
//print_r( $db->simpleQuery($sql) );
foreach( $db->simpleQuery($sql) as $user ) {
    print "<h3>row $rn</h3>";
    foreach( $user as $key => $value ) {
        print "$key = $value<br>";
    }
    $rn++;
}

$db->disconnect();  // Test destructor


$db = Database::connect("local");
print "<h1>query and fetch Test</h1>";
$sql = "SELECT login, password, name, email, gebiete FROM user";
print "<h2>sql: '$sql'</h2>";
$db->query($sql);
$rn = 1;
//print_r( $db->simpleQuery($sql) );
while( $user = $db->fetch($sql) ) {
    print "<h3>row $rn</h3>";
    foreach( $user as $key => $value ) {
        print "$key = $value<br>";
    }
    $rn++;
}
$db->query($sql);
$db->disconnect();

$db = Database::connect("local");
print "<h1>query and fetch Test</h1>";
$sql = "SELECT login, password, name, email, gebiete FROM user";
print "<h2>sql: '$sql'</h2>";
$db->query($sql);
$rn = 1;
//print_r( $db->simpleQuery($sql) );
while( $user = $db->fetch($sql) ) {
    print "<h3>row $rn</h3>";
    foreach( $user as $key => $value ) {
        print "$key = $value<br>";
    }
    $rn++;
}
$db->disconnect();


$db = Database::connect("local");
print "<h1>selectOneRow Test</h1>";
if( $row = $db->selectOneRow("user", "login", "admin") ) {
    foreach( $row as $key => $value ) {
        print "$key = $value<br>";
    }
} else {
    print "selectOneRow returned false";
}
if( $row = $db->selectOneRow("user", "login", "root") ) {
    foreach( $row as $key => $value ) {
        print "$key = $value<br>";
    }
} else {
    print "selectOneRow returned false";
}
if( $row = $db->selectOneRow("user", "(login", "admin') OR login LIKE 'paul") ) {
    foreach( $row as $key => $value ) {
        print "$key = $value<br>";
    }
} else {
    print "selectOneRow returned false";
}
$db->disconnect();
*/

?>
