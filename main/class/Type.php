<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Type
 *
 * @author EDRauS
 */
class Type {

    private $db = null;
    private $db_id = null;
    private $db_name = null;


    public function __construct($p_row) {

        $this->db           = Database::connect();

        // assign owner information
        $this->db_id     = $p_row['ID'];
        $this->db_name   = $p_row['col_name'];

    }


    public function __destruct() {
        $this->db = null;
    }


    public function getName() {
        return $this->db_name;
    }

    public static function printDropdownHeader($p_action, $p_step) {
        print '<h1>Typ Bearbeiten</h1>'
            . '<form action="index.php?action=' . $p_action . '&step=' . $p_step . '" '
            . 'class="form-inline" method="post">'
            . '<div class="form-group"> <div class="input-group">'
            . '<select class="form-control" name=type[ID] onchange="this.form.submit()">'
            . '<option value="-1">Bitte Typ auswählen</option>'
        ;
    }
    public function printOptionLine($p_id) {
        print '<option value="' . "$this->db_id" . '"'
            . ($this->db_id == $p_id ? " selected " : " " )
            . ">$this->db_name (ID: $this->db_id) </option>";
    }
   public static function printDropdownTailer() {
        print '</select></div></div></form>';
    }


    public function printUpdate($p_action, $p_step) {
        print "<h2>Typ Aktualisieren</h2>";
        print '<form action="index.php?action=' . $p_action . '&step=' . $p_step
            . '" class="form-inline" method="post">'
            . '<div class="form-group"> <div class="input-group">'
            . '<span class="input-group-addon">Name</span>'
            . '<input type="text" class="form-control" value="' . $this->db_name
            . '" name=type[name]>'
            . '</div></div>'
            . '<button type="submit" style="width: 8.5em;" '
            . 'class="btn btn-warning">Aktualisieren</button>'
            . '<input type="hidden" class="text" name=type[ID]" value="' . $this->db_id .'"/>'
            . '</form>';
    }
    public function update() {
        $type = $_REQUEST['type'];
        if ( $this->db_id == $type['ID'] ) {
            if( $this->db->update("type", $type) ) {
                print '<div class="alert alert-success">Typ (ID: '
                    . $type['ID'] . ') zu "' . $type['name'] . '" aktualisiert</div>';
                // update object information
                $this->db_name = $type['name'];
            } else {
                print '<div class="alert alert-danger">Typ konnte nicht aktualisiert werden. '
                    . 'Versuche es bitte noch einmal.</div>';
            }
        } else {
            print '<div class="alert alert-danger">Typ konnte nicht aktualisiert werden. '
                . 'Versuche es bitte noch einmal.</div>';
        }
    }

    public function printRemove($p_action, $p_step) {
        print "<h2>Typ Löschen</h2>";
        print '<button action="index.php?action=' . $p_action . '&step=' . $p_step
            . 'method="post" type="submit" style="width: 8.5em;" '
            . 'class="btn btn-danger">Löschen</button>'
            . '<input type="hidden" class="text" name=id" value="' . $this->db_id .'"/>';
    }

}
