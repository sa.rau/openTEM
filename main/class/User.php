<?php
/***************************************************************************************************

 Author:    Stephan Rau

 Purpose:   provide user information

***************************************************************************************************/

class User {

    private static $inst = null; // used for singeltion impementation
    private static $db;

    private $db_login;
    private $db_name;
    private $db_email;
    private $db_admin;
    private $db_session_id;

    public static function login() {

        if ( self::$inst == null ) {

            // get data data the user entered
            //$req_login = filter_input(INPUT_REQUEST, 'name');     // TODO not working with php 5.5
            $req_login = $_REQUEST['name'];
            //$req_password = filter_input(INPUT_REQUEST, 'pwd');   // TODO not working with php 5.5
            $req_password = $_REQUEST['pwd'];

            // connect to Database to check login data
            self::connect_db();

            // user data array from database
            $data = self::db_get_user_data($req_login);

            if ( isset($data) ) {
                // user name exists => check password
                if ( $data["password"] == md5($req_password) ) {
                    // valid password given => create user object
                    self::$inst = new User($data);
                    self::$inst->checkIfLoggedOut();
                    $_SESSION['login'] = $req_login;
                } else {
                    // password is invalid, disconnect to database and redirect to login page
                    self::disconnect_db();
					print '<div class="alert alert-danger">'
                    . 'Achtung: Dein Passwort war falsch. '
                    . 'Sie werden gleich weitergeleitet. '
                    . 'Wenn das nicht passiert, bitte auf Weiter klicken.</div>'
                    . '<a href="index.php?fehler=2&action=login" class="btn btn-success">Weiter</a>';
                    print '<script> location.href = "index.php?fehler=2&action=login" </script>';
                }
            } else {
                // username does not exists, disconnect database and redirect to login page
                self::$db->disconnect();
				print '<div class="alert alert-danger">'
				. 'Achtung: Dein Benutzername war falsch. '
				. 'Sie werden gleich weitergeleitet. '
				. 'Wenn das nicht passiert, bitte auf Weiter klicken.</div>'
				. '<a href="index.php?fehler=1&action=login" class="btn btn-success">Weiter</a>';
                print '<script> location.href = "index.php?fehler=2&action=login" </script>';
            }

        }

        return self::$inst;

    }

    public static function printLogin( ) {

        // print the login form header
        print '<form class="form-signin" style="max-width: 330px; margin: 0 auto;"'
            . 'action="index.php?action=login&step=verify" method="post">'
            . '<h2 class="form-signin-heading">Bitte melde dich an</h2>';

        // print error message if the login was not successfull
        if( isset($_REQUEST["fehler"]) ) {
            $req_error = $_REQUEST['fehler'];

            switch ( $req_error ) {
                case 1:
                    print '<div class="alert alert-info">'
                        . 'Der Benutzername ist unbekannt. Bitte versuche es erneut.'
                        . '</div>';
                break;
                case 2:
                    print '<div class="alert alert-info">'
                        . 'Das Passwort ist falsch. Bitte versuche es erneut.'
                        . '</div>';
                break;
                default :
                    print '<div class="alert alert-info">'
                        . 'Die Zugangsdaten waren ungültig. Bitte versuche es erneut.'
                        . '</div>';
                break;
            }
        }

        // print the login form
        print '<input type="text" name="name" class="form-control" placeholder="Benutzername" autofocus>'
            . '<input type="password" name="pwd"  class="form-control" placeholder="Passwort">'
            . '<button class="btn btn-lg btn-primary btn-block" type="submit">Anmelden</button>'
            . '</form>' ;
    }

    public static function sessionRelogin() {
        if ( isset($_SESSION['login']) && self::$inst == null ) {

            // connect to Database to check login data
            self::connect_db();

            // user data array from database
            $data = self::db_get_user_data($_SESSION['login']);

            if ( isset($data) && $data['session_id'] == session_id() ) {
                self::$inst = new User($data);
            } else {
                // some trouble with the session_id, disconnect database and redirect to login page
                self::disconnect_db();
                print '<script> location.href = "index.php?action=login" </script>';
            }
        }
        return self::$inst;
    }

    private function __construct($p_data) {
        // create new user object, save information from database
        $this->db_login         = $p_data['login'];
        $this->db_email         = $p_data['email'];
        $this->db_admin         = $p_data['admin'];
        $this->db_session_id    = $p_data['session_id'];
    }

    private function checkIfLoggedOut () {
        $logged_out_last_time = $this->db_session_id == null;

        // get current session id and write to database
        self::db_update_session( session_id() );

        if ( $logged_out_last_time ) {
            print '<div class="alert alert-success">'
                . 'Login erfolgreich. Sie werden gleich weitergeleitet. '
                . 'Wenn das nicht passiert, bitte auf Weiter klicken.'
                . '</div>'
                . '<a href="index.php?action=blog" class="btn btn-success">Weiter</a>';
            print '<script> location.href = "index.php?action=blog" </script>';
        } else {
            print '<div class="alert alert-danger">'
                . 'Achtung: Du hattest dich nicht abgemeldet. '
                . 'Dies kann zu nicht vorhersehbaren Fehlern führen.'
                . 'Bitte melde dich immer ab bevor du deinen Browser schließt '
                . 'oder du deine Arbeit beendet hast.'
                . '<br><strong>Danke!</strong>'
                . '</div>'
                . '<a href="index.php?action=blog" class="btn btn-success">Weiter</a>';
        }
    }

    public function logout() {

        self::db_update_session(null);
        self::disconnect_db();

		session_destroy();
        print '<div class="alert alert-success">'
			. 'Logout erfolgreich. Sie werden gleich weitergeleitet. '
			. 'Wenn das nicht passiert, bitte auf Weiter klicken.'
			. '</div>'
			. '<a href="index.php" class="btn btn-success">Weiter</a>';
        print '<script> location.href = "index.php" </script>';

		self::$inst = null; // call destructor

    }

    public function __destruct() {
        unset( $this->db_login);
        unset( $this->db_name);
        unset( $this->db_email);
        unset( $this->db_admin);
        unset( $this->db_session_id);
        unset( $this->last_activity);
    }

    public function checkSession() {
        // check if still same sessions as in database
        $ret =  TRUE ;

        $data = self::db_get_user_data($_SESSION['login']);

        if ( isset($data) && $data['session_id'] != session_id() ) {
            print '<div class="alert alert-danger">'
                . 'Achtung: Du bist noch wo anders angemeldet. '
                . 'Dies kann zu nicht vorhersehbaren Fehlern führen.'
                . 'Bitte melde dich immer ab bevor du deinen Browser schließt '
                . 'oder du deine Arbeit beendet hast.'
                . '<br><strong>Danke!</strong>'
                . '</div>'
                . '<a href="index.php?action=login&step=logout" '
                . 'class="btn btn-success">Abmelden</a>';
            $ret = FALSE ;
        }

        return $ret;
    }

    public function isLoggedIn() {
        if ( self::$inst == null ) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function hasAdminRight() {
        return $this->db_admin ;
    }

    public function printChangePw () {
        print '<form class="form-signin" style="max-width: 330px; margin: 0 auto;" method="post" '
            . 'action="index.php?action=profil&step=2" >'
            . '<h2 class="form-signin-heading">Passwort ändern</h2>'
			. '<input type="password" name="password"   class="form-control" placeholder="Neues Passwort" autofocus>'
			. '<input type="password" name="password_b" class="form-control" placeholder="Passwort bestätigen">'
			. '<button class="btn btn-lg btn-primary btn-block" type="submit">Ändern</button>'
			. '</form>'
		;
    }

    public function changePw () {
        if($_REQUEST["password"] == $_REQUEST["password_b"]){
            $pw_hash = md5($_REQUEST['password']);
            //$para['user_login'] = $this->db_login;
            if( self::db_update_pw($pw_hash) ) {
                print '<div class="alert alert-success">Passwort erfolgreich geändert.</div>';
            } else {
                print '<div class="alert alert-danger">Es ist ein Fehler aufgetreten.'
                    . 'Bitte versuche es noch einmal.</div>';
            }
        }else{
            print '<script> location.href = "index.php?fehler=1&action=profil" </script>';
        }
    }


    // --------------------------------------------------------------------------------------------
    private static function connect_db() {
        self::$db = new PDO("sqlite:./database/user.sqlite3");
    }

    private static function disconnect_db() {
        self::$db = null;
    }

    private static function db_get_user_data($p_login) {
        $ret = null;

        $qry_string = "SELECT * FROM users WHERE login LIKE '$p_login'";
        $qry_result = self::$db->query($qry_string);
        $qry_return = $qry_result->fetchAll(PDO::FETCH_ASSOC);

        // check if exact one element is returned, else there is an error
        if( count($qry_return) == 0 ) {
            //throw new Exception("Login not found");
            // username does not exists, diconnect database and redirect to login page
            self::disconnect_db();
            print '<script> location.href = "index.php?fehler=1&action=login" </script>';
        } elseif( count($qry_return) == 1 ) {
            $ret = $qry_return[0];
        } else {
            // throw new Exception("Login matches more than once");
            self::disconnect_db();
            print '<script> location.href = "index.php?fehler=1&action=login" </script>';
        }

        return($ret);
    }

    private function db_update_session($p_session) {
        // throw Exception ???
        $para['login'] = $this->db_login;
        $this->db_session_id = $p_session;
        $para['session_id'] = $this->db_session_id;
        $cur_qry = self::$db->prepare("UPDATE users SET session_id = :session_id WHERE login = :login");
        return ( $cur_qry->execute($para) );
    }

    private function db_update_pw($p_password) {
        // throw Exception ???
        $para['login'] = $this->db_login;
        $para['password'] = $p_password;
        $cur_qry = self::$db->prepare("UPDATE users SET password = :password WHERE login = :login");
        return ( $cur_qry->execute($para) );
    }

}

