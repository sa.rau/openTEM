<?php
/***************************************************************************************************

 Author:    Stephan Rau

 Propose:   handle all territory database queries and input formulas

 TODO:
    o add commit question before database update
    o add sanity checks: territory name, streets

 **************************************************************************************************/
require_once( "./class/Territory.php"   );

class TerritoryNurse {

    private $db = null;          // database connection object

    // create header array TODO move to maybe move to a include file for multilingual
    //-- name mapping to step number
    const ALL           = 0;
    const ASSIGNED      = 1;
    const FREE          = 2;
    const REPORT        = 3;
    const EDIT          = 4;
    const ADD           = 5;
    const ADD_INFO      = 6;
    const CARDS         = 7;
    //-- name mapping to do number
    const DO_ADD           = 1;
    const DO_EDIT          = 2;
    const DO_ADD_TIME      = 3;
    const DO_EDIT_TIME     = 4;
    const DO_ADD_STREET    = 5;
    const DO_EDIT_STREET   = 6;
    const DO_ADD_INFO      = 7;
    const DO_EDIT_INFO     = 8;
    const DO_LINK_TERRI    = 9;
    const DO_LINK_REMOVE   = 10;
    //-- step name mapping to languarge
    private static $HEADERS = array(
        self::EDIT          => "Bearbeiten",
        self::ADD           => "Hinzufügen",
        self::ALL           => "Alle",
        self::ASSIGNED      => "Ausgegeben",
        self::FREE          => "Frei",
        self::REPORT        => "Bericht",
        self::ADD_INFO      => "Zusatzinformationen",
        self::CARDS         => "Karten drucken"
    );
    // create header names and number of records for each section
    private $noTerri = array(
        self::ALL           => "Unbekannt",
        self::ASSIGNED      => "Unbekannt",
        self::FREE          => "Unbekannt",
        self::REPORT        => "S-13-X-mod"
    );

    //-- class variables
    private $id     = null;     // territory id
    private $action = null;
    private $step   = null;
    private $do     = null;

    private $groups = null;
    private $types  = null;
    private $owners = null;

    private $terriArray = array();

    const ACTION = "terri";

    //---- __construct -----------------------------------------------------------------------------
    // Propose:     connect to database
    // Parameter:   action name
    // Return:      new object is returned
    public function __construct($action) {
        $this->action = $action;

        $this->db = Database::connect();

        // TODO merge into rowCount select function
        $this->noTerri[self::ALL] = $this->db->getNumOfAllTerries();

        $this->noTerri[self::ASSIGNED] = $this->db->getNumOfAssignedTerries();

        $this->noTerri[self::FREE] = $this->db->getNumOfFreeTerries();

        $this->groups = GroupNurse::startDialogue();  // create before OwnerNurse, because she talks to
        $this->types  = TypeNurse::startDialogue();   // create before OwnerNurse, because she talks to
        $this->owners = OwnerNurse::startDialogue();
    }


    //---- __destruct ------------------------------------------------------------------------------
    // Propose:     close databse connection
    // Parameter:   none
    // Return:      none
    public function __destruct() {
        //$this->db->disconnect; ?? dont work ??
        //self::$db = null;
        unset ( $this->db );
    }


    //---- getHead ---------------------------------------------------------------------------------
    // Propose:     create a header string which constains the number of territories
    // Parameter:   none
    // Return:      string
    public function getHead($step) {
        // TODO try catch block?
        $ret = "";
        switch( $step ) {
            case self::ALL           :
            case self::ASSIGNED      :
            case self::FREE          :
            case self::REPORT        :
               $ret = self::$HEADERS[$step] . " (" . $this->noTerri[$step] . ")" ;
            break;
            case self::EDIT          :
            case self::ADD           :
            case self::ADD_INFO      :
            case self::CARDS         :
               $ret = self::$HEADERS[$step] ;
            break;
        }
        return $ret ;
    }
    public function printLink($step) {
        print "<li><a href=\"index.php?action=$this->action&step=$step\">" . $this->getHead($step) . "</a></li>";
    }


    //---- printReport -----------------------------------------------------------------------------
    // Propose:     print depended on step a report
    // Parameter:   step
    // Return:      none
    public function printReport($step) {

        switch($step) {

            case self::ALL:
            // TODO change to multilingual implementation
                print "<h1>Bericht über alle Gebiete</h1>";
                print '<div class="container"> <table class="table"> <thead> <tr>';
                print '<th>#</th>';
                print '<th>Gebiet Name</th>';
                print '<th>Anzahl Wohnungen</th>';
                print '<th>Aktueller Halter</th>';
                print '<th>Gruppe</th>';
                print '<th>Strassen</th>';
                print '</tr> </thead> <tbody>';

                // create temporary for each gebiet a own object
                $this->db->getAllTerries();
                $tmpListOfTerri = [];
                while ( $row = $this->db->fetch() ) {
                    $tmpListOfTerri[] = new Territory($row);
                }
                // print all territory strings
                foreach ( $tmpListOfTerri as $curTerri ) {
                    print $curTerri->getHtmlTableRowString(Territory::ALL) ;
                }
                // clean up temporary variables
                unset( $tmpListOfTerri );
                unset( $curTerri );

                print '</tbody> </table> </div>';
                break;

            case self::ASSIGNED:
            // Ausgegeben
                print "<h1>Bericht über ausgegebene Gebiete</h1>";
                print '<div class="container"> <table class="table header-fixed"> <thead> <tr>';
                print '<th>#</th>';
                print '<th>Gebiet Name</th>';
                print '<th>Anzahl Wohn.</th>';
                print '<th>Datum letzten Bearb.</th>';
                print '<th>Monate letzten Bearb.</th>';
                print '<th>Fortschritt</th>';
                print '<th>Monate im Besitz</th>';
                print '<th>Halter</th>';
                print '<th>PDG</th>';
                print '</tr> </thead> <tbody>';

                // create temporary for each gebiet a own object
                $this->db->getAssignedTerries();
                $tmpListOfTerri = [];
                while ( $row = $this->db->fetch() ) {
                    $tmpListOfTerri[] = new Territory($row);
                }
                // print all territory strings
                foreach ( $tmpListOfTerri as $curTerri ) {
                    print $curTerri->getHtmlTableRowString(Territory::ASSIGNED) ;
                }
                // clean up temporary variables
                unset( $tmpListOfTerri );
                unset( $curTerri );

                print '</tbody> </table> </div>';
                break;

            case self::ADD_INFO:
            // show table of additional information
                //xdebug_break();
                print "<h1>Zusatzinformation der Gebiete</h1>";

                // headers (language depended) TODO global
                $json_string = file_get_contents("./config/header.json");
                $header_all = json_decode($json_string, true);
                $header = $header_all["terri_add_info"];

                $json_string = file_get_contents("./config/territoriesaddinfo.json");
                $taicn = json_decode($json_string, true); // territory additional info column name

                // create temporary for each gebiet a own object
                $this->db->getAllTerries();
                while ( $row = $this->db->fetch() ) {
                    $tmpListOfTerri[$row["ID"]] = new Territory($row);
                }

                // build table header
                print '<div class="container"> <table class="table header-fixed"> <thead> <tr>';
                print '<th>#</th>';
                print '<th>Gebiet</th>';
                foreach ($taicn as $col_no => $col_setting) {
                    print '<th>' . $header[$col_setting["name"]] . '</th>';
                }
                print "</tr> </thead>\n";

                // show existing entries as table body
                print '<tbody>';

                $this->db->getTerriAddInfoAll();
                while($row = $this->db->fetch()){
                    print "<tr>";
                    print "<td>LDF</td>";
                    if ( array_key_exists($row["col_territoryid"],$tmpListOfTerri) ) {
                        print "<td>" . $tmpListOfTerri[$row["col_territoryid"]]->getUpdateLink() . "</td>";
                    } else {
                        print "<td> ID '" . $row["col_territoryid"] . "' NA</td>";
                    }
                    $value = json_decode($row["col_infojson"],true);
                    foreach ($taicn as $col_no => $col_setting) {
                        print "<td>" . $value[$col_no] . "</td>";
                    }
                    print "</tr>\n" ;
                }

                // clean up temporary variables
                unset( $tmpListOfTerri );


                print '</tbody> </table> </div>';
                break;

            case self::FREE:
            // Frei
                print "<h1>Bericht über freie Gebiete</h1>";
                print '<div class="container"> <table class="table"> <thead> <tr>';
                print '<th>#</th>';
                print '<th>Gebiet</th>';
                print '<th>#Wohn</th>';
                print '<th>Blockgebiet</th>';
                print '<th>Strassen</th>';
                print '<th>Datum</th>';
                print '<th>Monate</th>';
                print '<th>Letzter Halter</th>';
                print '</tr> </thead> <tbody>';

                // create temporary for each gebiet a own object
                $this->db->getFreeTerries();
                $tmpListOfTerri = [];
                while ( $row = $this->db->fetch() ) {
                    $tmpListOfTerri[] = new Territory($row);
                }

                // print all territory strings
                foreach ( $tmpListOfTerri as $curTerri ) {
                    print $curTerri->getHtmlTableRowString(Territory::FREE) ;
                }
                // clean up temporary variables
                unset( $tmpListOfTerri );
                unset( $curTerri );

                print '</tbody> </table> </div>';
                break;

            case self::REPORT:
                print'
                <h2><a href="gebiete_pdf.php?step=4&tabsPerSheet=1" target="_blank">S-13-X-mod-1</a></h2>
                <p>Erstellt ein PDF mit 5 Gebieten pro Seite, mit einer Historie von 26 Einträgen je Gebiet.</p>
                <h2><a href="gebiete_pdf.php?step=4&tabsPerSheet=2" target="_blank">S-13-X-mod-2</a></h2>
                <p>Erstellt ein PDF mit 10 Gebieten pro Seite, mit einer Historie von 11 Einträgen je Gebiet.</p>
                <h2><a href="gebiete_pdf.php?step=4&tabsPerSheet=3" target="_blank">S-13-X-mod-3</a></h2>
                <p>Erstellt ein PDF mit 15 Gebieten pro Seite, mit einer Historie von 6 Einträgen je Gebiet.</p>
                <br/><h2><a href="gebiete_pdf.php?step=1" target="_blank">Gruppenbericht</a></h2>
                <p>Erstellt ein PDF mit einer Liste der ausgegeben Gebiete pro Gruppe.</p>
                ';
                //<br/><h2><a href="gebiete_pdf.php?step=0" target="_blank">Inventurbericht</a></h2>
                //<p>Erstellt ein PDF zum Inventur abgleich.</p>
                //';
                $_SESSION["user_login"] = "default";
                $_SESSION["user_gebiete"] = 1;
                break;

            case self::CARDS:
                $this->evalPhpParameter();
                $this->db->getAllTerries();
                print "<h1>Gebietskarte Drucken</h1>
                <form action=\"index.php?action=$this->action&step=" . self::CARDS . '" class="form-inline" method="post">
                    <div class="form-group"> <div class="input-group">
                        <span class="input-group-addon">Gebiet</span>
                        <select class="form-control" name=territory_string onchange="this.form.submit()">
                        <option value="-1,none,none,none">Bitte Gebiet auswählen</option>
                ';
                while($row = $this->db->fetch()){
                    print "
                            <option value=\"$row[ID],$row[col_name]\"" .
                            ( $this->id == $row['ID'] ? " selected " : " ")
                            . ">$row[col_name]</option>
                    ";
                }
                print '
                        </select>
                    </div></div>
                </form>
                ';
                if ( $this->definedId() ) {
                    include("cards.php");
                }

                break;

            default:
                print "<div class=\"alert alert-warn\">Irgendwas ist schief gegangen geh bitte zurück</div>";
                break;
        }

    }


    //---- printDropdown ---------------------------------------------------------------------------
    // Propose:     print a drop down list with all available territories
    // Parameter:   none ( for current selected territory class variable is used )
    // Return:      none
    public function printDropdown() {

        $this->db->select("gebiete_sorted");
        print "<h1>Gebiet Bearbeiten</h1>
        <form action=\"index.php?action=$this->action&step=" . self::EDIT . '" class="form-inline" method="post">
            <div class="form-group"> <div class="input-group">
                <span class="input-group-addon">Gebiet</span>
                <select class="form-control" name=territory_string onchange="this.form.submit()">
                  <option value="-1,none,none,none">Bitte Gebiet auswählen</option>
        ';
        while($row = $this->db->fetch()){
            print "
                    <option value=\"$row[ID],$row[col_name]\"" .
                    ( $this->id == $row['ID'] ? " selected " : " ")
                    . ">$row[col_name]</option>
            "; // TODO option to show owner ID
        }
        print '
                </select>
            </div></div>
        </form>
        ';
    }


    //---- printTimeEntries ------------------------------------------------------------------------
    // Propose:
    // Parameter:
    // Return:
    public function printTimeEntries() {
        $gpar['ID'] = $this->id;
        switch ( $this->terriArray[$this->id]->getType() ) {
            case Territory::TYPE['ROOT'] :
                if( $this->terriArray[$this->id]->checkRoot() ) {
                    print "<h2>Zuteilungen Bearbeiten</h2>";
                    print "<div class=\"alert alert-warning\">"
                    . "Ein <strong>Stammgebiet</strong> sollte keine Zuteilungen haben. "
                    . "Bitte kontrolieren ob dieses Gebiet ein Stammgebiet sein soll. "
                    . "Wenn NEIN Gebietstyp ändern, wenn JA die Einträge löschen."
                    . "</div>";
                }
                break;
            case Territory::TYPE['REGULAR'] :
            case Territory::TYPE['ADDRESS'] :
            case Territory::TYPE['SEARCH'] :
                print "<h2>Zuteilungen Bearbeiten</h2>";
                print "<h3>Neue Zuteilung erstellen</h3>";
                $this->db->select("halter_alle");
                print '
                <form action="index.php?action=' . $this->action . '&step=' . self::EDIT . '&do=' . self::DO_ADD_TIME .'" class="form-inline" method="post">
                    <div class="form-group" style="width: 24em;"> <div class="input-group">
                        <span class="input-group-addon">Halter</span>
                        <select class="form-control" name=halter_string>
                        print "<option value=0,0,0,0">Bitte Halter auswählen</option>";
                ';
                while($row = $this->db->fetch()){
                    print "<option value=\"$row[ID],$row[col_surname],$row[col_firstname],$row[col_ownertypeid]\">$row[col_surname], $row[col_firstname]</option>";
                }
                print '
                        </select>
                    </div></div>
                    <div class="form-group" style="width: 17em;"> <div class="input-group">
                            <span class="input-group-addon">Ausgegeben</span>
                            <input type="date" style="color:green;" class="form-control" value="' . date('Y-m-d') . '" name=gebiet[assigned]>
                    </div> </div>
                    <div class="form-group" style="width: 21em;"> <div class="input-group">
                            <span class="input-group-addon">Zurück</span>
                            <input type="date" class="form-control" placeholder="YYYY-MM-DD" name="gebiet[back]">
                            <span class="input-group-addon"><input type="checkbox" name=gebiet[verloren]>  Verloren</span>
                    </div> </div>
                    <div class="form-group" style="width: 10.0em;"> <div class="input-group">
                            <span class="input-group-addon">Fortschritt</span>
                            <input type="text" class="form-control" placeholder="50" name=gebiet[progress]>
                    </div> </div>
                    <button type="submit" class="btn btn-success" name="update" data-toggle="tooltip" title="Zuteilen">
                        <span class="glyphicon glyphicon-plus"></span>
                    </button>
                    <input type="hidden" class="text" name="gebiet[ID]"    value="' . $this->id .'"/>
                </form>
                ';
            break;
        }

        $h1 = 0;
        $h2 = 0;
        $this->db->selectCon("gebiet_by_ID_dsc", $gpar);
        while($row = $this->db->fetch()){
            if( $row['col_lost'] == 1 ) {   $checked = "checked=\"1\"";   } else {   $checked = "";    }
            if( $h1 == 0 ) {
                print "<h3>Aktuelle Zuteilung bearbeiten</h3>";
                $h1 = 1;
            }
            if( isset($row['col_back']) &&  $h2 == 0) {
                print "<h3>Abgeschlossene Zuteilungen bearbeiten</h3>";
                $h2 = 1;
            }
            print '
            <form action="index.php?action=' . $this->action . '&step=' . self::EDIT . '&do=' . self::DO_EDIT_TIME .'" class="form-inline" method="post">
                <div class="form-group" style="width: 24em;"> <div class="input-group">
                      <span class="input-group-addon">Halter</span>
                      <input type="text" class="form-control" value="' . $row['col_surname'] . " " . $row['col_firstname'] . '" disabled>
                </div> </div>
                <div class="form-group" style="width: 17em;"> <div class="input-group">
                    <span class="input-group-addon">Ausgegeben</span>
                    <input type="date" class="form-control" value="' . $row['col_assigned'] . '" name="gebiet[assigned]">
                </div> </div>
                <div class="form-group" style="width: 21em;"> <div class="input-group">
                    <span class="input-group-addon">Zurück</span>
                      <input type="date" class="form-control" value="' . $row['col_back'] . '" name=gebiet[back]>
                      <span class="input-group-addon"><input type="checkbox" ' . $checked . ' name=gebiet[verloren]>  Verloren</span>
                </div> </div>
                <div class="form-group" style="width: 10.0em;"> <div class="input-group">
                    <span class="input-group-addon">Fortschritt</span>
                    <input type="text" class="form-control" value="' . $row['col_progress'] . '" name=gebiet[progress]>
                </div> </div>
                <button type="submit" class="btn btn-warning" name="update" data-toggle="tooltip" title="Ändern">
                    <span class="glyphicon glyphicon-ok"></span>
                </button>
                <button type="submit" class="btn btn-danger" name="delete" data-toggle="tooltip" title="Löschen">
                    <span class="glyphicon glyphicon-remove"></span>
                </button>
                <input type="hidden" class="text" name="gebiet[z_ID]"  value="' . $row['z_ID'] .'"/>
                <input type="hidden" class="text" name="gebiet[ID]"    value="' . $row['g_ID'] .'"/>
                <input type="hidden" class="text" name="gebiet[h_ID]"  value="' . $row['h_ID'] .'"/>
            </form>
            ';
        }
    }


    //---- printLinkedTerries ----------------------------------------------------------------------
    // Propose:
    // Parameter:
    // Return:
    public function printLinkedTerries() {
        $gpar['ID'] = $this->id;
        switch ( $this->terriArray[$this->id]->getType() ) {
            case Territory::TYPE['REGULAR'] :
            case Territory::TYPE['SEARCH'] :
                break;
            case Territory::TYPE['ROOT'] :
            case Territory::TYPE['ADDRESS'] :
                print "<h2>Gebiets Verknüpfungen bearbeiten</h2>";
                print "<h3>Ein Gebiet verknüpfen</h3>";
                print '<form action="index.php?action=' . $this->action
                                             . '&step=' . self::EDIT
                                               . '&do=' . self::DO_LINK_TERRI
                    . '" class="form-inline" method="post">'
                    . '<div class="form-group"> <div class="input-group">' . "\n"
                    . '    <span class="input-group-addon">Gebiet</span>' . "\n"
                    . '    <select class="form-control" name=territory_string>' . "\n"
                    . '        <option value="' . $this->id . ',NULL,NULL">'
                    .              'Bitte zu verkünpfendes Gebiet auswählen'
                    . '        </option>' . "\n"
                ;
                //$this->db->select("gebiete_sorted"); // only not connected and not ROOT or ADDRESS
                foreach ( $this->db->getNotLinkedTerries() as $terri ) {
                    // todo check if exists else create territory
                    if( $this->id != $terri['ID']) {
                        print "<option value=\"" . $this->id . ",$terri[ID],$terri[col_name]\">".
                            $terri['col_name'] .
                        "</option>";
                    }
                }
                print '
                        </select>
                    </div></div>
                    <button type="submit" class="btn btn-success" name="update" data-toggle="tooltip" title="Verknüpfen">
                        <span class="glyphicon glyphicon-plus"></span>
                    </button>
                    <input type="hidden" class="text" name="gebiet[ID]"    value="' . $this->id .'"/>
                </form>
                ';

                print "<h3>Aktuelle Verknüfungen bearbeiten</h3>";
                foreach ( $this->db->getLinkedTerries($this->id) as $terri ) {
                    if( !isset($this->terriArray[$terri['ID']]) ) {
                        $this->terriArray[$terri['ID']] = new Territory($this->db->getTerri($terri['ID']));
                    }
                    print '
                    <form action="index.php?action=' . $this->action . '&step=' . self::EDIT . '&do=' . self::DO_LINK_REMOVE .'" class="form-inline" method="post">
                        <div class="form-group"> <div class="input-group">' .
                            '<span class="input-group-addon">Gebiet</span>' .
                            '<span class="input-group-addon" style="min-width: 18em;">'.
                                $this->terriArray[$terri['ID']]->getUpdateLink()
                            . '</span></div></div>
                        <button type="submit" class="btn btn-danger" name="delete" data-toggle="tooltip" title="Löschen">
                            <span class="glyphicon glyphicon-remove"></span>
                        </button>
                        <input type="hidden" class="text" name="relation[masterid]"  value="' . $this->id .'"/>
                        <input type="hidden" class="text" name="relation[slaveid]"   value="' . $terri['ID'] .'"/>
                        <input type="hidden" class="text" name="gebiet[ID]"    value="' . $this->id .'"/>
                    </form>';
                }

                // add javascript with sub terri info
                print "<script>\n";
                print "var subterri = [];\n";
                foreach (  $this->db->getLinkedTerries($this->id) as $terri ) {
                    print "subterri.push({"
                        . "name:'" . $this->terriArray[$terri['ID']]->getName() . "',"
                        . "mark:" . $this->terriArray[$terri['ID']]->getMapmark()
                    ."});\n";
                }
                print "</script>\n";
                break;

        }
    }


    //---- printEdit -------------------------------------------------------------------------------
    // Propose:
    // Parameter:
    // Return:
    public function printEdit() {
        print "<h2>Gebiets Daten Bearbeiten</h2>";
        $this->terriArray[$this->id]->printEdit();
        $this->terriArray[$this->id]->printEditMap();
    }


    //---- printAdd --------------------------------------------------------------------------------
    // Propose:
    // Parameter:
    // Return:
    public function printAdd() {
        // add new Gebiet formular
        print "<h2>Neues Gebiet einfügen</h2>";
        $json_string = file_get_contents("./config/header.json");
        $header_all = json_decode($json_string, true);
        //print "<div class=\"alert alert-info\">Ein paar Informationen.</div>";
        // get free gebiet numbers
        $clean_name = array();
        $this->db->select("gebiete_alle");
        while($row = $this->db->fetch()){
            array_push($clean_name, trim($row['col_name'], 'ABC'));
        }
        sort($clean_name);
        $clean_name_unique = array_unique( $clean_name );
        $last_name = end($clean_name_unique);
        print "<div class=\"alert alert-info\">Freie Gebietsnummern: ";
        for ($i = 1; $i <= $last_name; $i++) {
            if (!in_array($i, $clean_name_unique)) {
                print "$i, ";
            }
        }
        print "$i</div>";
        unset ($i, $clean_name, $clean_name_unique);

        // add new Gebiet formular
        print '
            <form action="index.php?action=' . $this->action . '&step=' . self::EDIT . '&do=' . self::DO_ADD .'" class="form-inline" method="post">
                <div class="form-group"> <div class="input-group">
                      <span class="input-group-addon">Name</span>
                      <input style="width: 10em;" type="text" class="form-control" placeholder="222" name=gebiet[name]>
                </div> </div>
                <div class="form-group"> <div class="input-group">
                      <span class="input-group-addon">Wohnungen</span>
                      <input style="width: 5em;" type="text" class="form-control" placeholder="100" name=gebiet[flats]>
                </div> </div>
                <div class="form-group"> <div class="input-group">
                      <span class="input-group-addon">Blockgebiet</span>
                      <input style="width: 4em;" type="text" class="form-control" placeholder="50" name=gebiet[blockpart]>
                      <span class="input-group-addon">%</span>
                </div> </div>
                <div class="form-group"> <div class="input-group">
                      <span class="input-group-addon">Typ</span>
                      <select style="width: 10em;" class="form-control" name=gebiet[type]>';
                      foreach ( Territory::TYPE as $value) {
                          print '<option value="' . $value . '">'
                              . $header_all["terri_type"][$value]
                              . '</option>';
                      }
        print '       </select>
                </div> </div>
                <div class="form-group" style="width: 22.0em;"> <div class="input-group">
                      <span class="input-group-addon">Ort</span>
                      <input type="text" class="form-control" placeholder="81545 Harlaching" name=gebiet[place]>
                </div> </div>
                <button type="submit" class="btn btn-success" data-toggle="tooltip" title="Hinzufügen">
                    <span class="glyphicon glyphicon-plus"></span>
                </button>
                <div class="form-group" style="width: 100%;"> <div class="input-group">
                    <span class="input-group-addon">Notiz</span>
                        <input type="text" class="form-control" placeholder="Gebiet relevant aber nicht auf Ausdruck sichtbar." name=gebiet[comment]>
                        <span class="input-group-addon"><input type="checkbox" "" name=gebiet[ask]> Gebietsdiener Ansprechen</span>
                </div> </div>
            </form>
        ';
    }

    //---- printEditStreets ------------------------------------------------------------------------
    // Propose:
    // Parameter:
    // Return:
    public function printEditStreets() {

        // show street entries and button to delete, modify or add
        // select related to current territory type

        switch( $this->terriArray[$this->id]->getType() ) {

            case Territory::TYPE['ROOT'] :
                print "<h2>Strassen von verknüpften Gebieten</h2>\n";
                print '<table class="table table-sm">';
                print '<thead><tr class="success"><th>Gebiet</th><th>Strasse</th><th>Notiz</th> </tr></thead>';
                print "<tbody>\n";
                // run through related tables
                $i = 0;  // neded to alternate row color with territory change
                foreach ( $this->db->getLinkedTerries($this->id) as $terri ) {
                    if( $this->terriArray[$terri['ID']]->printStreetsTableEntries($i) ) {
                        $i++; // just change row color if current territory has street entries
                    }
                }
                print "</tbody> </table>";
            case Territory::TYPE['REGULAR'] :
                print "<h2>Strassen des aktullen Gebiets Bearbeiten</h2>";
                $this->terriArray[$this->id]->printEditStreets("Strasse", "Notiz");
                $this->terriArray[$this->id]->printAddStreet("Strasse", "Notiz");
            break;

            case Territory::TYPE['ADDRESS'] :
                print "<h2>Adressen von verknüpften Gebieten</h2>";
                print '<table class="table table-sm">';
                print '<thead><tr class="success"><th>Suchgebiet</th><th>Adresse</th><th>Name</th> </tr></thead> ';
                print '<tbody>';
                // run through related tables
                $i = 0;  // neded to alternate row color with territory change
                foreach ( $this->db->getLinkedTerries($this->id) as $terri ) {
                    if( $this->terriArray[$terri['ID']]->printStreetsTableEntries($i) ) {
                        $i++; // just change row color if current territory has street entries
                    }
                }
                print '</tbody> </table>';
            case Territory::TYPE['SEARCH'] :
                print "<h2>Adressen Bearbeiten</h2>";
                $this->terriArray[$this->id]->printEditStreets("Adresse", "Name");
                $this->terriArray[$this->id]->printAddStreet("Adresse", "Name");
            break;

        }

    }


    //---- printAddInfo ----------------------------------------------------------------------------
    // Propose:
    // Parameter:
    // Return:
    public function printAddInfo() {

        // show street entries and button to delete, modify or add
        // select related to current territory type

        switch( $this->terriArray[$this->id]->getType() ) {

            case Territory::TYPE['ROOT'] :
            case Territory::TYPE['ADDRESS'] :
                print "<h2>Zusatzinformationen von verknüpften Gebieten</h2>";
                print '<table class="table table-sm">';

                // headers (language depended) TODO global
                $json_string = file_get_contents("./config/header.json");
                $header_all = json_decode($json_string, true);
                $header = $header_all["terri_add_info"];

                $json_string = file_get_contents("./config/territoriesaddinfo.json");   // TODO move to constructor ?
                $taicn = json_decode($json_string, true); // territory additional info column name

                print '<thead><tr class="success"><th>Suchgebiet</th>';
                foreach ($taicn as $col_no => $col_setting) {
                    print '<th>' . $header[$col_setting["name"]] . '</th>';
                }
                print '</thead> ';
                print '<tbody>';

                // run through related tables
                $i = 0;  // neded to alternate row color with territory change
                foreach ( $this->db->getLinkedTerries($this->id) as $terri ) {
                    if( $this->terriArray[$terri['ID']]->printAddInfoTableEntries($i) ) {
                        $i++; // just change row color if current territory has street entries
                    }
                }
                print '</tbody> </table>';

            case Territory::TYPE['REGULAR'] :
            case Territory::TYPE['SEARCH'] :
                print "<h2>Zusatzinformationen Bearbeiten</h2>";
                $this->terriArray[$this->id]->printEditAddInfo();
                $this->terriArray[$this->id]->printAddAddInfo();
            break;

        }

    }

    //---- evalPhpParameter ------------------------------------------------------------------------
    // Propose:     eval step and do php address line parameter
    // Parameter:
    // Return:
    public function evalPhpParameter() {
        // check action
        //
        // check step
        if( isset($_REQUEST['step']) ) {
            $this->step = $_REQUEST['step'];
        } else {
            $this->step = null;
        }

        // check do
        if( isset($_REQUEST['do']) ) {
            $this->do = $_REQUEST['do'];
        } else {
            $this->do = null;
        }

        // check id
        if( isset($_GET['terri']) ) {
            $this->id = $_GET['terri'];
        } elseif( isset($_REQUEST['territory_string']) ) {
            //$h_ID = explode( ',', $_REQUEST['halter_string'])[0]; // this don't work on the webserver
            $garray = explode( ',', $_REQUEST['territory_string']);
            $this->id = $garray[0];
        //} elseif( isset($newGebiet) ) { // new gebiet added
        //    $this->id = $newGebiet['ID'];
        //    unset( $newGebiet );
            //$this->do = 2; // to select gebiet and show all gebiete formulars
        } elseif( isset($_REQUEST['gebiet']) ) {
            if( $this->do == self::DO_ADD ) {
                $this->id = null;
            } else {
                $this->id = $_REQUEST['gebiet']['ID'];
            }
        } else {
            // nach seiten aufruf
            $this->id = null;
        }

        if ( isset($this->id) ) {
            $this->terriArray[$this->id] = new Territory($this->db->getTerri($this->id));
        }

    }


    //---- definedId -------------------------------------------------------------------------------
    // Propose:     check if id is defined
    // Parameter:   none
    // Return:      boolean
    public function definedId() {
        $ret = true;
        if( $this->id == null ) {
            $ret = false;
        }
        return $ret;
    }


    //---- updateDatabase --------------------------------------------------------------------------
    // Propose:     eval do step and update database
    // Parameter:   ?
    // Return:      owner ID
    public function updateDatabase() {

        // TODO move to db class ????
        function check_date ( $date ) {
            //print "<h1>Date to check '$date'</h1>" ;
            if( preg_match("/\d\d\d\d-\d\d-\d\d/", $date) ) {
                return 1;
            } else {
                return 0;
            }
        }

        switch( $this->do ) {

        case self::DO_ADD: // add new gebiet
            //xdebug_break();
            $gebiet = $_REQUEST['gebiet'];
            //print "<br/>";
            if( $gebiet['name'] == '' || $gebiet['flats'] == '' ) {
               print "<div class=\"alert alert-danger\"><strong>Fehler:</strong> Für ein Gebiet" .
                       " muss ein Name und die Anzahl der Wohnungen eingeben werden. Bitte versuche es" .
                       " nochmal.</div>";
            } else {
                if( $gebiet['blockpart'] == '' ) { $gebiet['blockpart'] = null; }
                if( $gebiet['type'] == '' ) { $gebiet['type'] = null; }
                if( $gebiet['place'] == '' ) { $gebiet['place'] = null; }
                if( isset($gebiet['ask']) && $gebiet['ask'] == 'on' ) {
                    $gebiet['ask'] = 1;
                } else {
                    $gebiet['ask'] = 0;
                }
                // check if territory with same name exists
                if( $this->db->getTerriByName($gebiet['name']) == false ) {
                    // insert new territory
                    if( $this->db->insert("gebiet", $gebiet) ) {
                        // get new territory entry from db to get id for current object
                        $newGebiet = $this->db->getTerriByName($gebiet['name']);
                        $this->id = $newGebiet['ID'];
                        $this->terriArray[$this->id] = new Territory($this->db->getTerri($this->id));
                    } else {
                        print "<div class=\"alert alert-danger\">Neues Gebiet: '$gebiet[name]' mit '$gebiet[flats]' Wohnungen konnte nicht hinzugefügt werden. Versuche es bitte noch einmal.</div>";
                    }
                } else {
                    print "<div class=\"alert alert-danger\">Neues Gebiet: '$gebiet[name]' mit '$gebiet[flats]' Wohnungen konnte nicht hinzugefügt werden, der Name ist schon vergeben. Versuche es bitte noch einmal.</div>";
                }
            }
        break;

        case self::DO_EDIT: // modify and delete the gebiet entry from database
            if( isset($_POST['update']) ) {
                //xdebug_break();
                $gebiet = $_REQUEST['gebiet'];
                if( isset($gebiet['ask']) && $gebiet['ask'] == 'on' ) {
                    $gebiet['ask'] = 1;
                } else {
                    $gebiet['ask'] = 0;
                }
                if( $this->db->update("gebiet", $gebiet) ) {
                    //print "<div class=\"alert alert-success\">Strasse (ID $strasse[ID]): '$strasse[street]' aktualisiert</div>";
                    $this->terriArray[$this->id] = new Territory($this->db->getTerri($this->id));
                } else {
                    print "<div class=\"alert alert-danger\">Gebiet: 'Name: $gebiet[name], ID: $gebiet[ID]' konnte nicht aktualisiert werden. Versuche es bitte noch einmal.</div>";
                }
            } elseif( isset($_POST['delete']) ) {
                $gpar['ID'] = $_REQUEST['gebiet']['ID'];
                if( $this->db->delete("gebiet", $gpar) ) {
                    //print "<div class=\"alert alert-success\">Strasse (ID $strasse[ID]): '$strasse[street]' gelöscht</div>";
                    $gpar['ID'] = "-1";
                    unset($this->terriArray[$this->id]);
                } else {
                    print "<div class=\"alert alert-danger\">Gebiet: 'Name: $gebiet[name], ID: $gebiet[ID]' konnte nicht gelöscht werden. Versuche es bitte noch einmal.</div>";
                }
            } else {
                print "<div class=\"alert alert-warning\">Schiefgegangen.</div>";
            }
        break;

        case self::DO_ADD_INFO: // add additonal terri info
            //xdebug_break();
            $info = $_REQUEST['add_info'];
            $this->db->insertTerriAddInfo($info);
        break;

        case self::DO_EDIT_INFO: // add additonal terri info
            //xdebug_break();
            $info = $_REQUEST['add_info'];
            if( isset($_POST['update']) ) {
                $this->db->updateTerriAddInfo($info);
            } elseif( isset($_POST['delete']) ) {
                $spar['ID'] = $info['ID'];
                $this->db->deleteTerriAddInfo($spar);
            } else {
                print "<div class=\"alert alert-warning\">Schiefgegangen.</div>";
            }
        break;

        case self::DO_ADD_STREET: // add street
            //print "<div class=\"alert alert-warning\">SQL um neue Strasse hinzuzufügen.</div>";
            $strasse = $_REQUEST['strasse'];
            if( $this->db->insert("strasse", $strasse) ) {
                //print "<div class=\"alert alert-success\">Strasse (ID $strasse[ID]): '$strasse[street]' aktualisiert</div>";
            } else {
                print "<div class=\"alert alert-danger\">Strasse: '$strasse[street]' konnte nicht hinzugefügt werden. Versuche es bitte noch einmal.</div>";
            }
        break;

        case self::DO_EDIT_STREET: // update or delete street
            if( isset($_POST['update']) ) {
                $strasse = $_REQUEST['strasse'];
                if( $this->db->update("strasse", $strasse) ) {
                    //print "<div class=\"alert alert-success\">Strasse (ID $strasse[ID]): '$strasse[street]' aktualisiert</div>";
                } else {
                    print "<div class=\"alert alert-danger\">Strasse: '$strasse[street]' konnte nicht aktualisiert werden. Versuche es bitte noch einmal.</div>";
                }
            } elseif( isset($_POST['delete']) ) {
                $strasse = $_REQUEST['strasse'];
                $spar['ID'] = $strasse['ID'];
                if( $this->db->delete("strasse", $spar) ) {
                    //print "<div class=\"alert alert-success\">Strasse (ID $strasse[ID]): '$strasse[street]' gelöscht</div>";
                } else {
                    print "<div class=\"alert alert-danger\">Strasse: '$strasse[street]' konnte nicht gelöscht werden. Versuche es bitte noch einmal.</div>";
                }
            } else {
                print "<div class=\"alert alert-warning\">Schiefgegangen.</div>";
            }
        break;

        case self::DO_ADD_TIME: // add new time entry
            $gebiet = $_REQUEST["gebiet"];
            //$h_ID = explode( ',', $_REQUEST['halter_string'])[0]; // this don't work on the webserver
            $harray = explode( ',', $_REQUEST['halter_string']);
            $h_ID = $harray[0];

            if( $h_ID != 0 && $gebiet['ID'] != 0 && isset($gebiet['assigned']) && check_date($gebiet['assigned']) ) {
                $para['ID'] = $gebiet['ID'];
                $para['h_ID'] = $h_ID;
                $para['assigned'] = $gebiet['assigned'];
                $para['back'] = null;
                $para['lost'] = null;
                $para['progress'] = $gebiet['progress'];
                if( check_date($gebiet['back']) ) {
                    $para['back'] = $gebiet['back'];
                }

                if( !$this->db->insert("zeiten_all", $para) ) {
                    print "<div class=\"alert alert-warning\">Schiefgegangen 2.</div>";
                }
            } else {
                print "<div class=\"alert alert-warning\">Schiefgegangen, wähle einen gültigen Halter aus.</div>";
            }

        break;


        case self::DO_EDIT_TIME: // update or remove time entry
            $zpar['ID'] = $_REQUEST['gebiet']['z_ID'];

            if( isset($_POST['update']) ) {
                $gebiet = $_REQUEST['gebiet'];
                // TODO check (later convert) date strings
                $para['z_ID'] = $gebiet['z_ID'];
                $sql = '';
                if( isset($gebiet['assigned']) && check_date($gebiet['assigned']) ) {
                    $para['assigned'] = $gebiet['assigned'];
                    $para['progress'] = $gebiet['progress'];

                    if( (isset($gebiet['back']) && $gebiet['back'] != '') ) {
                        if( check_date($gebiet['back']) ) {
                            // date correct
                            if( isset($gebiet['verloren']) && $gebiet['verloren'] == 'on' ) {
                                $para['lost'] = 1;
                            } else {
                                $para['lost'] = 0;
                            }
                            $para['back'] = $gebiet['back'];
                            $sql = $this->db->update("zurueck", $para);
                        } else {
                            // Fehlermeldung
                            print "<div class=\"alert alert-danger\"> Der Inhalt des Feldes <strong>Zur.</strong> ist fehlerhat, bitte überprüfe den Inhalt. Versuche es bitte noch einmal.</div>";
                        }
                    } else {
                        $para['back'] = null;
                        $para['lost'] = null;
                        $sql = $this->db->update("zurueck", $para);
                    }

                    unset($para);

                    if( $sql ) {
                        //print "<div class=\"alert alert-success\">" .
                        //    " Gebiet wurde eingetragen werden.</div>"
                        //;
                    } else {
                        print "<div class=\"alert alert-danger\">" .
                            " Gebiet konnte nicht eingetragen werden. Versuche es bitte noch einmal.</div>"
                        ;
                    }

                } else {
                    print "<div class=\"alert alert-danger\">" .
                            "Der Inhalt des Feldes <strong>Ausgegeben</strong> ist fehlerhaft, bitte überprüfe den Inhalt." .
                            " Versuche es bitte noch einmal." .
                          "</div>";
                }
                   // print "<div class=\"alert alert-warning\">Zeiten-Eintrag updaten ist schiefgegangen.</div>";
            } elseif( isset($_POST['delete']) ) {
                if( $this->db->delete("zeiten", $zpar) ) {
                //print "<div class=\"alert alert-success\">" .
                //    "Zeiten-Eintrag mit ID: " . $zpar['ID'] . " erfolgreich gelöscht.".
                //    "</div>";
                } else {
                    print "<div class=\"alert alert-danger\">Zeiten-Eintrag löschen ist schiefgegangen.</div>";
                }
            } else {
                print "<div class=\"alert alert-warning\">Schiefgegangen.</div>";
            }
        break;

        case self::DO_LINK_TERRI:
            $myts = explode( ',', $_REQUEST['territory_string'] );
            if( $myts[1] == "NULL" ) { // a bit strange but NULL comes as string
                print "<div class=\"alert alert-danger\">Bitte ein zu verknüpfendes Gebiet wählen</div>";
            } else {
                // try catch in db does failure handling ??
                $this->db->insertLinkTerri($myts[0], $myts[1]);
            }
        break;

        case self::DO_LINK_REMOVE:
            print "<div class=\"alert alert-warning\">SQL Verknüfung zu löschen.</div>";
            $this->db->deleteLinkTerri($_REQUEST['relation']['masterid'], $_REQUEST['relation']['slaveid']);
        break;

        default:
        break;
        }
    }



}


// Test Section


?>

