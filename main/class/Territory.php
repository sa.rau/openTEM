<?php
/***************************************************************************************************

 Author:    Stephan Rau

 Purpose:   contains information about a single territory

***************************************************************************************************/

class Territory {

    private $db = null;      // database connection object

    //-- name mapping for table type
    const ALL           = 0;
    const ASSIGNED      = 1;
    const FREE          = 2;

    //-- name mapping for territory type
    const TYPE          = array(
        'REGULAR' => 0,
        'SEARCH'  => 1,
        'ADDRESS' => 2,
        'ROOT'    => 3
    );

    //-- class variables
    private $id         = null;     // territory id
    private $name       = null;
    private $flats      = null;
    private $blockpart  = null;
    private $type       = null;
    private $place      = null;
    private $mapmark    = null;
    private $ask        = null;
    private $comment    = null;

    // TODO dates not set in constructor ? not sure if it possible, second db access needed
    private $currentOwnerID = null;
    private $currentProgress = null;
    private $currentDoneDate = null;
    private $ownedForMonth  = null;
    private $lastOwnerID    = null;
    private $lastDoneDate   = null;
    private $lastDoneMonth  = null;
    private $freeSinceDate  = null;
    private $freeSinceMonth = null;

    //---- __construct -----------------------------------------------------------------------------
    // Purpose:     connect to database and set private class variables
    // Parameter:   array of parameter values from a sql query
    // Return:      new object is returned
    public function __construct($p_row) {

        $this->db = Database::connect();

        // assign territory information
        $this->id           = $p_row['ID'];
        $this->name         = $p_row['col_name'];
        $this->flats        = $p_row['col_flats'];
        $this->blockpart    = $p_row['col_blockpart'];
        $this->type         = $p_row['col_type'];
        $this->place        = $p_row['col_place'];
        $this->mapmark      = $p_row['col_mapmark'];
        $this->ask          = $p_row['col_ask'];
        $this->comment      = $p_row['col_comment'];

        // TODO second db access needed but not possible in sqlite version ?

    }


    //---- __destruct ------------------------------------------------------------------------------
    // Purpose:     close databse connection
    // Parameter:   none
    // Return:      none
    public function __destruct() {
        //self::$db = null;
    }


    //---- getHtmlTableRowString -------------------------------------------------------------------
    // Purpose:     create html table string
    // Parameter:   selects predefined columns
    // Return:      string
    public function getHtmlTableRowString( $p_tableType ) {
        // TODO try catch block?
        $ret = "<tr>Fehler</tr>";
        $lastFinish = $this->db->getTerritoryStatus($this->id);
        $currentOwner = $this->getOwnerInfo();
        switch( $p_tableType ) {
            case self::ALL      :
                $ret = "<tr>"
                     . "<td>LDF</td>"
                     . "<td>" . $this->getUpdateLink() . "</td>"
                     . "<td>" . $this->flats . "</td>"
                     . "<td>" . $currentOwner['ownerName'] . "</td>"
                     . "<td>" . $currentOwner['ownerGroup'] . "</td>"
                     . "<td>" . $this->getStreetsString() . "</td>"
                     . "</tr>" ;
            break;

            case self::ASSIGNED :
                $ret = "<tr>"
                     . "<td> LDF </td>"
                     . "<td>" . $this->getUpdateLink() . "</td>"
                     . "<td>" . $this->flats . "</td>"
                     . "<td>" . $lastFinish['lastDoneDate'] . "</td>"
                     . "<td>" . $lastFinish['lastDoneMonth'] . "</td>"
                     . "<td>" . $lastFinish['currentProgress'] . " %</td>"
                     . "<td>" . $currentOwner['ownedForMonth'] . "</td>"
                     . "<td>" . $currentOwner['ownerName'] . "</td>"
                     . "<td>" . $currentOwner['ownerGroup'] . "</td>"
                     . "</tr>" ;
            break;

            case self::FREE     :
                $ret = "<tr>"
                     . "<td>LDF</td>"
                     . "<td>" . $this->getUpdateLink() . "</td>"
                     . "<td>" . $this->flats . "</td>"
                     . "<td>" . $this->blockpart . "</td>"
                     . "<td>" . $this->getStreetsString() . "</td>"
                     . "<td>" . $lastFinish['freeSinceDate'] . "</td>"
                     . "<td>" . $lastFinish['freeSinceMonth'] . "</td>"
                     . "<td>" . $this->getLastOwner() . "</td>"
                     . "</tr>" ;
            break;

        }
        return $ret ;
    }


    public function checkRoot() {
        // check if current territory is suitable as root territory
        $ret = false;
        $this->db->getLastTerriOwnerEntry($this->id);
        while ( $row = $this->db->fetch() ) {
            $ret = true;
        }
        return $ret;
    }

    // private getter functions time information

    private function getLastOwner() {
        $ret = "Keiner";
        $this->db->getLastTerriOwnerEntry($this->id);
        while ( $row = $this->db->fetch() ) {
            $back = $row['col_back'];
            $h_id = $row['col_ownerid'];
        }

        if ( isset($back) && isset($h_id) ) {
            $owner = OwnerNurse::getOwner($h_id);
            $ret = $owner->getUpdateLink() ;
        }

        return $ret;
    }

    private function getStreetsString( $p_seperator = "<br>") {
        $this->db->getStreetsByTerri($this->id);
        $ret = "";
        while( $row = $this->db->fetch() ) {
            $ret .= "$row[col_street] $p_seperator" ;
        }
        return $ret;
    }

    private function getLastFinish() {
        // TODO no db access should be needed, done in constructor
        $ret = $this->db->getTerritoryStatus($this->id);
        return "<td>$ret[freeSinceDate]</td>"
             . "<td>$ret[freeSinceMonth]</td>"
             . "<td>$ret[currentProgress] %</td>";
    }

    private function getOwnerInfo() {
        // TODO no db access should be needed, done in constructor
        //xdebug_break();
        $db_ret = $this->db->getCurrentOwnerInfo($this->id);
        if ( $db_ret == null ) {
            $ret['ownedForMonth'] = "";
            $ret['ownerName'] =  "Keiner";
            $ret['ownerGroup'] = "";
        } else {
            $ret['ownedForMonth'] = $db_ret["ownedForMonth"];
            $owner = OwnerNurse::getOwner($db_ret['owner_id']);
            $ret['ownerName'] =  $owner->getUpdateLink();
            $ret['ownerGroup'] = $owner->getGroupName();
        }

        return $ret;
    }

    public function getUpdateLink() {
        return '<a href="index.php?'
            . 'action=' . TerritoryNurse::ACTION
            . '&step=' . TerritoryNurse::EDIT
            . '&terri=' . $this->id
            . '" >'
            . $this->name
            . '</a>';
    }

    public function getType() {
        return $this->type;
    }

    public function getName() {
        return $this->name;
    }

    public function getMapmark() {
        $_mapmark = json_decode($this->mapmark, true);
        return $_mapmark["mark"];
    }

    //----------------------------------------------------------------------------------------------
    // STREET
    //----------------------------------------------------------------------------------------------
    public function printStreetsTableEntries($p_table_count = 0 ) {
        $gpar['ID'] = $this->id;
        $this->db->selectCon("strassen_by_gebiet_ID", $gpar);
        $row_color = array("active", "default"); // alter color with changing table number
        $ret = false; // return false if selection did not contain any entry
        while($row = $this->db->fetch()) {
            print '<tr class="' . $row_color[$p_table_count%2] . '">'
                . "<td>" . $this->getUpdateLink() . "</td>"
                . "<td>" . $row['col_street'] . "</td>"
                . "<td>" . $row['col_note'] . "</td>"
                . "</tr>\n"
            ;
            $ret = true;
        }
        return $ret;
    }

    //----------------------------------------------------------------------------------------------
    public function printEditStreets($p_street_header, $p_note_header) {
        $gpar['ID'] = $this->id;
        $this->db->selectCon("strassen_by_gebiet_ID", $gpar);
        while($row = $this->db->fetch()) { print '
            <form action="index.php?action=' . TerritoryNurse::ACTION
                                  . '&step=' . TerritoryNurse::EDIT
                                    . '&do=' . TerritoryNurse::DO_EDIT_STREET
                    .'" class="form-inline" method="post">
                <div class="form-group" style="width: 43em;"> <div class="input-group">
                      <span class="input-group-addon">' . $p_street_header . '</span>
                      <input type="text" class="form-control" value="'
                            . $row['col_street'] . '" name=strasse[street]>
                </div> </div>
                <div class="form-group" style="width: 25em;"> <div class="input-group">
                      <span class="input-group-addon">' . $p_note_header . '</span>
                      <input type="text" class="form-control" value="'
                            . $row['col_note'] . '" name=strasse[note]>
                </div> </div>
                <button type="submit" class="btn btn-warning" name="update" data-toggle="tooltip" title="Ändern">
                    <span class="glyphicon glyphicon-ok"></span>
                </button>
                <button type="submit" class="btn btn-danger" name="delete" data-toggle="tooltip" title="Löschen">
                    <span class="glyphicon glyphicon-remove"></span>
                </button>
                <input type="hidden" class="text" name="strasse[ID]"  value="' . $row['ID']  .'"/>
                <input type="hidden" class="text" name="gebiet[ID]"   value="' . $this->id .'"/>
            </form>
        '; }
    }

    //----------------------------------------------------------------------------------------------
    public function printAddStreet($p_street_header, $p_note_header) {
        print '
            <form action="index.php?action=' . TerritoryNurse::ACTION
                                  . '&step=' . TerritoryNurse::EDIT
                                    . '&do=' . TerritoryNurse::DO_ADD_STREET
                    .'" class="form-inline" method="post">
                <div class="form-group" style="width: 43em;"> <div class="input-group">
                      <span class="input-group-addon">'. $p_street_header . '</span>
                      <input type="text" class="form-control" placeholder="Strassenname" name=strasse[street]>
                </div> </div>
                <div class="form-group" style="width: 25em;"> <div class="input-group">
                      <span class="input-group-addon">' . $p_note_header . '</span>
                      <input type="text" class="form-control" placeholder="inkl. Läden" name=strasse[note]>
                </div> </div>
                <button type="submit" class="btn btn-success" data-toggle="tooltip" title="Hinzufügen">
                    <span class="glyphicon glyphicon-plus"></span>
                </button>
                <input type="hidden" class="text" name="strasse[territoryid]"   value="' . $this->id .'"/>
                <input type="hidden" class="text" name="gebiet[ID]"             value="' . $this->id .'"/>
            </form>
        ';
    }


    //----------------------------------------------------------------------------------------------
    // ADDITIONAL INFO
    //----------------------------------------------------------------------------------------------
    public function printAddInfoTableEntries($p_table_count = 0 ) {

        //xdebug_break();

        $json_string = file_get_contents("./config/territoriesaddinfo.json");   // TODO move to constructor ?
        $taicn = json_decode($json_string, true); // territory additional info column name

        $row_color = array("active", "default"); // alter color with changing table number
        $ret = false; // return false if selection did not contain any entry

        $this->db->getTerriAddInfo($this->id);
        while($row = $this->db->fetch()){
            $value = json_decode($row["col_infojson"],true);

            print '<tr class="' . $row_color[$p_table_count%2] . '">'
                . "<td>" . $this->getUpdateLink() . "</td>";

            foreach ($taicn as $col_no => $col_setting) {
                print '<td>' . $value[$col_no] . "</td>";
            }

            print "</tr>\n";
            $ret = true;
        }
        return $ret;
    }

    //----------------------------------------------------------------------------------------------
    public function printEditAddInfo() {
        $gpar['ID'] = $this->id;

        // headers (language depended) TODO global
        $json_string = file_get_contents("./config/header.json");
        $header_all = json_decode($json_string, true);
        $header = $header_all["terri_add_info"];

        $json_string = file_get_contents("./config/territoriesaddinfo.json");   // TODO move to constructor ?
        $taicn = json_decode($json_string, true); // territory additional info column name

        // show existing entries
        $this->db->getTerriAddInfo($this->id);
        while($row = $this->db->fetch()){
            $value = json_decode($row["col_infojson"],true);

            print '<form action="index.php?action=' . TerritoryNurse::ACTION
                                         . '&step=' . TerritoryNurse::EDIT
                                           . '&do=' . TerritoryNurse::DO_EDIT_INFO
                        .'" class="form-inline" method="post">';

            foreach ($taicn as $col_no => $col_setting) {
                print '<div class="form-group" style="width:' . $col_setting["width"] . ';"> <div class="input-group">';
                print '<span class="input-group-addon">' . $header[$col_setting["name"]] . '</span>';
                print '<input type="text" class="form-control" value="' . $value[$col_no] . '" name=add_info[value][' . $col_no . ']>';
                print "</div></div>\n";
            }
            print '
                <button type="submit" class="btn btn-warning" name="update" data-toggle="tooltip" title="Ändern">
                    <span class="glyphicon glyphicon-ok"></span>
                </button>
                <button type="submit" class="btn btn-danger" name="delete" data-toggle="tooltip" title="Löschen">
                    <span class="glyphicon glyphicon-remove"></span>
                </button>
            ';
            print
                '<input type="hidden" class="text" name="add_info[ID]"       value="' . $row['ID'] .'"/>'
              . '<input type="hidden" class="text" name="add_info[territoryid]" value="' . $this->id  .'"/>'
              . '<input type="hidden" class="text" name="gebiet[ID]"         value="' . $this->id .'"/>' // needed to open terri page again
            ;
            print "</form>\n";
        }

    }

    //----------------------------------------------------------------------------------------------
    public function printAddAddInfo() {
        // headers (language depended) TODO global
        $json_string = file_get_contents("./config/header.json");
        $header_all = json_decode($json_string, true);
        $header = $header_all["terri_add_info"];

        $json_string = file_get_contents("./config/territoriesaddinfo.json");   // TODO move to constructor ?
        $taicn = json_decode($json_string, true); // territory additional info column name

        print '<form action="index.php?action=' . TerritoryNurse::ACTION
                                     . '&step=' . TerritoryNurse::EDIT
                                       . '&do=' . TerritoryNurse::DO_ADD_INFO
                    .'" class="form-inline" method="post">';

        foreach ($taicn as $col_no => $col_setting) {
            print '<div class="form-group" style="width:' . $col_setting["width"] . ';"> <div class="input-group">';
            print '<span class="input-group-addon">' . $header[$col_setting["name"]] . '</span>';
            print '<input type="text" class="form-control" name=add_info[value][' . $col_no . ']>';
            print "</div></div>\n";
        }
        print'
            <button type="submit" class="btn btn-success" data-toggle="tooltip" title="Hinzufügen">
                <span class="glyphicon glyphicon-plus"></span>
            </button>';
            print
                '<input type="hidden" class="text" name="add_info[territoryid]"  value="' . $this->id .'"/>'
              . '<input type="hidden" class="text" name="gebiet[ID]"             value="' . $this->id .'"/>' // needed to open terri page again
            ;
        print "</form>\n</br>";

    }

    //----------------------------------------------------------------------------------------------
    // MAP
    //----------------------------------------------------------------------------------------------
    public function printEditMap() {
        print "<h3>Karte Bearbeiten</h3>";

        // define printed card dimension in mm
        $card_config_file = "./config/card_regular.json";
        switch ( $this->type ) {
            case 1:
                $card_config_file = "./config/card_search.json";
                break;
        }
        $json_string = file_get_contents($card_config_file);
        $card = json_decode($json_string, true);

        // common congregation information
        $json_string = file_get_contents("./config/congregation.json");
        $congr = json_decode($json_string, true);

        // calculate map dimension in px
        $webpage_width_px = 900;
        $width_px = $webpage_width_px;
        $height_px = ($width_px / $card["map"]["near"]["width"]) * $card["map"]["near"]["height"];
        $style = 'style="width: ' . $width_px . 'px; height: ' . $height_px . 'px;" ' ;

        //print '<div id="map" style="width: 900px; height: 600px; border: 1px solid #ccc"></div>';
        print '<div id="map" ' . $style . '></div>';
        if ( isset($this->mapmark) ) {
            $tem_maps_data = json_decode($this->mapmark,true);
        } else {
            // set default values from config file
            $tem_maps_data = [
                "mark"      => null,
                "center"    => json_encode($congr["center"]),
                "zoom"      => json_encode($congr["zoom"])
            ];
        }
        if( !isset($tem_maps_data["mark"]) ) {
            $tem_maps_data["mark"] = 'null';
        }
        // write init value for tem_maps // TODO change to json
        print "<script>
            var mark    = $tem_maps_data[mark];
            var center  = $tem_maps_data[center];
            var zoom    = $tem_maps_data[zoom];
            var tid     = $this->id;
        </script>";
    }


    //----------------------------------------------------------------------------------------------
    // TERRITORY
    //----------------------------------------------------------------------------------------------
    public function printEdit() {
        $json_string = file_get_contents("./config/header.json");
        $header_all = json_decode($json_string, true);

        print "<h3>Gebiet Details Ändern</h3>";

        if( $this->ask == 1 ) {   $checked = "checked=\"1\"";   } else {   $checked = "";    }
        print '
        <form action="index.php?action=' . TerritoryNurse::ACTION
                    . '&step=' . TerritoryNurse::EDIT
                    . '&do=' . TerritoryNurse::DO_EDIT
                    .'" class="form-inline" method="post">
            <div class="form-group"> <div class="input-group">
                  <span class="input-group-addon">Name</span>
                  <input style="width: 8em;" type="text" class="form-control" value="'
                  . $this->name . '" name=gebiet[name]>
            </div> </div>
            <div class="form-group"> <div class="input-group">
                  <span class="input-group-addon">Wohnungen</span>
                  <input style="width: 5em;" type="text" class="form-control" value="'
                  . $this->flats . '" name=gebiet[flats]>
            </div> </div>
            <div class="form-group"> <div class="input-group">
                  <span class="input-group-addon">Blockgebiet</span>
                  <input style="width: 5em;" type="text" class="form-control" value="'
                  . $this->blockpart . '" name=gebiet[blockpart]>
                  <span class="input-group-addon">%</span>
            </div> </div>

            <div class="form-group"> <div class="input-group">
            <span class="input-group-addon">Typ</span>
            <select style="width: 8em;" class="form-control" name=gebiet[type]>';
            foreach ( self::TYPE as $value) {
                print '<option value="' . $value . '"'
                    . ( $this->type == $value ? " selected " : " " ) . '>'
                    . $header_all["terri_type"][$value]
                    . '</option>';
            }
        print '
            </select>
            </div> </div>

            <div class="form-group" style="width: 22.0em;"> <div class="input-group">
                  <span class="input-group-addon">Ort</span>
                  <input type="text" class="form-control" value="'
                  . $this->place . '" name=gebiet[place]>
            </div> </div>
            <button type="submit" class="btn btn-warning" name="update" data-toggle="tooltip" title="Ändern">
                <span class="glyphicon glyphicon-ok"></span>
            </button>
            <button type="submit" class="btn btn-danger" name="delete" data-toggle="tooltip" title="Löschen">
                <span class="glyphicon glyphicon-remove"></span>
            </button>
            <div class="form-group" style="width: 100%;"> <div class="input-group">
                <span class="input-group-addon">Notiz</span>
                    <input type="text" class="form-control" value="'
                    . $this->comment . '" name=gebiet[comment]>
                    <span class="input-group-addon"><input type="checkbox" '
                    . $checked . ' name=gebiet[ask]> Gebietsdiener Ansprechen</span>
            </div> </div>
            <input type="hidden" class="text" name="gebiet[ID]" value="' . $this->id .'"/>
        </form>
        ';
    }



}
