<?php
/***************************************************************************************************

 Author:    Stephan Rau

 Purpose:   take care of all owner objects

 TODO:
    o add commit question before database update
    o add sanity checks: owner name, date, time

***************************************************************************************************/
require_once( "./class/Owner.php"   );

class OwnerNurse {

    private $db = null;          // database connection object
    private static $inst = null;        // used for singelton implementation

    // create header array TODO move to maybe move to a include file for multilingual
    const ACTION        = 'owner';
    //-- name mapping to step number
    const INTERNALS     = 0;
    const EXTERNALS     = 1;
    const GROUPS        = 2;
    const CAMPAINS      = 3;
    const CONGREGATIONS = 4;
    const ALL           = 5;

    const EDIT          = 6;
    const ADD           = 7;

    //-- name mapping to do number
    const DO_ADD_OWNER     = 1;
    const DO_ASSIGN_FIELD  = 2;
    const DO_FIELD_RECORDS = 3;
    const DO_EDIT_OWNER    = 4;
    //-- step name mapping to languarge
    private static $HEADERS = array(
        self::INTERNALS     => "Intern",
        self::EXTERNALS     => "Extern",
        self::GROUPS        => "Gruppen",
        self::CAMPAINS      => "Aktionen",
        self::CONGREGATIONS => "Versammlung",
        self::ALL           => "Alle",
        self::EDIT          => "Bearbeiten",
        self::ADD           => "Hinzufügen"
    );
    //-- numbers of owners
    private $noOwners = array(
        self::INTERNALS     => "Unbekannt",
        self::EXTERNALS     => "Unbekannt",
        self::GROUPS        => "Unbekannt",
        self::CAMPAINS      => "Unbekannt",
        self::CONGREGATIONS => "Unbekannt",
        self::ALL           => "Unbekannt"
    );

    //-- action name
    private $action = null;
    private $oid    = null; // owner id
    private $owners = array ();
    private $db_type        = null;
    private $db_group       = null;

    //---- __construct -----------------------------------------------------------------------------
    // Propose:     connect to database
    // Parameter:   action name
    // Return:      new object is returned
    private function __construct($action) {
        $this->action = $action;

        $this->db           = Database::connect();
        $this->groupNurse   = GroupNurse::startDialogue();
        $this->typeNurse    = TypeNurse::startDialogue();

        $this->db->getOwners();
        while( $row = $this->db->fetch() ){
            // create array of owner objects
            $this->owners[$row['ID']] = new Owner($row);
        }

    }

    public static function startDialogue() {
        if ( self::$inst == null ) {
            self::$inst = new OwnerNurse(self::ACTION);
        }
        return self::$inst;
    }

    public static function getOwner($owner_id) {
        if ( self::$inst == null ) {
            self::$inst = new OwnerNurse(self::ACTION);
        }
        return self::$inst->owners[$owner_id];
    }


    //---- __destruct ------------------------------------------------------------------------------
    // Propose:     close database connection
    // Parameter:   none
    // Return:      none
    public function __destruct() {
        unset ($this->db);
    }


    //---- getHead ---------------------------------------------------------------------------------
    // Propose:     create a header string which constrains the number of owners
    // Parameter:   none
    // Return:      string
    public function getHead($step) {
        // TODO try catch block?
        $ret = "";
        switch( $step ) {
            case self::INTERNALS     :
            case self::EXTERNALS     :
            case self::GROUPS        :
            case self::CAMPAINS      :
            case self::CONGREGATIONS :
            case self::ALL           :
               $ret = self::$HEADERS[$step] . " (" . $this->noOwners[$step] . ")" ;
            break;
            case self::EDIT          :
            case self::ADD           :
               $ret = self::$HEADERS[$step] ;
            break;
        }
        return $ret ;
    }
    public function printLink($step) {
        print "<li><a href=\"index.php?action=$this->action&step=$step\">" . $this->getHead($step) . "</a></li>";
    }


    //---- printReport -----------------------------------------------------------------------------
    // Purpose:
    // Parameter:   none
    // Return:
    public function printReport() {
        Owner::printTableHeader();
        foreach ( $this->owners as $owner ) {
            $owner->printTableRow();
        }
        Owner::printTableTailer();
    }


    //---- printOwnerDropdown ----------------------------------------------------------------------
    // Propose:
    // Parameter:   oid := owner id
    // Return:
    public function printOwnerDropdown() {
        Owner::printDropdownHeader($this->action, self::EDIT);
        foreach ( $this->owners as $owner ) {
            $owner->printOptionLine($this->oid);
        }
        Owner::printDropdownTailer();
    }


    //---- printAssignField ------------------------------------------------------------------------
    // Purpose:
    // Parameter:
    // Return:
    public function printAssignField() {
        $this->owners[$this->oid]->printAssignField(
                $this->action, self::EDIT, self::DO_ASSIGN_FIELD
        );
    }


    //---- printFieldRecords -----------------------------------------------------------------------
    // Propose:
    // Parameter:   oid := owner id
    // Return:
    public function printFieldRecords() {
        $this->owners[$this->oid]->printFieldRecords(
                $this->action, self::EDIT, self::DO_FIELD_RECORDS
        );
    }


    //---- printEditOwner --------------------------------------------------------------------------
    // Propose:
    // Parameter:   oid := owner id
    // Return:
    public function printEditOwner() {
        $this->owners[$this->oid]->printEdit(
                $this->action, self::EDIT, self::DO_EDIT_OWNER
        );
    }


    //---- printAddOwner ---------------------------------------------------------------------------
    // Propose:
    // Parameter:
    // Return:
    public function printAddOwner() {
        // add new Halter formular
        print "<h2>Halter Hinzufügen</h2>";
        print "<div class=\"alert alert-info\">Bei Versammlungen bitte als Nachname 'Versammlung'" .
                " eintragen, als Vorname den Versammlungsnamen. Bitte <strong>unbedingt</strong>" .
                " den Halter 'Typ' auswählen sonst funktionieren die Skripte zum auswerten der" .
                " Daten nicht mehr. Halter werden mit einer ID abgespeichert nur diese ID ist eindeutig." .
                " D. h. es können nachträglich noch alle Attribute geändert werden." .
                " Allerdings bedeutet es auch das man zwei unterschiedliche IDs mit dem selben Namen anlegen kann." .
                " Wenn das gemacht wird ist Datenkaos die Folge, also macht das bitte nicht." .
                "</div>";
        print '
            <form action="index.php?action=' . $this->action . '&step=' . self::EDIT . '&do=' . self::DO_ADD_OWNER . '" class="form-inline" method="post">
                <div class="form-group"> <div class="input-group">
                      <span class="input-group-addon">Nachname</span>
                      <input type="text" class="form-control" placeholder="Mustermann" name=halter[surname]>
                </div> </div>
                <div class="form-group"> <div class="input-group">
                      <span class="input-group-addon">Vorname</span>
                      <input type="text" class="form-control" placeholder="Max" name=halter[firstname]>
                </div> </div>
                <div class="form-group"> <div class="input-group">
                        <span class="input-group-addon">Typ</span>
                        <select class="form-control" name=halter[ownertypeid]>
                        <option value="-1">Bitte auswählen</option>' ;
                        $this->typeNurse->getDropdownOptionLines(null);
        print           '</select>
                </div> </div>
                <div class="form-group"> <div class="input-group">
                        <span class="input-group-addon">Gruppe</span>
                        <select class="form-control" name=halter[groupid]>
                        <option value="-1">Bitte auswählen</option>' ;
                        $this->groupNurse->getDropdownOptionLines(null);
        print           '</select>
                </div> </div>
                <button type="submit" class="btn btn-success data-toggle="tooltip" title="Hinzufügen">
                    <span class="glyphicon glyphicon-plus"></span>
                </button>
            </form>
        ';

    }


    //---- updateDatabase --------------------------------------------------------------------------
    // Propose:     eval do step and update database
    // Parameter:   ?
    // Return:      owner ID
    public function updateDatabase() {

        // TODO move to db class ????
        function check_date ( $date ) {
            //print "<h1>Date to check '$date'</h1>" ;
            if( preg_match("/\d\d\d\d-\d\d-\d\d/", $date) ) {
                return 1;
            } else {
                return 0;
            }
        }

        // TODO implement own function evalPara includes first do ??? ---
        if( isset($_GET['owner']) ) {
            $h_ID = $_GET['owner'];
        } elseif( isset($_REQUEST['halter_string']) ) {
            //print "<h3>if 1</h3>";
            //$h_ID = explode( ',', $_REQUEST['halter_string'])[0]; // this don't work on the webserver
            $harray = explode( ',', $_REQUEST['halter_string']);
            $h_ID = $harray[0];
        } elseif( isset($_REQUEST['halter']) ) {
            //print "<h3>if 2</h3>"; // nach eintragen
            if( isset($_REQUEST['halter']['ID']) ) {
                $h_ID = $_REQUEST['halter']['ID'];
            } else {
                $h_ID = "-1";
            }
        } elseif( isset($_REQUEST['gebiet']) ) {
            //print "<h3>if 3</h3>";
            $h_ID = $_REQUEST['gebiet']['h_ID'];
        } else {
            //print "<h3>if 4</h3>"; // nach seiten aufruf
              $h_ID = "-1";
        }
        $this->oid = $h_ID;


        $do   = isset($_GET['do'])   ? $_GET['do']   : 0 ;
        // -------------------------------------------------------------

        switch( $do ) {
			case self::DO_ADD_OWNER:
                $halter = $_REQUEST['halter'];
                print "<br/>";
                if( $halter['surname'] == '' || $halter['firstname'] == '' ) {
                   print "<div class=\"alert alert-danger\"><strong>Fehler:</strong> Für den Halter" .
                           " muss ein Vorname und ein Nachname eingegeben werden. Bitte versuche es" .
                           " nochmal.</div>";
                } else {
                    // TODO check if owner name already exists
                    if( $this->db->insert("halter", $halter) ) {
                        print "<div class=\"alert alert-success\">Neuen Halter: '$halter[surname]', '$halter[firstname]' hinzugefügt</div>";
                    } else {
                        print "<div class=\"alert alert-danger\">Neuen Halter: '$halter[surname]', '$halter[firstname]' konnte nicht hinzugefügt werden. Versuche es bitte noch einmal.</div>";
                    }
                }
                // TODO get new halter ID integrate sanity check ?? owner public static add ??
                $this->db->selectCon("halter_ID", $halter);
                while($row = $this->db->fetch()){
                    $h_ID = $row['ID'];
                    $this->oid = $h_ID;
                    $row["nog"] = 0;
                    $this->owners[$row['ID']] = new Owner($row);
                }

            break;

            case self::DO_ASSIGN_FIELD:
                // add new zeiten sql query
                $gebiet = $_REQUEST['gebiet'];
                print "<br/>";
                if( $gebiet['assigned'] == '' ) {
                   print "<div class=\"alert alert-danger\"><strong>Fehler:</strong>" .
                         " Es wurde kein Datum eingegeben. Bitte Versuchs nochmal." .
                         "</div>";
                } else {
                    if( check_date( $gebiet['assigned']) ) {
                        if( $this->db->insert("zeiten", $gebiet) ) {
                            //print "<div class=\"alert alert-success\">Zuteilung erfolgreich.</div>";
                        } else {
                            print "<div class=\"alert alert-danger\">
                                Zuteilung konnte nicht gemacht werden. Versuche es bitte noch einmal.
                            </div>";
                        }
                    } else {
                        print "<div class=\"alert alert-danger\">" .
                                "Der Inhalt des Feldes <strong>Ausgegeben</strong> ist fehlerhaft." .
                                " Versuche es bitte noch einmal." .
                              "</div>";
                    }
                }
            break;

            case self::DO_FIELD_RECORDS:
                if( isset($_REQUEST['gebiet']) ) {
                    $gebiet = $_REQUEST['gebiet'];
                    // TODO check (later convert) date strings
                    $sql = '';
                    if( isset($gebiet['assigned']) && check_date($gebiet['assigned']) ) {

                        if( isset($gebiet['back']) && $gebiet['back'] != '' ) {
                            if( check_date($gebiet['back']) ) {
                                // date correct

								// check if update_and_assign, update or delete
								if( isset($_POST['update']) or isset($_POST['update_and_assign']) ) {
                                    if( isset($gebiet['verloren']) && $gebiet['verloren'] == 'on' ) {
                                        $gebiet['lost'] = 1;
                                    } else {
                                        $gebiet['lost'] = 0;
                                    }

                                    $this->db->updateTimeEntryByID(
                                        $gebiet['z_ID'],
                                        $gebiet['assigned'],
                                        $gebiet['back'],
                                        $gebiet['lost'],
                                        100
                                    );

									// check if update_and_assign
									if( isset($_POST['update_and_assign']) ) {
										unset( $para );
										$para['ID']			= $gebiet['ID'];
										$para['h_ID'] 		= $gebiet['h_ID'];
										$para['assigned']	= $gebiet['back'];
										$this->db->insert("zeiten", $para);
                                        print "<div class=\"alert alert-success\">Gebiet wurde Eingetragen.</div>";
									}
								} elseif ( isset($_POST['delete'] ) ) {
									$zpar['ID'] = $gebiet['z_ID'];
									if( $this->db->delete("zeiten", $zpar) ) {
									} else {
										print "<div class=\"alert alert-danger\">Zeiten-Eintrag löschen ist schiefgegangen.</div>";
									}
								} else {
									print "<div class=\"alert alert-warning\">Schiefgegangen.</div>";
								}

                            } else {
                                // Fehlermeldung
                                print "<div class=\"alert alert-danger\"> Der Inhalt des Feldes <strong>Zurück</strong> ist fehlerhat, bitte überprüfe den Inhalt. Versuche es bitte noch einmal.</div>";
                            }
                        } else {
                            // update ausgegeben date
                            $sql = $this->db->update("ausgegeben", $para);
                        }

                        unset($para);

                    } else {
                        print "<div class=\"alert alert-danger\">" .
                                "Der Inhalt des Feldes <strong>Ausgegeben</strong> ist fehlerhaft, bitte überprüfe den Inhalt." .
                                " Versuche es bitte noch einmal." .
                              "</div>";
                    }
                } else {
                    print "<div class=\"alert alert-danger\">Etwas unerwartetes ist passiert." .
                          " Versuche es bitte noch einmal.</div>";
                }
            break;

            case self::DO_EDIT_OWNER:
                if( isset($_REQUEST['halter']) ) {
                    $this->owners[$this->oid]->update($_REQUEST['halter']);
                } else {
                    print "<div class=\"alert alert-danger\">Etwas unerwartetes ist passiert. Versuche es bitte noch einmal.</div>";
                }
            break;

            default:
            break;
        }
        return $h_ID;
    }


    public function isSelected() {
        return( $this->oid > 0 );
    }

}


// Test Section


?>

