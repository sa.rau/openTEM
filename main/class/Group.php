<?php
/***************************************************************************************************

 Author:    Stephan Rau

 Purpose:   provide group information

***************************************************************************************************/

class Group {

    private $db = null;
    private $db_id = null;
    private $db_name = null;

    public function __construct($p_row) {
        $this->db           = Database::connect();

        // assign owner information
        $this->db_id     = $p_row['ID'];
        $this->db_name   = $p_row['col_name'];
    }

    public function __destruct() {
        $this->db = null;
    }

    public function getName() {
        return $this->db_name;
    }

    public static function printDropdownHeader($p_action, $p_step) {
        print '<h1>Gruppe Bearbeiten</h1>'
            . '<form action="index.php?action=' . $p_action . '&step=' . $p_step . '" '
            . 'class="form-inline" method="post">'
            . '<div class="form-group"> <div class="input-group">'
            . '<select class="form-control" name=group[ID] onchange="this.form.submit()">'
            . '<option value="-1">Bitte Gruppe auswählen</option>'
        ;
    }

    public function printOptionLine($p_id) {
        print '<option value="' . "$this->db_id" . '"'
            . ($this->db_id == $p_id ? " selected " : " " )
            . ">$this->db_name (ID: $this->db_id) </option>";
    }

   public static function printDropdownTailer() {
        print '</select></div></div></form>';
    }

    public function printUpdate($p_action, $p_step) {
        print "<h2>Gruppe Aktualisieren</h2>";
        print '<form action="index.php?action=' . $p_action . '&step=' . $p_step
            . '" class="form-inline" method="post">'
            . '<div class="form-group"> <div class="input-group">'
            . '<span class="input-group-addon">Name</span>'
            . '<input type="text" class="form-control" value="' . $this->db_name
            . '" name=group[name]>'
            . '</div></div>'
            . '<button type="submit" style="width: 8.5em;" '
            . 'class="btn btn-warning">Aktualisieren</button>'
            . '<input type="hidden" class="text" name=group[ID]" value="' . $this->db_id .'"/>'
            . '</form>';
    }

    public function update() {
        $group = $_REQUEST['group'];
        if ( $this->db_id == $group['ID'] ) {
            if( $this->db->update("group", $group) ) {
                print '<div class="alert alert-success">Gruppe (ID: '
                    . $group['ID'] . ') zu "' . $group['name'] . '" aktualisiert</div>';
                // update object information
                $this->db_name = $group['name'];
            } else {
                print '<div class="alert alert-danger">Gruppe konnte nicht aktualisiert werden. '
                    . 'Versuche es bitte noch einmal.</div>';
            }
        } else {
            print '<div class="alert alert-danger">Gruppe konnte nicht aktualisiert werden. '
                . 'Versuche es bitte noch einmal.</div>';
        }
    }

    public function printRemove($p_action, $p_step) {
        print "<h2>Gruppe Löschen</h2>";
        print '<button action="index.php?action=' . $p_action . '&step=' . $p_step
            . 'method="post" type="submit" style="width: 8.5em;" '
            . 'class="btn btn-danger">Löschen</button>'
            . '<input type="hidden" class="text" name=id" value="' . $this->db_id .'"/>';
    }

}
