<?php

//      Cell(float w [, float h [, string txt [, mixed border [, int ln [, string align [, boolean fill [, mixed link]]]]]]])
// MultiCell(float w, float h, string txt [, mixed border [, string align [, boolean fill]]])

class GROUPPDF extends FPDF {
    // Page header
    function Header() {
        $dw = 25;
        $this->mySetFont(14, 'B');
        $str = utf8_decode("GEBIETE pro Gruppe - München-Harlaching");
        $this->Cell($this->lw-$dw,8,$str,0,0,'L');

        $today = getdate();
        $str = "$today[mday].$today[mon].$today[year]";
        $this->Cell($dw,8,$str,1,1,'R');

        $this->Ln($this->header_space);
    }

    // Page footer
    function Footer() {
        // Position at 2.0 cm from bottom
        $this->SetY(-20);
        $this->mySetFont(10);
        $this->Cell($this->lw,4,"* #W. = Anzahl der Wohnungen im Gebiet",0,0,'L');
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        $this->mySetFont(6);
        $this->Cell(40,4,"AGpGr        5/21",0,0,'L');
        $today = getdate();
        $this->Cell(90,4,"$today[mday].$today[mon].$today[year]",0,0,'C');
        $this->Cell(40,4,'Seite '.$this->PageNo().' von {nb}',0,0,'R');
    }

    // own cell function
    function MyMultiCell($w, $h, $txt, $border=0, $ln=0,  $align='J', $fill=false) {
	    if($this->y+$h > $this->PageBreakTrigger) {
            $this->y = $this->tMargin + 10;
            $this->AddPage();
        }
        $y = $this->y;

    	// Output text with automatic or explicit line breaks
    	if($w==0)
    		$w = $this->w-$this->rMargin-$this->x;

    	$s = str_replace("\r",'',$txt); // remove \r
    	$nb = strlen($s);               // get string length
    	if($nb>0 && $s[$nb-1]=="\n")    // check if last char in string is a '\n'
    		$nb--;                      // if so remove it from number of bytes

        // check border settings
    	$b = 0;
    	if($border) {
    		if($border==1) {
    			$border = 'LTRB';
    			$b = 'LRT';
    			$b2 = 'LR';
    		} else {
    			$b2 = '';
    			if(strpos($border,'L')!==false)
    				$b2 .= 'L';
    			if(strpos($border,'R')!==false)
    				$b2 .= 'R';
    			$b = (strpos($border,'T')!==false) ? $b2.'T' : $b2;
    		}
    	}

        // analyse string
    	$i = 0;
    	$j = 0;
    	$nl = 1;
        // go through each character
    	while($i<$nb) {

    		// Get next character
    		$c = $s[$i];

    		if($c=="\n") {
    			// Explicit line break
    			if($this->ws>0)	{
    				$this->ws = 0;
    				$this->_out('0 Tw');
    			}

    			$this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
    			$i++;
    			$j = $i;
    			$nl++;
    			if($border && $nl==2)
    				$b = $b2;
    			continue;
    		}

    		$i++;
    	}

    	// Last chunk
    	if($this->ws>0) {
    		$this->ws = 0;
    		$this->_out('0 Tw');
    	}

    	if($border && strpos($border,'B')!==false)
    		$b .= 'B';
    	$this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);

        // check for line break after cell
        if($ln == 0) {
    		$this->x += $w;
            $this->y = $y;
        } elseif($ln == 1) {
    		$this->x = $this->lMargin;
        }
    }

    function __construct( $top=30 ) {
        parent::__construct();

        $this->date_today = new DateTime(date('Y-m-d'));

        $this->SetLeftMargin(25);
        $this->SetRightMargin(16);
        $this->AliasNbPages();
        $this->SetAutoPageBreak(1, 15);

        $this->top = $top;
        $this->header_space = 4.5;
        $this->lh = 9;
        $this->thickB = 0.5;
        $this->thinB  = 0.2;
        $this->fontSize = 12;

        //$this->hh = 16;
        $this->lh = 8;
        $this->lw = 170;
        $this->nw = 10;
        $this->ow = 40;
        $this->tnw = 15;
        $this->tfw = 15;

        $this->mw = 15;
        $this->dw = 20;
        $this->tw = 15;

        $this->cw = $this->lw -
            ( $this->nw + $this->ow + $this->tnw + $this->tfw + $this->mw + $this->mw+$this->dw );

        $this->mySetFont();
    }

    function mySetFont(  $size=10, $style='') {
        $this->SetFont('Arial',$style,$size);
        $this->fontSize = $size;
    }


    function headerLine( $groupName ) {

        $this->AddPage();

        $this->SetFillColor(200,200,200);
        $this->SetLineWidth($this->thickB);

        $this->mySetFont(14, 'B');
        $this->Cell($this->lw,$this->lh, $groupName ,1, 1, 'C', 1);

        $this->mySetFont(12, 'B');
        $this->Cell($this->nw,$this->lh, ' ' ,'LTR', 0, 'C', 1);
        $this->Cell($this->ow,$this->lh, 'Halter' ,'1',0,'C',1);
        $this->Cell($this->mw,$this->lh, 'Besitz' ,'1',0,'C',1);
        $this->Cell($this->tnw+$this->tfw,$this->lh, 'Gebiet' ,'1',0,'C',1);
        $this->Cell($this->mw+$this->dw,$this->lh, 'Bearbeitet am' ,'1',0,'C',1);
        $this->Cell($this->cw,$this->lh, 'Bemerkungen' ,'LTR',1,'C',1);

        $this->mySetFont(10, 'B');
        $this->Cell($this->nw,$this->lh,  '#'      ,'LB'  ,0 ,'C' ,1);
        $this->Cell($this->ow,$this->lh,  'Name'   ,'LBR'  ,0 ,'C' ,1);
        $this->Cell($this->mw,$this->lh,  'Monate' ,'LBR'  ,0 ,'C' ,1);
        $this->Cell($this->tnw,$this->lh, 'Name'   ,'LBR'  ,0 ,'C' ,1);
        $this->Cell($this->tfw,$this->lh, '#W.*'   ,'BR'  ,0 ,'C' ,1);
        $this->Cell($this->mw,$this->lh,  'Monate' ,'LBR'  ,0 ,'C' ,1);
        $this->Cell($this->dw,$this->lh,  'Datum'  ,'BR'  ,0 ,'C' ,1);
        $this->Cell($this->cw,$this->lh,  ''       ,'LBR'  ,1 ,'C' ,1);

    }

    function entryLine( $no, $ownerName, $terriName, $terriFlats, $ownedForMonth, $terriDoneDate, $terriDoneMonths ) {

        $ownerName2 = $this->shortName($ownerName, 140);

        $this->mySetFont(10);
        $this->Cell($this->nw,$this->lh, $no ,'1',0,'C');
        if ( $ownedForMonth >= 24 && $terriDoneMonths >= 24 ) {
            $this->SetTextColor(139,0,139); // DarkMagenta www.farb-tabelle.de
        } elseif ( $ownedForMonth >= 12 && $terriDoneMonths >= 12 ) {
            $this->SetTextColor(205,0,0); // red3 www.farb-tabelle.de
        } elseif ( $ownedForMonth >= 6 && $terriDoneMonths >= 6 ) {
            $this->SetTextColor(255,69,0); // OrangeRed www.farb-tabelle.de
        } else {
            $this->SetTextColor(0,0,0);
        }
        $this->Cell($this->ow,$this->lh, $ownerName2 ,'1',0,'L');
        if ( $ownedForMonth >= 24 ) {
            $this->SetTextColor(139,0,139); // DarkMagenta www.farb-tabelle.de
        } elseif ( $ownedForMonth >= 12 ) {
            $this->SetTextColor(205,0,0); // red3 www.farb-tabelle.de
        } elseif ( $ownedForMonth >= 6 ) {
            $this->SetTextColor(255,69,0); // OrangeRed www.farb-tabelle.de
        } else {
            $this->SetTextColor(0,0,0);
        }
        $this->Cell($this->mw,$this->lh, $ownedForMonth ,'1',0,'R');
        $this->SetTextColor(0,0,0);
        $this->mySetFont(10, 'B');
        $this->Cell($this->tnw,$this->lh, $terriName ,'LB',0,'C');
        $this->mySetFont(10);
        $this->Cell($this->tfw,$this->lh, $terriFlats ,'LB',0,'R');
        if ( $terriDoneMonths >= 24 ) {
            $this->SetTextColor(139,0,139); // DarkMagenta www.farb-tabelle.de
        } elseif ( $terriDoneMonths >= 12 ) {
            $this->SetTextColor(205,0,0); // red3 www.farb-tabelle.de
        } elseif ( $terriDoneMonths >= 6 ) {
            $this->SetTextColor(255,69,0); // OrangeRed www.farb-tabelle.de
        } else {
            $this->SetTextColor(0,0,0);
        }
        $this->Cell($this->mw,$this->lh, $terriDoneMonths ,'LBR',0,'R');
        $this->Cell($this->dw,$this->lh, $terriDoneDate ,'BR',0,'R');
        $this->SetTextColor(0,0,0);
        $this->Cell($this->cw,$this->lh, '','1',1,'R');

    }

    function getTextWidthPx( $txt ) {
        $bbox = imagettfbbox($this->fontSize,0,'./design/fonts/arial.ttf',$txt);
        $xmax = max($bbox[0], $bbox[2], $bbox[4], $bbox[6]);
        $xmin = min($bbox[0], $bbox[2], $bbox[4], $bbox[6]);
        return( $xmax - $xmin );
    }

    function shortName( $name, $max = 240 ) {
        // loop until needed width is reached
        while( $this->getTextWidthPx($name) > $max ) {
            // split given name at ', '
            preg_match( "/^(.*), (.*)$/", $name, $matches);

            if( preg_match( "/Versammlung/", $name) ) {
                $name = str_replace( "Versammlung", "VS", $name);
            } elseif( $this->getTextWidthPx($matches[1]) > $max/2 ) {
                // if sur name is wider than half cell short it first
                $name = substr($matches[1], 0, -2) . "., $matches[2]";
            } else {
                $name = substr($name, 0, -2) . ".";
            }
        }
        return( utf8_decode($name) );

    }

    function thinLine( $x1, $y1, $x2, $y2 ) {
        $this->SetLineWidth($this->thinB);
        $this->Line($x1,$y1,$x2,$y2);
        $this->SetLineWidth($this->thickB);
    }

}

?>
