<?php

//require('fpdf.php');

//      Cell(float w [, float h [, string txt [, mixed border [, int ln [, string align [, boolean fill [, mixed link]]]]]]])
// MultiCell(float w, float h, string txt [, mixed border [, string align [, boolean fill]]])

class S13PDF extends FPDF {
    // Page header
    function Header() {
        $this->mySetFont(12, 'B');
        $this->Cell(0,8,'GEBIETSZUTEILUNGSKARTE',0,0,'L');
        $this->mySetFont(10, 'I');
        $this->Cell(0,8, utf8_decode($this->congr["name"]) ,0,1,'R');
        $this->Ln($this->header_space);
    }

    // Page footer
    function Footer() {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        $this->mySetFont(6);
        $this->Cell(40,4,"S-13-X-mod-$this->tabs       6/18",0,0,'L');
        $today = getdate();
        $this->Cell(90,4,"$today[mday].$today[mon].$today[year]",0,0,'C');
        $this->Cell(40,4,'Seite '.$this->PageNo().' von {nb}',0,0,'R');
    }

    // covers sheet lines
    function coverLine($p_name, $p_value) {
        $this->mySetFont(12, 'B');
        $name = utf8_decode($p_name);
        $this->Cell(90,8,$name,1,0,'L');
        $this->x = $this->lMargin + 90;
        $this->mySetFont(12, '');
        $value = utf8_decode($p_value);
        $this->Cell(0,8,$value,1,1,'R');
    }

    // covers sheet lines
    function coverTableLine($p_name, $p_value_1, $p_value_2, $p_value_3) {
        $this->mySetFont(12, 'B');

        $this->SetTextColor(0,0,0);
        $this->x = $this->lMargin;
        $name = utf8_decode($p_name);
        $this->Cell(90,8,$name,1,0,'L');

        $this->mySetFont(12, '');

        $this->SetTextColor(139,0,139); // DarkMagenta www.farb-tabelle.de
        $this->x = $this->lMargin + 90;
        $value_1 = utf8_decode($p_value_1);
        $this->Cell(27,8,$value_1,1,0,'R');

        $this->SetTextColor(205,0,0); // red3 www.farb-tabelle.de
        $this->x = $this->lMargin + 117;
        $value_2 = utf8_decode($p_value_2);
        $this->Cell(27,8,$value_2,1,0,'R');

        $this->SetTextColor(255,69,0); // OrangeRed www.farb-tabelle.de
        $this->x = $this->lMargin + 144;
        $value_3 = utf8_decode($p_value_3);
        $this->Cell(0,8,$value_3,1,1,'R');
    }

    // own cell function
    function MyMultiCell($w, $h, $txt, $border=0, $ln=0,  $align='J', $fill=false) {
	    if($this->y+$h > $this->PageBreakTrigger) {
            $this->y = $this->tMargin + 10;
            $this->AddPage();
        }
        $y = $this->y;

    	// Output text with automatic or explicit line breaks
    	if($w==0)
    		$w = $this->w-$this->rMargin-$this->x;

    	$s = str_replace("\r",'',$txt); // remove \r
    	$nb = strlen($s);               // get string length
    	if($nb>0 && $s[$nb-1]=="\n")    // check if last char in string is a '\n'
    		$nb--;                      // if so remove it from number of bytes

        // check border settings
    	$b = 0;
    	if($border) {
    		if($border==1) {
    			$border = 'LTRB';
    			$b = 'LRT';
    			$b2 = 'LR';
    		} else {
    			$b2 = '';
    			if(strpos($border,'L')!==false)
    				$b2 .= 'L';
    			if(strpos($border,'R')!==false)
    				$b2 .= 'R';
    			$b = (strpos($border,'T')!==false) ? $b2.'T' : $b2;
    		}
    	}

        // analyse string
    	$i = 0;
    	$j = 0;
    	$nl = 1;
        // go through each character
    	while($i<$nb) {

    		// Get next character
    		$c = $s[$i];

    		if($c=="\n") {
    			// Explicit line break
    			if($this->ws>0)	{
    				$this->ws = 0;
    				$this->_out('0 Tw');
    			}

    			$this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
    			$i++;
    			$j = $i;
    			$nl++;
    			if($border && $nl==2)
    				$b = $b2;
    			continue;
    		}

    		$i++;
    	}

    	// Last chunk
    	if($this->ws>0) {
    		$this->ws = 0;
    		$this->_out('0 Tw');
    	}

    	if($border && strpos($border,'B')!==false)
    		$b .= 'B';
    	$this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);

        // check for line break after cell
        if($ln == 0) {
    		$this->x += $w;
            $this->y = $y;
        } elseif($ln == 1) {
    		$this->x = $this->lMargin;
        }
    }

    function __construct( $p_congr, $p_nog, $p_now, $p_nouct, $p_aug, $p_frei_seit, $p_ausg_seit, $tabs=1 ) {
        parent::__construct();
        $this->SetLeftMargin(25);
        $this->SetRightMargin(16);
        $this->AliasNbPages();
        $this->SetAutoPageBreak(1, 15);
        $this->congr = $p_congr; // congregation name
        $this->lh = 9;
        $this->cur_col = 0;
        $this->cur_row = 0;
        $this->tabs = $tabs;
        switch( $tabs ) {
            case 3:
                $this->num_of_rows = 8;
                $this->header_space = 4.5;
                $this->tab_space = 6;
                break;
            case 2:
                $this->num_of_rows = 13;
                $this->header_space = 4.5;
                $this->tab_space = 4.5;
                break;
            case 1 :
            default :
                $this->num_of_rows = 28;
                $this->header_space = 2;
                $this->tab_space = 1;
                break;
        }
        //$this->num_of_rows = $rows;
        $this->num_of_cols = 5;
        $this->cur_col_y = 0;
        $this->cur_col_x = 0;
        $this->col_width = 34;
        $this->col_lmargin = 10;
        $this->mySetFont();
        $this->thickB = 0.5;
        $this->thinB  = 0.2;
        $this->fontSize = 12;

        $this->AddPage();
        // create cover sheet
        $this->mySetFont(24, 'B');
        $str = utf8_decode('Deckblatt');
        $this->Cell(0,12,$str,1,1,'C');
        $this->Ln(15);
        $today = getdate();
        $this->coverLine('Datum der Zusammenstellung', "$today[mday].$today[mon].$today[year]");
        $this->Ln(8);
        $flatDefSize = 200;
        $flatsUncounted = $p_nouct * $flatDefSize;
        $average = round( ($p_now + $flatsUncounted) / $p_nog, 2);
        $this->coverLine('Durchschnittliche Größe',
            "($p_now + $flatsUncounted) : $p_nog = $average" );
        $this->coverLine('    Ungezählte Gebiete * Standardgröße',
            "$p_nouct * $flatDefSize = $flatsUncounted" );
        $this->Ln(8);
        $this->coverLine('Gesamtzahl', $p_nog);
        $this->coverLine('    Ausgegebenen', $p_aug);
        $this->coverLine('    Frei', $p_nog - $p_aug);
        $this->Ln(8);
        $this->coverTableLine("Länger als ... Monate nicht bearbeitet",  24, 12, 6);
        $sum[24] = $p_ausg_seit[0] + $p_ausg_seit[24] + $p_frei_seit[0] + $p_frei_seit[24];
        $add[12] = $p_ausg_seit[12] + $p_frei_seit[12];
        $sum[12] = $sum[24] + $add[12];
        $add[6] = $p_ausg_seit[6] + $p_frei_seit[6];
        $sum[6] = $sum[12] + $add[6];
        $this->coverTableLine("Gesamtzahl",
                "$sum[24]",
                "+ $add[12] = $sum[12]",
                "+ $add[6] = $sum[6]");
        $sum[24] = $p_ausg_seit[0] + $p_ausg_seit[24];
        $sum[12] = $sum[24] + $p_ausg_seit[12];
        $sum[6] = $sum[12] + $p_ausg_seit[6];
        $this->coverTableLine("    Ausgegeben",
                "$p_ausg_seit[0]* + $p_ausg_seit[24] = $sum[24]",
                "+ $p_ausg_seit[12] = $sum[12]",
                "+ $p_ausg_seit[6] = $sum[6]");
        $sum[24] = $p_frei_seit[0] + $p_frei_seit[24];
        $sum[12] = $sum[24] + $p_frei_seit[12];
        $sum[6] = $sum[12] + $p_frei_seit[6];
        $this->coverTableLine("    Frei",
                "$p_frei_seit[0]* + $p_frei_seit[24] = $sum[24]",
                "+ $p_frei_seit[12] = $sum[12]",
                "+ $p_frei_seit[6] = $sum[6]");
        $this->mySetFont(6, 'I');
        $str = utf8_decode('* neue Gebiete die noch nicht bearbeitet wurden');
        $this->SetTextColor(0,0,0); // black
        $this->Cell(0,6,$str,0,1,'L');

        $this->AddPage();
    }

    function getNumOfRows() {
        return $this->num_of_rows;
    }

    function mySetFont(  $size=12, $style='') {
        // available styles: B ^= Bold, I ^= Italic, U ^= Underline
        $this->SetFont('Arial',$style,$size);
        $this->fontSize = $size;
    }


    function headerCell( $str ) {

        $this->SetLineWidth($this->thickB);

        // new header => must be row 0
        $this->cur_row = 0;

        // new column => set x pos and col number
        if( $this->cur_col >= $this->num_of_cols ) {
            $this->cur_col = 0;
            $this->foo = "\n";
    		$this->x = $this->lMargin;
        }

        // first col => save y pos else set y pos
        if( $this->cur_col == 0 ) {
            // check for needed page break
	        if($this->y+$this->lh > $this->PageBreakTrigger) {
                $this->y = $this->tMargin + 10;
                $this->AddPage();
            }
            $this->cur_col_y = $this->y;
        } else {
            $this->y = $this->cur_col_y;
        }

        // calculate x pos in dependence of col number and cell width
        $this->cur_col_x = $this->lMargin + $this->cur_col * $this->col_width;
        $this->x = $this->cur_col_x;

        // create header cell
        $this->mySetFont(8, 'B');
        $this->MyMultiCell($this->col_lmargin,$this->lh/2, "Geb.-\nNr.",'LTB',0,'L');
        $this->mySetFont(12, 'B');
        $this->Cell($this->col_width-$this->col_lmargin,$this->lh, $str ,'RTB',1,'C');

    }

    function entryCell( $name, $start, $end ) {


        $this->SetLineWidth($this->thickB);

        // set x pos
        $this->x = $this->cur_col_x;

        // cout rows
        $this->cur_row++;

        // increment col when last row is reached
        if( $this->cur_row >= $this->num_of_rows ) {
            $this->cur_col++;
        }

        // create entry cell
        $this->mySetFont(8);
        $lh = $this->lh/2;
        $y = $this->y;
        $x = $this->x;
        /*
        $this->Cell($this->col_lmargin,$lh, '' ,'LT',0,'L');
        $xtb = $this->x;
        $ytb = $this->y;
        $this->Cell($this->col_width-$this->col_lmargin,$lh, $this->shortName($name) ,'TR',1,'L');
        $this->thinLine($xtb,$ytb,$xtb,$this->y);
        */
        $this->Cell($this->col_width,$lh, $this->shortName($name) ,'LTR',1,'L');
        $this->thinLine($x,$this->y,$x+$this->col_width,$this->y);
        $this->x = $x;
        $this->Cell($this->col_width/2,$lh, $start ,'LB',0,'C');
        $xtb = $this->x;
        $ytb = $this->y;
        $this->Cell($this->col_width/2,$lh, $end   ,'BR',1,'C');
        $this->thinLine($xtb,$ytb,$xtb,$this->y);

        // add line break if last row is reached
        if( $this->cur_col >= $this->num_of_cols ) {
            $this->Ln($this->tab_space);
        }

    }

    function getTextWidthPx( $txt ) {
        $bbox = imagettfbbox($this->fontSize,0,'./design/fonts/arial.ttf',$txt);
        $xmax = max($bbox[0], $bbox[2], $bbox[4], $bbox[6]);
        $xmin = min($bbox[0], $bbox[2], $bbox[4], $bbox[6]);
        return( $xmax - $xmin );
    }

    function shortName( $name ) {
        // loop until needed width is reached
        $px_max = 120;
        while( $this->getTextWidthPx($name) > $px_max ) {
            // split given name at ', '
            preg_match( "/^(.*), (.*)$/", $name, $matches);

            if( preg_match( "/Versammlung/", $name) ) {
                $name = str_replace( "Versammlung", "VS", $name);
            } elseif( $this->getTextWidthPx($matches[1]) > $px_max/2 ) {
                // if sur name is wider than half cell short it first
                $name = substr($matches[1], 0, -2) . "., $matches[2]";
            } else {
                $name = substr($name, 0, -2) . ".";
            }
        }
        return( utf8_decode($name) );

    }

    function thinLine( $x1, $y1, $x2, $y2 ) {
        $this->SetLineWidth($this->thinB);
        $this->Line($x1,$y1,$x2,$y2);
        $this->SetLineWidth($this->thickB);
    }

}


// Instanciation of inherited class
/* Test code
$tabs_per_sheet = 1;
$pdf = new S13PDF( $tabs_per_sheet );

for( $g=1; $g<42; $g++ ) {

    // 1. Header
    $pdf->headerCell( "Gebiet $g/" );

    // 2. - 29. Zuteilung Historie (max = getNumOfRows - 2 Einträge)
    for($i=1;$i<=$pdf->getNumOfRows();$i++) {
        $pdf->entryCell( "Gebüt $g/$i", "genommen", "zurück" );
    }

}

$pdf->Output();
*/

?>
