<?php

//require('fpdf.php');

//      Cell(float w [, float h [, string txt [, mixed border [, int ln [, string align [, boolean fill [, mixed link]]]]]]])
// MultiCell(float w, float h, string txt [, mixed border [, string align [, boolean fill]]])

class WORKPDF extends FPDF {
    // Page header
    function Header() {
        $dw = 25;
        $this->mySetFont(14, 'B');
        $str = utf8_decode("AUSGEGEBENE GEBIETE TOP 30 - München-Harlaching");
        $this->Cell($this->lw-$dw,8,$str,0,0,'L');

        $today = getdate();
        $str = "$today[mday].$today[mon].$today[year]";
        $this->Cell($dw,8,$str,1,1,'R');

        $this->Ln($this->header_space);
    }

    // Page footer
    function Footer() {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        $this->mySetFont(6);
        $this->Cell(40,4,"AGT30        2/14",0,0,'L');
        $today = getdate();
        $this->Cell(90,4,"$today[mday].$today[mon].$today[year]",0,0,'C');
        $this->Cell(40,4,'Seite '.$this->PageNo().' von {nb}',0,0,'R');
    }

    // own cell function
    function MyMultiCell($w, $h, $txt, $border=0, $ln=0,  $align='J', $fill=false) {
	    if($this->y+$h > $this->PageBreakTrigger) {
            $this->y = $this->tMargin + 10;
            $this->AddPage();
        } 
        $y = $this->y;

    	// Output text with automatic or explicit line breaks
    	if($w==0)
    		$w = $this->w-$this->rMargin-$this->x;

    	$s = str_replace("\r",'',$txt); // remove \r
    	$nb = strlen($s);               // get string length
    	if($nb>0 && $s[$nb-1]=="\n")    // check if last char in string is a '\n'
    		$nb--;                      // if so remove it from number of bytes

        // check border settings
    	$b = 0;
    	if($border) {
    		if($border==1) {
    			$border = 'LTRB';
    			$b = 'LRT';
    			$b2 = 'LR';
    		} else {
    			$b2 = '';
    			if(strpos($border,'L')!==false)
    				$b2 .= 'L';
    			if(strpos($border,'R')!==false)
    				$b2 .= 'R';
    			$b = (strpos($border,'T')!==false) ? $b2.'T' : $b2;
    		}
    	}

        // analyse string
    	$i = 0;
    	$j = 0;
    	$nl = 1;
        // go through each character
    	while($i<$nb) {

    		// Get next character
    		$c = $s[$i];

    		if($c=="\n") {
    			// Explicit line break
    			if($this->ws>0)	{
    				$this->ws = 0;
    				$this->_out('0 Tw');
    			}

    			$this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
    			$i++;
    			$j = $i;
    			$nl++;
    			if($border && $nl==2)
    				$b = $b2;
    			continue;
    		}

    		$i++;
    	}

    	// Last chunk
    	if($this->ws>0) {
    		$this->ws = 0;
    		$this->_out('0 Tw');
    	}

    	if($border && strpos($border,'B')!==false)
    		$b .= 'B';
    	$this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);

        // check for line break after cell
        if($ln == 0) {
    		$this->x += $w;
            $this->y = $y;
        } elseif($ln == 1) {
    		$this->x = $this->lMargin;
        }
    }

    function WORKPDF( $top=30 ) {
        FPDF::FPDF();
        $this->SetLeftMargin(25);
        $this->SetRightMargin(16);
        $this->AliasNbPages();
        $this->SetAutoPageBreak(1, 15);

        $this->top = $top;
        $this->header_space = 4.5;
        $this->lh = 9;
        $this->thickB = 0.5;
        $this->thinB  = 0.2; 
        $this->fontSize = 12;

        $this->hh = 16;
        $this->lh = 8;
        $this->lw = 170;
        $this->pw = 15;
        $this->gw = 30;
        $this->sw = 40;
        $this->sgw = 15;
        $this->hw = $this->lw - $this->pw - $this->gw - $this->sw;
        $this->hgw = 15;

        $this->mySetFont();
        $this->AddPage();
    }
    
    function mySetFont(  $size=10, $style='') {
        $this->SetFont('Arial',$style,$size);
        $this->fontSize = $size;
    }


    function headerLine( ) {

        $this->SetFillColor(200,200,200);
        $this->SetLineWidth($this->thickB);

        $this->mySetFont(12, 'B');
        $this->Cell(15,$this->hh, 'Platz' ,1,0,'C',1);

        $y = $this->y;
        $x = $this->x;
        $this->Cell($this->gw,$this->hh/2, 'Gebiet' ,'TR',1,'C',1);
        $this->thinLine($x, $this->y, $x+$this->gw, $this->y );
        $this->SetX($x);
        $this->mySetFont(10, 'B');
        $this->Cell($this->gw/2,$this->hh/2, 'Name' ,'B',0,'C',1);
        $x = $this->GetX();
        $this->Cell($this->gw/2,$this->hh/2, '#W.' ,'BR',0,'C',1);
        $this->thinLine($x, $this->y, $x, $this->y+$this->hh/2 );

        $this->y = $y;
        $x = $this->x;
        $this->mySetFont(12, 'B');
        $this->Cell($this->sw,$this->hh/2, 'Ausg. seit' ,'TR',1,'C',1);
        $this->thinLine($x, $this->y, $x+$this->sw, $this->y );
        $this->x = $x;
        $this->mySetFont(10, 'B');
        $this->Cell($this->sw-$this->sgw,$this->hh/2, 'Datum' ,'B',0,'C',1);
        $x = $this->x;
        $this->Cell($this->sgw,$this->hh/2, 'Monate' ,'BR',0,'C',1);
        $this->thinLine($x, $this->y, $x, $this->y+$this->hh/2 );
        
        $this->y = $y;
        $x = $this->x;
        $this->mySetFont(12, 'B');
        $this->Cell($this->hw,$this->hh/2, 'Halter' ,'TR',1,'C',1);
        $this->thinLine($x, $this->y, $x+$this->hw, $this->y );
        $this->x = $x;
        $this->mySetFont(10, 'B');
        $this->Cell($this->hw-$this->hgw,$this->hh/2, 'Name' ,'B',0,'C',1);
        $x = $this->x;
        $this->Cell($this->hgw,$this->hh/2, 'Gebiete' ,'BR',1,'C',1);
        $this->thinLine($x, $this->y, $x, $this->y-$this->hh/2 );

    }

    function entryLine( $platz, $g_name, $g_typ, $a_date, $a_month, $h_name, $h_nog ) {

        $this->mySetFont(12, 'B');
        $this->SetLineWidth($this->thickB);
        $this->Cell($this->pw,$this->lh, $platz ,1,0,'C',1);

        $this->mySetFont(10);
        $this->Cell($this->gw/2,$this->lh, $g_name ,'B',0,'C');
        $x = $this->x;
        $this->Cell($this->gw/2,$this->lh, "$g_typ " ,'BR',0,'R');
        $this->thinLine($x, $this->y, $x, $this->y+$this->lh );

        $this->Cell($this->sw-$this->sgw,$this->lh, $a_date ,'B',0,'C');
        $x = $this->x;
        $this->Cell($this->sgw,$this->lh, "$a_month " ,'BR',0,'R');
        $this->thinLine($x, $this->y, $x, $this->y+$this->lh );
        
        $this->Cell($this->hw-$this->hgw,$this->lh, " " . $this->shortName($h_name) ,'B',0,'L');
        $x = $this->x;
        $this->Cell($this->hgw,$this->lh, $h_nog ,'BR',1,'C');
        $this->thinLine($x, $this->y, $x, $this->y-$this->lh );

    }

    function getTextWidthPx( $txt ) {
        $bbox = imagettfbbox($this->fontSize,0,'./design/fonts/arial.ttf',$txt);
        //$bbox = imagettfbbox($this->fontSize,0,'../../design/fonts/arial.ttf',$txt);
        $xmax = max($bbox[0], $bbox[2], $bbox[4], $bbox[6]);
        $xmin = min($bbox[0], $bbox[2], $bbox[4], $bbox[6]);
        return( $xmax - $xmin );
    }

    function shortName( $name ) {
        // loop until needed width is reached 
        $max = 240;
        while( $this->getTextWidthPx($name) > $max ) {
            // split given name at ', '
            preg_match( "/^(.*), (.*)$/", $name, $matches);

            if( preg_match( "/Versammlung/", $name) ) {
                $name = str_replace( "Versammlung", "VS", $name);
            } elseif( $this->getTextWidthPx($matches[1]) > $max/2 ) {
                // if sur name is wider than half cell short it first
                $name = substr($matches[1], 0, -2) . "., $matches[2]";
            } else {
                $name = substr($name, 0, -2) . ".";
            }
        }
        return( utf8_decode($name) );

    }

    function thinLine( $x1, $y1, $x2, $y2 ) {
        $this->SetLineWidth($this->thinB);
        $this->Line($x1,$y1,$x2,$y2); 
        $this->SetLineWidth($this->thickB);
    }
    
}


// Instanciation of inherited class
// Test code 
//$pdf = new WORKPDF( );
//$pdf->headerLine( );
//for( $i=0 ; $i < 30 ; $i++ ) {
//    $pdf->entryLine( $i+1, "138B", "100%", "12.12.2009", "100", "Einganzganzaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa, langer Name der muss schon echt lang sein", "5" );
//}
//$pdf->Output();

?>
