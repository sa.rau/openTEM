<?php

//require('fpdf.php');

//      Cell(float w [, float h [, string txt [, mixed border [, int ln [, string align [, boolean fill [, mixed link]]]]]]])
// MultiCell(float w, float h, string txt [, mixed border [, string align [, boolean fill]]])

class FREEPDF extends FPDF {
    // Page header
    function Header() {
        $dw = 25;
        $this->mySetFont(14, 'B');
        $str = utf8_decode("FREIE GEBIETE TOP 30 - München-Harlaching");
        $this->Cell($this->lw-$dw,8,$str,0,0,'L');

        $today = getdate();
        $str = "$today[mday].$today[mon].$today[year]";
        $this->Cell($dw,8,$str,1,1,'R');

        $this->Ln($this->header_space);

        $this->headerLine();
    }

    // Page footer
    function Footer() {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        $this->mySetFont(6);
        $this->Cell(40,4,"FGT30        2/14",0,0,'L');
        $today = getdate();
        $this->Cell(90,4,"$today[mday].$today[mon].$today[year]",0,0,'C');
        $this->Cell(40,4,'Seite '.$this->PageNo().' von {nb}',0,0,'R');
    }

    // own cell function
    function MyMultiCell($w, $h, $txt, $border=0, $ln=0,  $align='J', $fill=false) {
	    if($this->y+$h > $this->PageBreakTrigger) {
            $this->y = $this->tMargin + 10;
            $this->AddPage();
        }
        $y = $this->y;

    	// Output text with automatic or explicit line breaks
    	if($w==0)
    		$w = $this->w-$this->rMargin-$this->x;

    	$s = str_replace("\r",'',$txt); // remove \r
    	$nb = strlen($s);               // get string length
    	if($nb>0 && $s[$nb-1]=="\n")    // check if last char in string is a '\n'
    		$nb--;                      // if so remove it from number of bytes

        // check border settings
    	$b = 0;
    	if($border) {
    		if($border==1) {
    			$border = 'LTRB';
    			$b = 'LRT';
    			$b2 = 'LR';
    		} else {
    			$b2 = '';
    			if(strpos($border,'L')!==false)
    				$b2 .= 'L';
    			if(strpos($border,'R')!==false)
    				$b2 .= 'R';
    			$b = (strpos($border,'T')!==false) ? $b2.'T' : $b2;
    		}
    	}

        // analyse string
    	$i = 0;
    	$j = 0;
    	$nl = 1;
        // go through each character
    	while($i<$nb) {

    		// Get next character
    		$c = $s[$i];

    		if($c=="\n") {
    			// Explicit line break
    			if($this->ws>0)	{
    				$this->ws = 0;
    				$this->_out('0 Tw');
    			}

    			$this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
    			$i++;
    			$j = $i;
    			$nl++;
    			if($border && $nl==2)
    				$b = $b2;
    			continue;
    		}

    		$i++;
    	}

    	// Last chunk
    	if($this->ws>0) {
    		$this->ws = 0;
    		$this->_out('0 Tw');
    	}

    	if($border && strpos($border,'B')!==false)
    		$b .= 'B';
    	$this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);

        // check for line break after cell
        if($ln == 0) {
    		$this->x += $w;
            $this->y = $y;
        } elseif($ln == 1) {
    		$this->x = $this->lMargin;
        }
    }

    function FREEPDF( $top=30 ) {
        FPDF::FPDF();
        $this->SetLeftMargin(25);
        $this->SetRightMargin(16);
        $this->AliasNbPages();
        $this->SetAutoPageBreak(1, 15);

        $this->top = $top;
        $this->header_space = 4.5;
        $this->lh = 9;
        $this->thickB = 0.5;
        $this->thinB  = 0.2;
        $this->fontSize = 12;

        $this->hh = 16;
        $this->lh = 8;
        $this->lw = 170;
        $this->pw = 15;

        $this->sw = 40;
        $this->sgw = 15;

        $this->gnw = 15;    // gebiet name width
        $this->gww = 15;    // gebiet #wohnungen width
        $this->gtw = 15;    // gebiet type width
        $this->gpw = 15;    // gebiet PLZ width
        $this->gsw = $this->lw - $this->sw - $this->sgw - $this->gnw - $this->gww - $this->gtw - $this->gpw;  // gebiet statdtteil width
        $this->gw = $this->gnw + $this->gww + $this->gtw + $this->gpw + $this->gsw;


        $this->hw = $this->lw - $this->pw - $this->gw - $this->sw;
        $this->hgw = 15;

        $this->mySetFont();
        $this->AddPage();
    }

    function mySetFont(  $size=10, $style='') {
        $this->SetFont('Arial',$style,$size);
        $this->fontSize = $size;
    }


    function headerLine( ) {

        $this->SetFillColor(200,200,200);
        $this->SetLineWidth($this->thickB);

        $this->mySetFont(12, 'B');
        $this->Cell(15,$this->hh, 'Platz' ,1,0,'C',1);

        $y = $this->y;
        $x = $this->x;
        $this->Cell($this->gw,$this->hh/2, 'Gebiet' ,'TR',1,'C',1);
        $this->thinLine($x, $this->y, $x+$this->gw, $this->y );
        $this->SetX($x);
        $this->mySetFont(10, 'B');
        $this->Cell($this->gnw,$this->hh/2, 'Name' ,'B',0,'C',1);
        $x = $this->GetX();
        $this->Cell($this->gww,$this->hh/2, '#W.' ,'B',0,'C',1);
        $this->thinLine($x, $this->y, $x, $this->y+$this->hh/2 );
        $x = $this->GetX();
        $this->Cell($this->gtw,$this->hh/2, 'Block' ,'B',0,'C',1);
        $this->thinLine($x, $this->y, $x, $this->y+$this->hh/2 );
        $x = $this->GetX();
        $this->Cell($this->gpw,$this->hh/2, 'Typ' ,'B',0,'C',1);
        $this->thinLine($x, $this->y, $x, $this->y+$this->hh/2 );
        $x = $this->GetX();
        $this->Cell($this->gsw,$this->hh/2, 'Ort' ,'BR',0,'C',1);
        $this->thinLine($x, $this->y, $x, $this->y+$this->hh/2 );

        $this->y = $y;
        $x = $this->x;
        $this->mySetFont(12, 'B');
        $this->Cell($this->sw,$this->hh/2, 'Frei seit' ,'TR',1,'C',1);
        $this->thinLine($x, $this->y, $x+$this->sw, $this->y );
        $this->x = $x;
        $this->mySetFont(10, 'B');
        $this->Cell($this->sw-$this->sgw,$this->hh/2, 'Datum' ,'B',0,'C',1);
        $x = $this->x;
        $this->Cell($this->sgw,$this->hh/2, 'Monate' ,'BR',1,'C',1);
        $this->thinLine($x, $this->y, $x, $this->y-$this->hh/2 );

    }

    function entryLine( $platz, $g_name, $g_wohn, $g_typ, $g_plz, $g_st, $f_date, $f_month ) {

        $this->SetFillColor(200,200,200);
        $this->mySetFont(12, 'B');
        $this->SetLineWidth($this->thickB);
        $this->Cell($this->pw,$this->lh, $platz ,1,0,'C',1);

        $this->mySetFont(10);
        $this->Cell($this->gnw,$this->lh, $g_name ,'B',0,'C');
        $x = $this->x;
        $this->Cell($this->gww,$this->lh, "$g_wohn " ,'BR',0,'R');
        $this->thinLine($x, $this->y, $x, $this->y+$this->lh );
        $x = $this->x;
        $this->Cell($this->gtw,$this->lh, "$g_typ% " ,'BR',0,'R');
        $this->thinLine($x, $this->y, $x, $this->y+$this->lh );
        $x = $this->x;
        $this->Cell($this->gpw,$this->lh, "$g_plz " ,'BR',0,'R');
        $this->thinLine($x, $this->y, $x, $this->y+$this->lh );
        $x = $this->x;
        $this->Cell($this->gsw,$this->lh, " " . $this->shortName($g_st) ,'BR',0,'L');
        $this->thinLine($x, $this->y, $x, $this->y+$this->lh );

        $this->Cell($this->sw-$this->sgw,$this->lh, $f_date ,'B',0,'C');
        $x = $this->x;
        $this->Cell($this->sgw,$this->lh, "$f_month " ,'BR',1,'R');
        $this->thinLine($x, $this->y, $x, $this->y-$this->lh );

    }

    function getTextWidthPx( $txt ) {
        $bbox = imagettfbbox($this->fontSize,0,'./design/fonts/arial.ttf',$txt);
        //$bbox = imagettfbbox($this->fontSize,0,'../../design/fonts/arial.ttf',$txt);
        $xmax = max($bbox[0], $bbox[2], $bbox[4], $bbox[6]);
        $xmin = min($bbox[0], $bbox[2], $bbox[4], $bbox[6]);
        return( $xmax - $xmin );
    }

    function shortName( $name ) {
        // loop until needed width is reached
        $max = 200;
        while( $this->getTextWidthPx($name) > $max ) {
            // split given name at ' '
            $name_split = preg_split("/\s+/", $name);
            $name = '';
            $non = count($name_split);
            $max_split_length = $max / $non;

            for( $i=0; $i < $non; $i++ ) {
                if( $this->getTextWidthPx($name_split[$i]) > $max_split_length ) {
                    $name_split[$i] = substr($name_split[$i], 0, -2) . ".";
                }
                $name .= $name_split[$i] . " ";
            }
        }
        return( utf8_decode(utf8_encode($name)) );

    }

    function thinLine( $x1, $y1, $x2, $y2 ) {
        $this->SetLineWidth($this->thinB);
        $this->Line($x1,$y1,$x2,$y2);
        $this->SetLineWidth($this->thickB);
    }

}


// Instanciation of inherited class
// Test code
//$pdf = new FREEPDF( );
//for( $i=0 ; $i < 50 ; $i++ ) {
//    $pdf->entryLine( $i+1, "138B", "200", "100", "81545", "Obergiesing Harlaching Menterschweige", "12.12.2009", "100" );
//}
//$pdf->Output();

?>
