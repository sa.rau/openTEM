<?php

//require('fpdf.php');

//      Cell(float w [, float h [, string txt [, mixed border [, int ln [, string align [, boolean fill [, mixed link]]]]]]])
// MultiCell(float w, float h, string txt [, mixed border [, string align [, boolean fill]]])

class ALLPDF extends FPDF {
    // Page header
    function Header() {
        $dw = 25;
        $this->mySetFont(14, 'B');
        $str = utf8_decode("GEBIETSÜBERSICHT - München-Harlaching");
        $this->Cell($this->lw-$dw,8,$str,0,0,'L');

        $today = getdate();
        $str = "$today[mday].$today[mon].$today[year]";
        $this->Cell($dw,8,$str,1,1,'R');

        $this->Ln($this->header_space);
    }

    // Page footer
    function Footer() {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        $this->mySetFont(6);
        $this->Cell(40,4,"GU        2/14",0,0,'L');
        $today = getdate();
        $this->Cell(90,4,"$today[mday].$today[mon].$today[year]",0,0,'C');
        $this->Cell(40,4,'Seite '.$this->PageNo().' von {nb}',0,0,'R');
    }

    // own cell function
    function MyMultiCell($w, $h, $txt, $border=0, $ln=0,  $align='J', $fill=false) {
	    if($this->y+$h > $this->PageBreakTrigger) {
            $this->y = $this->tMargin + 10;
            $this->AddPage();
        } 
        $y = $this->y;

    	// Output text with automatic or explicit line breaks
    	if($w==0)
    		$w = $this->w-$this->rMargin-$this->x;

    	$s = str_replace("\r",'',$txt); // remove \r
    	$nb = strlen($s);               // get string length
    	if($nb>0 && $s[$nb-1]=="\n")    // check if last char in string is a '\n'
    		$nb--;                      // if so remove it from number of bytes

        // check border settings
    	$b = 0;
    	if($border) {
    		if($border==1) {
    			$border = 'LTRB';
    			$b = 'LRT';
    			$b2 = 'LR';
    		} else {
    			$b2 = '';
    			if(strpos($border,'L')!==false)
    				$b2 .= 'L';
    			if(strpos($border,'R')!==false)
    				$b2 .= 'R';
    			$b = (strpos($border,'T')!==false) ? $b2.'T' : $b2;
    		}
    	}

        // analyse string
    	$i = 0;
    	$j = 0;
    	$nl = 1;
        // go through each character
    	while($i<$nb) {

    		// Get next character
    		$c = $s[$i];

    		if($c=="\n") {
    			// Explicit line break
    			if($this->ws>0)	{
    				$this->ws = 0;
    				$this->_out('0 Tw');
    			}

    			$this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
    			$i++;
    			$j = $i;
    			$nl++;
    			if($border && $nl==2)
    				$b = $b2;
    			continue;
    		}

    		$i++;
    	}

    	// Last chunk
    	if($this->ws>0) {
    		$this->ws = 0;
    		$this->_out('0 Tw');
    	}

    	if($border && strpos($border,'B')!==false)
    		$b .= 'B';
    	$this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);

        // check for line break after cell
        if($ln == 0) {
    		$this->x += $w;
            $this->y = $y;
        } elseif($ln == 1) {
    		$this->x = $this->lMargin;
        }
    }

    function ALLPDF( ) {
        FPDF::FPDF();
        $this->SetLeftMargin(25);
        $this->SetRightMargin(16);
        $this->AliasNbPages();
        $this->SetAutoPageBreak(1, 15);

        $this->header_space = 4.5;
        $this->thickB = 0.5;
        $this->thinB  = 0.2; 
        $this->fontSize = 12;

        $this->lh = 8;
        $this->lw = 170;

        $this->mySetFont();
        $this->AddPage();
    }
    
    function mySetFont(  $size=10, $style='') {
        $this->SetFont('Arial',$style,$size);
        $this->fontSize = $size;
    }

    function gebietTable( $g_name, $g_wohn, $g_typ, $g_plz, $g_ort, $g_str ) {

        // check and add page break if needed
        $nos = substr_count($g_str, "\n") + 1;
        $h = $this->lh + 5*$nos;
	    if($this->y+$h > $this->PageBreakTrigger) {
            $this->y = $this->tMargin + 10;
            $this->AddPage();
        } 
        $y = $this->y;

        $this->mySetFont(12, 'B');
        $this->SetFillColor(200,200,200);
        $this->SetLineWidth($this->thinB);

        $this->Cell(30,$this->lh, "Gebiet: $g_name"     ,1      ,0,'L',1);
        $this->Cell(20,$this->lh, "#W: $g_wohn"         ,'TBR'  ,0,'L',1);
        $this->Cell(30,$this->lh, "Block: $g_typ%"      ,'TBR'  ,0,'L',1);
        $this->Cell(90,$this->lh, "$g_plz $g_ort"       ,'TBR'  ,1,'L',1);

        $this->mySetFont(10);
        $this->MyMultiCell($this->lw,5, utf8_decode("$g_str")        ,'LBR'  ,1,'L');
        $this->Ln($this->header_space);
    }

    
}


// Instanciation of inherited class
// Test code 
//$pdf = new ALLPDF( );
//for( $i=0 ; $i < 30 ; $i++ ) {
//    $pdf->gebietTable( $i+1, "400", "100", "81234", "Harlaching-Menterschweige", "hallo\nStr\nes\könnetn auch mal längere straßen daabei sein mal sehen was dann passiert" );
//}
//$pdf->Output();

?>
