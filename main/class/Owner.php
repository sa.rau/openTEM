<?php
/***************************************************************************************************

 Author:    Stephan Rau

 Purpose:   provide owner information

***************************************************************************************************/

class Owner {

    //-- class variables
    private $db             = null;
    private $groupNurse     = null;
    private $typeNurse      = null;
    private $db_id          = null;
    private $db_surname     = null;
    private $db_firstname   = null;
    private $db_type        = null;
    private $db_group       = null;
    private $num_of_terri   = 0;

    public function __construct($p_row) {

        $this->db           = Database::connect();
        $this->groupNurse   = GroupNurse::startDialogue();
        $this->typeNurse    = TypeNurse::startDialogue();

        // assign owner information
        $this->db_id        = $p_row['ID'];
        $this->db_surname   = $p_row['col_surname'];
        $this->db_firstname = $p_row['col_firstname'];
        $this->db_type      = $p_row['col_ownertypeid'];
        $this->db_group     = $p_row['col_groupid'];
    }

    public function __destruct() {
        $this->db = null;
    }

    public static function printTableHeader() {
        print "<h1>Bericht über alle Halter</h1>";
        print '<div class="container"> <table class="table"> <thead> <tr>';
        print '<th>Laufende Nummer</th>';
        print '<th>Name</th>';
        print '<th>Typ</th>';
        print '<th>Anzahl Gebiete</th>';
        print '<th>Gruppe</th>';
        print '</tr> </thead> <tbody>';
    }

    public function getUpdateLink() {
        return '<a href="index.php?'
            . 'action=' . OwnerNurse::ACTION
            . '&step=' . OwnerNurse::EDIT
            . '&owner=' . $this->db_id
            . '" >'
            . $this->db_surname .', ' . $this->db_firstname
            . '</a>';
    }

    public function printTableRow() {
        print '<tr>';
        print "<td>LDF</td>";
        print "<td>" . $this->getUpdateLink() . "</td>";
        print "<td>" . $this->typeNurse->getName($this->db_type) . "</td>";
        print "<td>" . $this->db->getNumOfOwnedTerritories($this->db_id) . "</td>";
        print "<td>" . $this->groupNurse->getName($this->db_group) . "</td>";
        print '</tr>';

    }

    public static function printTableTailer() {
                print '</tbody> </table> </div>';
    }

    public function getGroupName() {
        return $this->groupNurse->getName($this->db_group);
    }

    public static function printDropdownHeader($p_action, $p_step) {
        print '<h1>Halter Bearbeiten</h1>'
            . '<form action="index.php?action=' . $p_action . '&step=' . $p_step . '" '
            . 'class="form-inline" method="post">'
            . '<div class="form-group"> <div class="input-group">'
            //. '<span class="input-group-addon">Halter</span>'
            . '<select class="form-control" name=halter_string onchange="this.form.submit()">'
            . '<option value="-1">Bitte Halter auswählen</option>'
        ;
    }

    public function printOptionLine($p_id) {
        print '<option value="' . $this->db_id . '"'
            . ($this->db_id == $p_id ? " selected " : " " )
            . ">$this->db_surname, $this->db_firstname (ID: $this->db_id) </option>";
    }

    public static function printDropdownTailer() {
        print '</select></div></div></form>';
    }

    public function printAssignField($p_action, $p_step, $p_do) {
        // add new gebiet to halter
        print "<h2>Gebiet Zuteilen</h2>";
        print '
            <form action="index.php?action=' . $p_action . '&step=' . $p_step . '&do=' . $p_do . '" class="form-inline" method="post">
                <div class="form-group" style="width: 16em;"> <div class="input-group">
                    <span class="input-group-addon">Gebiet</span>
                    <select class="form-control" name=gebiet[ID]>
        ';
        //xdebug_break();
        $this->db->selFreeTerries();
        while($row = $this->db->fetch()){
            print "   <option value=\"$row[ID]\">$row[col_name]</option>";
        }
        print '
                    </select>
                </div></div>
                <div class="form-group" style="width: 17em;"> <div class="input-group">
                    <span class="input-group-addon">Ausgegeben</span>
                    <input type="date" class="form-control" style="color:green;" value="' . date('Y-m-d') . '" name=gebiet[assigned]>
                </div> </div>
                <button type="submit" class="btn btn-success" data-toggle="tooltip" title="Zuteilen">
                    <span class="glyphicon glyphicon-plus"></span>
                </button>
                <input type="hidden" class="text" name="gebiet[h_ID]" value="' . $this->db_id .'"/>
            </form>
        ';
    }

    public function printFieldRecords($p_action, $p_step, $p_do) {
        // update gebiete of halter formular
        //print "<div class=\"alert alert-info\">" .
        //    "Wenn der Halter im Momment Gebiete hat können diese hier Eingetragen oder als Verloren gemeldet werden.".
        //    " Auch das Ausgabe Datum kann hier geändert werden (wenn das Feld Zurück frei gelassen wird, wird nur das Ausgegeben Datum aktualisiert).".
        //    " In beiden Fällen ist eine <strong>Datumsangabe</strong> nötig." .
        //    " Diese <strong>muss</strong> im Momment noch im <strong>Datenbank Format</strong> erfolgen." .
        //    " D. h. erst das Jahr als vierstellige Nummber, dann ein '-', dann der Monat als zweistellige Nummer,".
        //    " dann ein '-' und zum Schluss noch der Tag als zweistellige Nummer. Z. B. <strong>2014-02-01</strong>.".
        //    "<br/><strong>Wichtig:</strong> es muss auch immer ein vollstänges Datum angegeben werden.".
        //"</div>";
        $header_printed = 0;
        $this->db->selAssignedTimeTableByOwnerID($this->db_id);
        while($row = $this->db->fetch()){
            if( $header_printed == 0 ) {
                print "<h2>Gebiet Eintragen</h2>";
                $header_printed = 1;
            }
            print '
            <form action="index.php?action=' . $p_action . '&step=' . $p_step . '&do=' . $p_do . '" class="form-inline" method="post">
                <div class="form-group" style="width: 16em;"> <div class="input-group">
                      <span class="input-group-addon">Gebiet</span>
                      <input type="text" class="form-control" value="' . $row['col_name'] .'" disabled>
                </div> </div>
                <div class="form-group" style="width: 17em;"> <div class="input-group">
                      <span class="input-group-addon">Ausgegeben</span>
                      <input type="date" class="form-control" value="' . $row['col_assigned'] . '" name=gebiet[assigned]>
                </div> </div>
                <div class="form-group" style="width: 21em;"> <div class="input-group">
                      <span class="input-group-addon">Zurück</span>
                      <input type="date" style="color:green;" class="form-control" value="' . date('Y-m-d') . '" name=gebiet[back]>
                      <span class="input-group-addon"><input type="checkbox" name=gebiet[verloren]>  Verloren</span>
                </div> </div>
                <button type="submit" class="btn btn-warning" name="update_and_assign" data-toggle="tooltip" title="Eintragen und Zuteilen">
                     <span class="glyphicon glyphicon-refresh"></span>
                </button>
                <button type="submit" class="btn btn-success" name="update" data-toggle="tooltip" title="Fertig bearbeitet">
                    <span class="glyphicon glyphicon-ok"></span>
                </button>
                <input type="hidden" class="text" name="gebiet[z_ID]"   value="' . $row['z_ID'] .'"/>
				<input type="hidden" class="text" name="gebiet[ID]"     value="' . $row['g_ID'] .'"/>
                <input type="hidden" class="text" name="gebiet[h_ID]"   value="' . $this->db_id .'"/>
            </form>
            ';
		}
    }

    public function printEdit($p_action, $p_step, $p_do) {
        print "<h2>Halter Ändern</h2>";
        print '
        <form action="index.php?action=' . $p_action . '&step=' . $p_step . '&do=' . $p_do . '" class="form-inline" method="post">
            <div class="form-group"> <div class="input-group">
                <span class="input-group-addon">Nachname</span>
                <input type="text" class="form-control" value="' . $this->db_surname .'" name=halter[surname]>
            </div> </div>
            <div class="form-group"> <div class="input-group">
                <span class="input-group-addon">Vorname</span>
                <input type="text" class="form-control" value="' . $this->db_firstname . '" name=halter[firstname]>
            </div> </div>
            <div class="form-group"> <div class="input-group">
                <span class="input-group-addon">Typ</span>
                <select class="form-control" name="halter[ownertypeid]">';
                    $this->typeNurse->getDropdownOptionLines($this->db_type);
        print ' </select>
            </div> </div>
            <div class="form-group"> <div class="input-group">
                <span class="input-group-addon">Gruppe</span>
                <select class="form-control" name="halter[groupid]">
                    <option value="-1" >Bitte auswählen</option>';
                    $this->groupNurse->getDropdownOptionLines($this->db_group);
        print ' </select>
            </div> </div>
            <button type="submit" class="btn btn-warning" data-toggle="tooltip" title="Aktualisieren">
                <span class="glyphicon glyphicon-ok"></span>
            </button>
            <input type="hidden" class="text" name="halter[ID]"   value="' . $this->db_id .'"/>
        </form>
        ';
    }

    public function update( $p_array ) {
        if ( $this->db_id == $p_array['ID'] ) {
            if( $this->db->update("halter", $p_array) ) {
                print "<div class=\"alert alert-success\">Halter (ID $p_array[ID]): '$p_array[surname]', '$p_array[firstname]' aktualisiert</div>";
                // update object information
                $this->db_firstname = $p_array['firstname'];
                $this->db_surname = $p_array['surname'];
                $this->db_type = $p_array['ownertypeid'];
                $this->db_group = $p_array['groupid'];
            } else {
                print "<div class=\"alert alert-danger\">Halter: '$this->db_surname', '$this->firstname' konnte nicht aktualisiert werden. Versuche es bitte noch einmal.</div>";
            }
        } else {
            print "<div class=\"alert alert-danger\">Halter: '$this->db_surname', '$this->db_firstname' konnte nicht aktualisiert werden. Versuche es bitte noch einmal.</div>";
        }
    }

}
