<?php
/*
 *
 *
 */

  echo "<h1>Blog</h1>";

  /* TODO: move this text into databank that is modify able by admins */
  echo nl2br('
    <h2>"Neuer Report: Gruppenbericht</h2>
    <strong>16.05.2021</strong> - Nun kann auch ein Gruppenbericht erstellt werden, zur Information der Gruppenaufseher und zum einfachern Abgleich ob die Daten noch stimmen.
    <strong>Gruß, Stephan</strong>
  ');
  echo nl2br('
    <h2>"Nicht besuchen" Hinweis.</h2>
    <strong>25.12.2019</strong> - Der Hinweis "Nicht besuchen" steht wieder auf den Karten da sich die Anweisungen geändert haben.
    <strong>Gruß, Stephan</strong>
  ');
  echo nl2br('
    <h2>Kleine Erweiterung: Datumseingabe in Formularen</h2>
    <strong>25.09.2019</strong> - Anstatt Text wird jetzt der html5 input type date verwendet. Also ein kleiner Kalender aufgeht wenn man auf das Datum klickt.
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Bugfix und Erweiterung: Bericht S-13</h2>
    <strong>05.06.2019</strong> - Auf Karten wo die Wohnungszahl 0 oder der Blockanteil nicht angeben ist wird eine Notiz zum überprüfen hinzugefügt. Man kann diese Notiz auf über eine Checkbox erzeugen. Fehler mit Innenseite Beachten wurde gelöst. Bei Adressgebieten können die Namen ausgeblendet werden.
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Bugfix: Bericht S-13</h2>
    <strong>03.06.2018</strong> - Ausgegebe Gebiete erhalten jetzt nicht mehr das heute Datum als Rückgabedatum. Bei der durchschnittlichen Gebietsgrößenberechnung wird für ungezählte Gebiete eine Größe von 200 angenommen.
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Funktionsupdate: Adressgebiet Drucken</h2>
    <strong>30.04.2018</strong> - Ab sofort ist eine eigende PDF Version für die Adressgebiete verfügbar. Das besondere ist das die Adressen in der Karte markiert werden.
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Funktionsupdate: Stamm-, Adress- und Such-Gebiet</h2>
    <strong>26.03.2018</strong> - Ab sofort sind vier verschiedene Gebietstypen verfügbar: Stammgebiet, Adressgebiet, Reguläresgebiet und Suchgebiet. Einem Stammgebiet können Reguläregebiete zugewiesen werden und einen Adressgebiet Suchgebiete. Stammgebiete und Suchgebiete zeigen dann die Informationen ihrer zugeordneten Gebiete an. Die dazugehörige durchfunktion ist noch in arbeit.
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Adieu mySQL. Willkommen SQLite :-)</h2>
    <strong>22.12.2017</strong> - Im Hintergrund wurde die Datenbank vom Typ mySQL zu SQLite geändert. Für den Benutzer hat bedeutet das keine Änderung, für den Betreiber aber schon.
    So ist es möglich die Seite auf jedem php Hoster betreiben. Noch viel besser sie funktioniert auch auf einen Raspberry mit minimal installation oder in einer vmware oder mit xampp oder auf als service auf einem NAS oder ...
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Karten PDF Erstellung verfügbar</h2>
    <strong>02.04.2017</strong> - Im Bearbeiten Menü der Gebiete können jetzt auch eine Notiz zum Gebiet gespeichert werden.
    Das Menü Bearbeiten hat jetzt einen neuen Unterpunkt - Karten drucken - dort kann nun für das Ausgewählte Gebiet ein PDF erstellt werden. Dabei zeigt die Webseite eine abstrahierte Version der zu druckenden Daten.
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Tabelle mit Zusatzinformationen zu den Gebieten</h2>
    <strong>20.03.2017</strong> - Im Bearbeiten Menü der Gebiete können jetzt auch Zusatzinformationen hinterlegt werden. Die Spaltenanzahl und Bezeichnung können bei Bedarf noch angepasst werden.
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Eingabemasken überarbeitet</h2>
    <strong>04.03.2017</strong> - Diverse Eingabefelder wurden vergrößert, da teilweise die eingebebenen Werte abgeschnitten wurden. Um den dafür erforderlichen Platz zu gewinnen wurde die Texte auf den Buttons durch Piktogramme ersetzt.
    Die Karte hat eine mini Karte dazugekommen um besser einordnen zu können wo sich ein Gebiet befindet. Die Kartengröße wurde angepasst damit sie zu Druckversion passt.
    <strong>LG Stephan</strong>
  ');
    echo nl2br('
    <h2>Karten Suchfunktion</h2>
    <strong>14.02.2017</strong> - Auf den Karten kann jetzt auch nach Straßen, Orten usw. gesucht werden. Das Plugin ist allerdings nicht sonderlich schnell und es reagiert nicht wenn man zu schnell auf Enter drückt. Am besten anfangen eine Straße bzw. Ort zu tippen und dann bei dem etwas verzögertem erscheinendem Menü den passenden Punkt auswählen. (Ja ich weiß auf google und auch auf openstreetmap geht das besser, allerdings habe ich kein bessers Plugin gefunden das ich einfach hinzufügen konnte.)
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Karten Daten</h2>
    <strong>12.02.2017</strong> - Im Menüpunkt "Formulare -> Gebiet -> Bearbeiten" können nun Bereiche über OpenStreetMaps makiert werden. Es ist geplant den gezeigten Ausschnitt später für die Gebietskarten zu verwenden. Über die "+" und "-" Schaltflächen der Karte kann die Zoomstufe genauer geregelt werden als über das Mausrad.
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Neue Spalte für den Bearbeitungsfortschritt</h2>
    <strong>16.01.2017</strong> - Eine neue Spalte würde eingefügt um den aktuellen Bearbeitungsfortschritt anzuzeigen. Wenn jemand ein neues Gebiet nimmt wird davon ausgageangen das der Fortschritt 0 beträgt. Wenn ein Gebiet zurück gegeben wird, wird davon ausgegangen dass der der Fortschritt 100 beträgt.
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Geschwindigkeits Problem</h2>
    <strong>10.02.2016</strong> - Das sich die Webseite so träge verhalten hat lag an einer Datenbankabfrage. Die Entwicklungsumgebung die ich benutze verwendet eine neuere Version der Datenbanksoftware als auf dem Webserver installiert ist. Um das Problem zu lösen musste ein etwas komplexere Abfrage in zwei Abfragen aufgeteilt werden. Leider führt das dazu das <strong>neue Halter am Ende aller Listen und Auswahllisten auftauchen</strong> solange ihnen noch kein Gebiet zugeteilt wurde.
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>(Fast) Alles Neu</h2>
    <strong>18.01.2016</strong> - Das Menü wurde umgestalltet. Typen und Gruppen sind hinzugekommen. Typen sind z.B. Verkündiger, Gruppenaufseher, Gruppengehilfe und was sonst noch sinnvoll sein könnte.
	Der Code im der im Hintergrund läuft muss noch etwas aufgeräumt und dokumentiert werden. Außerdem fehlt noch das Deckblatt für das PDF.
	Aber zum ersten Beta test sollte es reichen.
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Gebiet und Halter Update</h2>
    <strong>29.04.2015</strong> - Das Menü Gebietsverwaltung -> Bearbeiten wurde entfernt. Die Bearbeiten Option ist nun in der Seitenleiste integriert unter der Überschrift Formulare.
	Auch ein paar Detailverbesserungen wurden eingebaut. So wird das Gebiet bzw. der Halter automatisch ausgewählt nach dem Selektieren. Das aktuelle Datum wird in die Felder voreingetragen und es gibt jetzt einen Schalter zum Gebiet eintragen und ein um es zurück zu geben.
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Gebiet und Halter Update</h2>
    <strong>18.2.2015</strong> - Unter Gebietsverwaltung -> Bearbeiten -> Gebiet bzw. Halter wurde die Reihenfolge der Bearbeitungsoptionen geändert.
    Die Rechte Seitenleiste klappt jetzt früher ein, auch auf dem iPad (hoffe ich).
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Gebiet Update</h2>
    <strong>7.2.2015</strong> - Unter Gebietsverwaltung -> Bearbeiten -> Gebiet können jetzt
     - neue Gebiete erstellt werden
     - die Strassen inzugegügt und bearbeitet werden
     - Zeitenträge hinzugefügt und bearbeitet werden
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Gebiet Update</h2>
    <strong>11.8.2014</strong> - Unter Gebietsverwaltung -> Bearbeiten -> Gebiet können jetzt Zeiteinträge gelöscht werdenerstellen.
    Die Datenbank Verbindung wurde überarbeitet, sowie weitere Sicherheitsmaßnahmen wurden implementiert.
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Gebiet Update</h2>
    <strong>28.2.2014</strong> - Unter Gebietsverwaltung gibt es einen neuen Unterpunkt: Bearbeiten. Dort ist das Eingabeformular für Halter funktionsfähig. Es fehlen noch ein paar Checks doch wenn man sich an die Beschreibung hält kann man Gebiete an Halter Zuteilen, Gebiete eines Halters eintragen oder als Verloren makieren und neue Halter erstellen. Damit kann ab sofort die Datenbank auf einen aktuellen stand gebracht werden. Als nächsten kommen die Fromulare für Gebietsänderungen.
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Gebiet Update</h2>
    <strong>16.2.2014</strong> - Das PDF mit allen Gebieten ist ferig. Als nächstes kommen noch die Eingabe-Formulare.
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Gebiet und Menü Update</h2>
    <strong>9.2.2014</strong> - Jetzt kann auch eine Tabelle der TOP 30 freien Gebiete erstellt werden. Und es gibt im Hauptmenü einen Link zu den Aufnahmen der Versammlung. Auch die Aufnahmen Seite wird noch überarbeitet, momentan ist es die einfachst mögliche Implementation.
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Gebiet Update</h2>
    <strong>5.2.2014</strong> - Jetzt wird auch das letzte Gebiete im Formular S-13-X angezeigt. Zudem ist jetzt neben jeder Hauptüberschrift der Gebiete Tabellen ein Button zum PDF generieren. Bei Gebiete->Ausgegeben wird ein PDF mit den Top 30 der einzutragenen Gebiete erstellt.
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Gebiet Update</h2>
    <strong>2.2.2014</strong> - Ab sofort können drei verschiedene modifierte Version des Formulars S-13-X als PDF erstellt werden. Je nach Version werden verschieden viele Gebiete auf einer Seite dargestellt. Details findet man unter Gebiete-PDF.
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Gebiet Update</h2>
    <strong>26.1.2014</strong> - Die Tabelle Gebiete-Alle zeigt nun auch die Strassennamen an. Strassen mit nicht definierten Hausnummern erhalten den Zusatz (???). Die Tabelle Gebiete-Ausgegeben zeigt nun auch eine fortlaufende Nummer an (Spaltenname Platz), damit ist leicht zu erkennen wieviele Gebiete wie lange nicht mehr eingetragen wurden. Die Tabelle Gebiete-Frei hat jetzt auch eine fortlaufende Nummer und eine Spallte die die Größe des Gebietes angibt.
    <strong>LG Stephan</strong>
  ');
  echo nl2br('
    <h2>Willkommen zur Webseite JZMH</h2>
    <strong>25.1.2014</strong> - Dies ist die iniziale Version der Webseite. Bisher Funktionen nur ein paar Abfragen was die Gebietsverwaltung betrifft. Es funktionen die Abfragen nach den Haltern der Gebiete und ein paar Abfragen über den aktuellen Status der Gebiete. In kürze folgen weitere Abfragen und die Erstellung von PDF Dokumenten, sowie Formulare zum bearbeiten und einfügen von Daten.
    <strong>LG Stephan</strong>
  ');

?>
