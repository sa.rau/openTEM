<?php
/***************************************************************************************************

 Autor:     Stephan Rau

 Propose:   create different pdf files

 TODO:
    o update messages (multilingual and common messages for all warnings)

***************************************************************************************************/

session_start(); // new browser window therefore a new session must be started

// check if user is logged in and if he has the right to see this page -----------------------------
if( isset($_SESSION["user_login"])
 && isset($_SESSION["user_gebiete"])
 &&       $_SESSION["user_gebiete"] ) {

    require('./addons/php/fpdf183/fpdf.php');
    require('./class/Database.php');

    // check step ----------------------------------------------------------------------------------
    if( isset($_GET['step']) ) {
        $step = $_GET['step'];
    } else {
        print "<h1>Error: step not given</h1>";
        exit;
    }

    $db = Database::connect();

    switch($step) {

        case 0:
        // Alle
            require('./class/reports/allpdf.php');

            $pdf = new ALLPDF( );
            $name = "";
            $flats = 0;
            $blockpart = 0;
            $type = "8xxxx";
            $place = "...";
            $str = "";

            $db->select("gebiete_all_sorted");
            while( $row = $db->fetch() ){
                // print row but create only new row if all streets printed which belong to this area
                if( $name != $row["col_name"] ) {
                    if( $name != "" ) {
                        $pdf->gebietTable($name,$flats,$blockpart,$type,$place,$str);
                    }
                    $name = $row["col_name"];
                    $flats = $row["col_flats"];
                    $blockpart = $row["col_blockpart"];
                    $type  = $row["col_type"];
                    $place  = $row["col_place"];
                    $str  = $row["col_street"] . " " . $row["col_note"];
                } else {
                    $str  .= "\n" . $row["col_street"] . " " . $row["col_note"];
                }
            }
            $pdf->gebietTable($name,$flats,$blockpart,$type,$place,$str);
            $pdf->Output();
            break;

        case 1:
            // current terri status per owner per group
            require('./class/reports/grouppdf.php');

            $pdf = new GROUPPDF( );

            // loop through groups
            $arrAllGroups = $db->getAllGroupIDs();
            foreach( $arrAllGroups as $groupID ) {
                $groupName = $db->getGroupName($groupID);
                $pdf->headerLine( $groupName );
                //   loop through owners of group
                $arrAllOwnersOfGroup = $db->getOwnerIdsByGroupId($groupID);
                $no = 1;
                foreach( $arrAllOwnersOfGroup as $ownerID ) {
                    //     list all terries of owner
                    $ownerName = $db->getOwnerNameById($ownerID);
                    $db->selAssignedTimeTableByOwnerID($ownerID);
                    $arrTimeEntries = array();
                    while($row = $db->fetch()){
                        $arrTimeEntries[$row['col_name']] = [
                            'id' => $row['col_territoryid'],
                            'lastEntry' => $row['col_assigned'],
                            'flats' => $row['col_flats']
                        ];
                    }
                    ksort($arrTimeEntries);
                    foreach( $arrTimeEntries as $terriName => $terriInfo) {
                        $terriOwnerInfo = $db->getCurrentOwnerInfo($terriInfo['id']);
                        $terriStatus = $db->getTerritoryStatus($terriInfo['id']);
                        $pdf->entryLine(
                            $no,
                            $ownerName,
                            $terriName,
                            $terriInfo['flats'],
                            $terriOwnerInfo['ownedForMonth'],
                            $terriStatus['lastDoneDate'],
                            $terriStatus['lastDoneMonth']
                        );
                        $no++;
                        if( $no % 29 == 0 ) { // add another group page if needed
                            $pdf->headerLine( $groupName );
                        }
                    }
                }
            }

            $pdf->Output();
            break;

        case 2:
        // Frei Top 30 on one Page
            require('./class/reports/freepdf.php');
            $pdf = new FREEPDF( );

            $i = 1;

            $db->select("gebiete_frei_sorted_empty_zeiten");
            while( $row = $db->fetch() ){
                $pdf->entryLine(
                    $i,
                    $row['col_name'],
                    $row['col_flats'],
                    $row['col_blockpart'],
                    $row['col_type'],
                    $row['col_place'],
                    $row['frei_seit_format'],
                    $row['frei_seit_monaten']
                );
                $i++;
            }

            $db->selectLimit("gebiete_frei_sorted", 31 - $i);
            while( $row = $db->fetch() ){
                $pdf->entryLine(
                    $i,
                    $row['col_name'],
                    $row['col_flats'],
                    $row['col_blockpart'],
                    $row['col_type'],
                    $row['col_place'],
                    $row['frei_seit_format'],
                    $row['frei_seit_monaten']
                );
                $i++;
            }

            $pdf->Output();
            break;

        case 3:
        // S-13-X-mod-3 standard Formular
            $tabsPerSheet = 3;
        case 4:
        // S-13-X-mod
            require('./class/reports/s13pdf.php');
            if( isset($_GET['tabsPerSheet']) ) {
                $tabsPerSheet = $_GET['tabsPerSheet'];
            }


            $nog = $db->getNumOfAllTerries();
            $now = $db->getNumOfAllFlats();
            $nouct =  $db->getNumOfUncountedTerries();
            $aug = $db->getNumOfAssignedTerries();
            $noet = $db->getNumOfFreeTerries();

            $arrAllTerries = $db->getAllTerrieIDs();

            $frei_seit[24] = 0;
            $frei_seit[12] = 0;
            $frei_seit[6]  = 0;
            $frei_seit[0]  = 0; // new terri, not in time table

            $ausg_seit[24] = 0;
            $ausg_seit[12] = 0;
            $ausg_seit[6]  = 0;
            $ausg_seit[0]  = 0; // new terri, was never assigned - not in time table

            // calculate month for new terries
            $date_today = new DateTime(date('Y-m-d'));
            $date_1900 = new DateTime("1900-01-01");
            $date_diff = $date_today->diff($date_1900);
            $month_1900 = $date_diff->format('%y') * 12 + $date_diff->format('%m') ;

            //while( $row = $db->fetch() ){
            foreach( $arrAllTerries as $terriID ) {
                $arrTerriStatus = $db->getTerritoryStatus($terriID);

                if ( $arrTerriStatus['assigned'] == 0 ) {
                    if ( $arrTerriStatus['freeSinceMonth'] >= $month_1900 ) { // undefined date 1900
                        $frei_seit[0]++;
                    } elseif ( $arrTerriStatus['freeSinceMonth'] > 24 ) {
                        $frei_seit[24]++;
                    } elseif ( $arrTerriStatus['freeSinceMonth'] > 12 ) {
                        $frei_seit[12]++;
                    } elseif ( $arrTerriStatus['freeSinceMonth'] > 6 ) {
                        $frei_seit[6]++;
                    }
                } else {
                    if ( $arrTerriStatus['lastDoneMonth'] >= $month_1900 ) { // undefined date 1900
                        $ausg_seit[0]++;
                    } elseif ( $arrTerriStatus['lastDoneMonth'] > 24 ) {
                        $ausg_seit[24]++;
                    } elseif ( $arrTerriStatus['lastDoneMonth'] > 12 ) {
                        $ausg_seit[12]++;
                    } elseif ( $arrTerriStatus['lastDoneMonth'] > 6 ) {
                        $ausg_seit[6]++;
                    }
                }
            }

            // create pdf
            $json_string = file_get_contents("./config/congregation.json");
            $congr = json_decode($json_string, true);
            $pdf = new S13PDF(
                $congr, $nog, $now, $nouct, $aug, $frei_seit, $ausg_seit, $tabsPerSheet
            );

            // get data
            //$db->select("gebiete_s13_sorted");

            $num_of_rows = $pdf->getNumOfRows();
            $num_of_entries = $num_of_rows - 2;
            $cur_g_name = "";
            $lifo = array();
            $i = 0;

            //while( $row = $db->fetch() ){
            foreach( $arrAllTerries as $terriID ) {

                // get terri info
                $cur_g_name = $db->getTerriName($terriID);
                $pdf->headerCell( $cur_g_name );



                // select time table of Territory and run though it
                $db->selTimeTableByTerrieID($terriID, $num_of_entries);
                $i = 0;
                while( $row = $db->fetch() ){

                    $date_assigned = new DateTime($row['col_assigned']);

                    // create empty string if back is not defined, DateTime of null returns today
                    if( $row['col_back'] == null ) {
                        $date_back_string = "";
                    } else {
                        $date_back     = new DateTime($row['col_back']);
                        $date_back_string = $date_back->format('d.m.Y');
                    }

                    $pdf->entryCell(
                        "$row[col_surname], $row[col_firstname]",
                        $date_assigned->format('d.m.Y'),
                        $date_back_string
                    );
                    $i++;
                }
                // fill column
                for( ; $i < $num_of_rows ; $i++ ) {
                    $pdf->entryCell( '', '', '');
                }
            }

            // create pdf
            $pdf->Output();
            break;


        default:
            print "<div class=\"alert alert-warn\">Irgendwas ist schief gegangen geh bitte zurück</div>";
            break;

    }

} else { ?>

<h1>Du bist nicht eingeloggt! Du darfst das nicht!</h1>

<?php header("Location: index.php"); exit; } ?>

