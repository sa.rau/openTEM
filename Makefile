#---------------------------------------------------------------------------------------------------
# Purpose: build docker images and run opentem locally
#---------------------------------------------------------------------------------------------------
.PHONY: help
help:
	@echo "prepare enviorment to run opentem with docker"
	@echo "  install_docker       install docker with convenience script for ubuntu"
	@echo "run opentem with docker"	
	@echo "  build-opentem        build opentem containter"
	@echo "  run-opentem          start opentem system locally and run in foreground"
#---------------------------------------------------------------------------------------------------


#---------------------------------------------------------------------------------------------------
install_docker:
	# installing an up to date docker version
	curl -fsSL https://get.docker.com -o get-docker.sh
	sudo sh get-docker.sh
	rm -f get-docker.sh
	sudo curl -L "https://github.com/docker/compose/releases/download/1.29.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
	sudo chmod +x /usr/local/bin/docker-compose

#---------------------------------------------------------------------------------------------------
build-opentem:
	docker build -t opentem-php .

run-opentem:
	docker run -dit --name opentem -p 8080:80 --mount type=bind,source="$$(pwd)",target=/var/www/html/opentem opentem-php
	echo "http://localhost:8080/opentem/www/develop/index.php"
